/**
 * Category Index
 * 
 * @package Post
 * @subpackage JavaScript
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */

$(function()
{
    /* Global section ------------------------------------------------------- */
        
    var global = {};
        
    /* Multiple locale ------------------------------------------------------ */
        
    $('#dsb_category_index .locale-tab li').click(function() 
    {   
        var sign = $(this).metadata().sign;
        set_classname($(this), 'selected');
        set_classname($('#locale-' + sign), 'selected');
    });
        
    $('#dsb_category_index .locale-tab li.selected').trigger('click');
        
    /* Form action ---------------------------------------------------------- */
        
    $('#dsb_category_index .button.submit').click(function()
    {
        $('#dsb_category_index form').submit();
    });
        
    $('#dsb_category_index form').live('submit', function()
    {
        var required = true;
        var data = $(this).serialize();
            
        for(var i = 0; i < $('.require').length; i++)
        {
            if($('.require').eq(i).val() == '')
            {                    
                required = false;
                break;
            }
        }
            
        if(required)
        {
            return true;
        }
        else
        {
            $('.message.error').text('Please input data on require field (req).').show();
            return false;
        }
    });
        
    /* Confirm dialog control ----------------------------------------------- */
    
    $('#confirm-okay').live('click', function()
    {       
        $.post('/dashboard/category/delete/' + global.cat_id, function()
        {
            window.location.reload();
        });
    });

    $('#confirm-cancel').live('click', function()
    {
        unblock_ui();
    });
        
    /* Category Action ------------------------------------------------------ */
        
    $('#dsb_category_index .icon-trash').click(function()
    {
        block_ui(null, $('#confirm-delete'));
            
        var cat_id = $(this).metadata().cat_id;
            
        /* Set category id to global variable */
        global.cat_id = cat_id;
    });
});
    
/* End of file category.edit.js */
/* Location: ./application/assets/modules/Dashboard/js/category.index.js */