/**
 * UI_Upload
 * 
 * @package File
 * @subpackage JavaScript
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */

function crop(data)
{
    /* Get image */
    $.post('/dashboard/file/image/ui_crop', data, function(ui_crop)
    {
        $('.mybox-body').html(ui_crop);
    
        fix_height($('#dsb_file_reader_ui_explorer').height());
    });
}

/* Globla data */
var data = null;

$(function()
{
    /* Form action ------------------------------------------------------ */
    
    /* Upload file to server */
    $('#dsb_file_writer_ui_upload form').ajaxForm(
    { 
        crop: false,
        
        beforeSend: function() 
        {
            /* Prevent close */
            tabprevent('File uploading, make sure if you want to leave this page?');
            
            /* Create progressing bar */
            $('.mybox-close').addClass('hide');
            
            /* Set crop true if #crop exist */
            if($('#crop').length > 0)
            {
                this.crop = true;
            }
            
            /* Replace upload progress */
            $('#dsb_file_writer_ui_upload #progress').removeClass('hide');
            $('.mybox-body').html($('#dsb_file_writer_ui_upload #progress'));
        },
        
        uploadProgress: function(event, position, total, percent) 
        {            
            /* Percentage counting */
            $('.progress-counter').text(percent + '%');
            
            /* Progress bar animation */
            $('.bar').width(percent + '%');
        },
        
        complete: function(response) 
        {
            $('.progress-counter').text('Complete');
            
            /* Progress bar animation */
            $('.bar').width('100%');
            
            /* Get return message */
            try 
            {
                response = $.parseJSON(response.responseText);
                
                if(response.status == 'success')
                {
                    /* Global data */
                    data = response;
                    
                    $('.progress-counter').text('File upload successfully.');
            
                    /* Starting crop image */
                    if(this.crop)
                    {
                        crop(data);
                    }
                    else
                    {
                        /* Release */
                        tabrelease();
                        
                        /* Trigger event */
                        $(window).trigger('onUploaded', data);
                    }
                }
                else
                {                   
                    $('.progress-counter').addClass('error').text(response.message);
                    $('.progress').addClass('progress-danger');
                    
                    /* Release */
                    tabrelease();
                }
            } 
            catch (exception) {  /* System error */ }
        }
    }); 
    
    /* Event handler ---------------------------------------------------- */
    
    /* Show file browser when click button */
    $('#dsb_file_writer_ui_upload a.button').click(function()
    {
        $('#dsb_file_writer_ui_upload input[type="file"]').trigger('click');
    });
    
    /* Auto submit form when file */
    $('#dsb_file_writer_ui_upload input[type="file"]').change(function()
    {
        $('#dsb_file_writer_ui_upload form').submit(); 
    });
});
    
/* End of file file.ui_upload.js */
/* Location: ./application/assets/modules/Dashboard/js/file.ui_upload.js */