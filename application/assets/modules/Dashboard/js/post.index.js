/**
 * Post Index
 * 
 * @package Post
 * @subpackage JavaScript
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */

$(function()
{  
    /* Global section ------------------------------------------------------- */
        
    var global = {};
    
    /* Checkbox ------------------------------------------------------------- */
    
    $('.goog-checkbox.all').click(function()
    {
        if($(this).is('.goog-checkbox-checked'))
        {
            $('.goog-checkbox.list').removeClass('goog-checkbox-checked');
            $(this).removeClass('goog-checkbox-checked');
        }
        else
        {
            $('.goog-checkbox.list').addClass('goog-checkbox-checked');
            $(this).addClass('goog-checkbox-checked');
        }
    });
        
    $('.goog-checkbox').click(function() 
    {
        $(this).toggleClass('goog-checkbox-checked');
    });
    
    /* Confirm dialog control ----------------------------------------------- */
    
    $('#confirm-okay').live('click', function()
    {       
        $.post('/dashboard/post/delete', 
        {
            id: global.post_id
        },
        function(response)
        {
            window.location.reload();
        });
    });

    $('#confirm-cancel').live('click', function()
    {
        unblock_ui();
    });
    
    /* Action --------------------------------------------------------------- */
    
    $('#post-delete').click(function()
    {
        var post_id = [];
        
        $('.goog-checkbox.list.goog-checkbox-checked').each(function(index, element)
        {
            post_id.push($(element).metadata().post_id);
        });
        
        if(post_id.length > 0)
        {
            global.post_id = post_id;
            
            block_ui(null, $('#confirm-delete'));
        }
    });
    
    /* Promotion */

    $('.goog-checkbox.promotion').click(function()
    {
        var post_id = $(this).metadata().post_id;
      
        block_ui();
            
        $.post('/dashboard/post/promotion', 
        {
            id: post_id
        }, 
        function()
        {
            unblock_ui(); 
        });
    });
});

/* End of file post.index.js */
/* Location: ./application/assets/modules/Dashboard/js/post.index.js */