/**
 * Script
 * 
 * @package Dashboard
 * @subpackage JavaScript
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */

/* Global function ---------------------------------------------------------- */

function tabprevent(message)
{
    if(message == '')
    {
        message = 'This page are busy, make sure if you want to refresh';
    }
    
    window.onbeforeunload = function() 
    {
        return message;
    }
}

function tabrelease()
{
    window.onbeforeunload = true;
}
                
function fix_width()
{
    /* Fixed width */
    var sidebar = {
        width: $('.mybox-body .sidebar').width(),
        height: $('.mybox-body .sidebar').height()
    };
 
    var content = {
        width: $('#cboxLoadedContent').width(),
        height: $('#cboxLoadedContent').height()
    }
    
    $('.mybox-response').width(content.width - sidebar.width - 1);
    
    /* Resize */
    $.colorbox.resize();
}

function fix_height(height)
{                
    $('#cboxMiddleLeft').height(height);
    $('#cboxContent').height(height);
    $('#cboxLoadedContent').height(height);
    $('#cboxMiddleRight').height(height);
 
    /* Resize */
    $.colorbox.resize();
}

function loading()
{
    return '\n\
        <div class="loading">\n\
            <img src="/application/assets/modules/ColorBox/image/loading.gif" alt="Loading, please wailt..." />\n\
        </div>\n\
    ';
}

function block_ui(element, message)
{
    if(typeof message == 'undefined')
    {
        message = 'Loading, please wait...';
    }
        
    var opt = 
    {
        css: 
        { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 0.65, 
            color: '#fff',
            fontSize: '13px',
            cursor: 'default',
            width: 'auto',
            left: '50%'
        },
        
        message: message
    };
            
    if(element != null)
    {
        $(element).block(opt);
    }
    else
    {
        $.blockUI(opt);
    }
    
    var marginLeft = Math.floor($('.blockUI.blockMsg.blockPage').width() / 2) * -1;
    
    $('.blockUI.blockMsg.blockPage').css('margin-left', marginLeft + 'px');
}

function unblock_ui(element)
{
    if(element != null)
    {
        $(element).unblock();  
    }
    else {
        $.unblockUI();
    }
}

function explorer(opt, event)
{
    if(typeof opt == 'undefined') opt = {};
    
    if(typeof opt.close != 'undefined')
    {
        /* Close ColoBox */
        $.colorbox.close();
        
        /* Unbind all event from window object */
        $(window).unbind();
        
        /* Unbind event from location history */
        Backbone.history.stop();
    }
    else
    {
        $.colorbox(
        {
            transition: 'none',
            overlayClose: false,
            escKey: false,
            href: '/dashboard/file/reader/ui_explorer',
            data: opt,
            onLoad: function()
            {
                /* Biding callback function if exist */
                
                /* On upload finish */
                try
                {
                    if(typeof event.onUploaded == 'function')
                    {
                        $(window).bind('onUploaded', event.onUploaded);
                    }
                
                    /* On crop finish */
                    if(typeof event.onCropped == 'function')
                    {
                        $(window).bind('onCropped', event.onCropped);
                    }
                } catch(e) { /* System error */ }
            },
            onComplete: function()
            {
                /* Fix Cufon replace */
                Cufon.replace('.cufon');
                
                /* Fix ColorBox size */
                fix_width();
                fix_height($('.mybox'));
                
                /* Auto trigger menu */
                if(opt.trigger != null)
                {
                    exe.navigate(opt.trigger, {
                        trigger: true
                    });
                }
            }
        });
    }
}

function metrobox()
{
    $('.metro-box').each(function(index, element)
    {
        /* create left, right border */
        var left = $('<div class="left-border">');
        var right = $('<div class="right-border">');
        
        /* temporary inner content */
        var children = $(element).children();
                        
        /* innsert left, right */
        $(children).before(left);
        $(children).after(right);
            
        /* fixed children width */
        $(children).width($(element).width() - 9);
    });
        
    $('.metro-box').live('focus', function()
    {
        $(this).find('.left-border').addClass('focused'); 
        $(this).find('.right-border').addClass('focused'); 
    });
        
    $('.metro-box').live('blur', function()
    {
        $(this).find('.left-border').removeClass('focused');
        $(this).find('.right-border').removeClass('focused'); 
    });
}

function lazyload(src)
{
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = src;
    var x = document.getElementsByTagName('script')[0];
    x.parentNode.insertBefore(s, x);
}

function set_classname(element, classname)
{
    $(element).siblings().removeClass(classname);
    $(element).addClass(classname);
}

/* onLoad ------------------------------------------------------------------- */

$(function()
{
    $(document).bind('cbox_closed', function()
    {
        //exe.navigate(null);
        tabrelease();
    });
    
    /* Fixed ---------------------------------------------------------------- */
    $('a.submit').live('click', function()
    {
        return false;
    });
    
    /* Metro-box script ----------------------------------------------------- */ 
    if($('.metro-box').length > 0)
    {
        metrobox();
    }
    
    /* Google checkbox script ----------------------------------------------- */
    if($('.div-label').length > 0)
    {
        $('.div-label').live('click', function()
        {
            var label = $(this).metadata().label;
            var name = $(this).metadata().name;
            
            if($(label).is('.goog-checkbox-checked'))
            {
                $(label).removeClass('goog-checkbox-checked');
                $('input[name="' + name + '"]').remove();
            }
            else
            {
                $(label).addClass('goog-checkbox-checked');
                $(label).after('<input type="hidden" name="' + name + '" value="true" />');
            }
        });
    }
    
    if($('.goog-checkbox').length > 0)
    {
        $('.goog-checkbox').live('click', function()
        {
            var trigger = $(this).metadata().trigger;
            
            $(trigger).trigger('click');
        });
    }
    
    /* MyBox event ---------------------------------------------------------- */
  
    $('.mybox-close').live('click', function()
    {
        $.colorbox.close();
    });
});

/* End of file dsb.script.js */
/* Location: ./application/assets/modules/Dashboard/js/dsb.script.js */