/**
 * Post Add
 * 
 * @package Post
 * @subpackage JavaScript
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */

$(function()
{
    /* Global ----------------------------------------------------------- */
        
    var global = {};
        
    /* Product photo ---------------------------------------------------- */
        
    $('.big-thumb img').click(function()
    {
        var self = this;
            
        explorer(
        {
            crop: true,
            ratio: 1,
            writeonly: true
        },
        {
            onCropped: function(event, args)
            {
                var id = $('#file_product_photo').val();
                    
                /* Remove old file uploaded */
                if(id != '')
                {
                    $.post('/dashboard/file/writer/delete/' + id);
                }
                    
                var image = args['message'][0];
                    
                $(self).attr('src', '/' + image['path'] + image['name']);
                $('#file_product_photo').val(image['id']);

                explorer({
                    close: true
                });
            }
        });
    });
        
    /* Multiple locale -------------------------------------------------- */
        
    $('#dsb_post_add .locale-tab li').click(function() 
    {   
        var sign = $(this).metadata().sign;
        set_classname($(this), 'selected');
        set_classname($('#locale-' + sign), 'selected');
    });
        
    $('#dsb_post_add .locale-tab li.selected').trigger('click');
        
    /* Form action ------------------------------------------------------ */
        
    $('#post-publish').click(function()
    {
        $('#post_status_draft').remove();
        $('#dsb_post_add form').submit();
    });
        
    $('#post-draft').click(function()
    {
        $('#post_status_publish').remove();
        $('#dsb_post_add form').submit();
    });
    
    $('#dsb_post_add form').live('submit', function()
    {
        var required = true;
            
        for(var i = 0; i < $('.require').length; i++)
        {
            if($('.require').eq(i).val() == '')
            {                    
                required = false;
                break;
            }
        }
            
        if(required)
        {
            return true;
        }
        else
        {
            $('.message-box.error').parent().show();
            $('.message-box.error').text('Please input data on require field (req).').show();
            return false;
        }
    });
});
    
/* End of file post.add.js */
/* Location: ./application/assets/modules/Dashboard/js/post.add.js */