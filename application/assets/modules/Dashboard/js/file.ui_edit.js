/**
 * UI_Edit
 * 
 * @package File
 * @subpackage JavaScript
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */

$(function()
{                    
    metrobox();
                
    $('.button-delete').click(function()
    {
        var id = $(this).metadata().id;
        
        block_ui($('.mybox'), $('#confirm-delete'));
    });
    
    /* Confirm dialog control ----------------------------------------------- */
    
    $('#confirm-okay').live('click', function()
    {
        var id = $(this).metadata().id;
        
        $.post('/dashboard/file/writer/delete/' + id, function()
        {
            history.back();
            unblock_ui($('.mybox'));
            $('.mybox').css('position', 'initial');
        });
    });

    $('#confirm-cancel').live('click', function()
    {
        unblock_ui($('.mybox'));
        $('.mybox').css('position', 'initial');
    });
    
    $('#url').mouseover(function()
    {
        $(this).focus().select();
    });
});

/* End of file file.ui_edit.js */
/* Location: ./application/assets/modules/Dashboard/js/file.ui_edit.js */