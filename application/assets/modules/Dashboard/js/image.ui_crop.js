/**
 * UI_Crop
 * 
 * @package File
 * @subpackage JavaScript
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */

var jopt = {
    onChange: setdata,
    onSelect: setdata,
    bgColor: 'black',
    bgOpacity: 0.4
};

if(parent.ratio != null)
{
    jopt.aspectRatio = ratio;
}

function setdata(jcrop)
{
    $('#src_x').val(jcrop.x);
    $('#src_y').val(jcrop.y);
    $('#src_width').val($('.crop-image').width());
    $('#src_height').val($('.crop-image').height());
    $('#dst_width').val(jcrop.w);
    $('#dst_height').val(jcrop.h);
}

function cropcomplete()
{
    var thumb = $('.group-thumb li.selected');
    var img = $(thumb).children('img');
    var crop_complete = '<div class="crop-complete" style="width: ' + $(img).width() + 'px;"><div class="icon-ok"></div></div>'

    $(thumb).addClass('completed');
    $(thumb).children('img').before(crop_complete);
    
    /* Trigger click to next image, if not then find the one not crop? */
    var next = $('.group-thumb li.selected').next();
    
    if(!$(next).is('.completed'))
    {
        $(next).trigger('click');
    }
    else 
    {
        var other = $(".group-thumb li:not('.completed')");
        
        if($(other).length > 0)
        {
            $(other).eq(0).trigger('click');
        }
    }
}

$(function()
{
    /* Binding event */
    $('.group-thumb li').click(function()
    {
        var self = this;
        
        /* Hide crop button is crop completed */
        if($(this).is('.completed'))
        {
            $('#form-crop').hide();
        }
            
        
        $(this).siblings().removeClass('selected');
        $(this).addClass('selected');
        
        var id = $(this).metadata().id;
        
        /* Set id */
        $('#id').val(id);
        
        /* Loading animation */        
        $('.crop-body').html(loading());
                
        /* Set image */
        $.post('/dashboard/file/image/ui_crop_image', 
        {
            id: id,
            screen: screen,
            mybox_width: $('.mybox').width()
        },
        function(ui_crop_image)
        {           
            /* Show image */
            $('.crop-body').html(ui_crop_image);
        
            if(!$(self).is('.completed'))
            {
                $('#form-crop').show();
                
                /* Init Jcrop */
                $('.crop-image').Jcrop(jopt);
            }
                
            /* Fixed mybox */
            fix_width();
            fix_height($('.mybox').height());
        });
    });

    /* Trigger selected .group-thumb */
    $('.group-thumb li.selected').trigger('click');
    
    /* Crop and save */
    $('#dsb_image_ui_crop form').submit(function()
    {
        var data = $(this).serialize();
    
        block_ui($('.mybox'));
    
        $.post($(this).attr('action'), data, function(response)
        {
            cropcomplete();
            
            unblock_ui($('.mybox'));
            
            $('.group-thumb li.selected').trigger('click');
            
            /* Return process */
            if($('.group-thumb li.completed').length == $('.group-thumb li').length)
            {            
                $('.crop-finish').fadeIn('fast');
            }
        });

        return false;
    });
    
    /* Biding on click finish button ---------------------------------------- */
    
    $('.crop-finish a.button').click(function()
    {
        /* Release */
        tabrelease();
                
        /* Trigger event */
        $(window).trigger('onCropped', parent.data);
    });
});

/* End of file image.ui_crop.js */
/* Location: ./application/assets/modules/Dashboard/js/image.ui_crop.js */