/**
 * UI_Explorer
 * 
 * @package File
 * @subpackage JavaScript
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */

var screen = { 
    width: $(window).width(),
    height: $(window).height()
};

    
function getopt()
{
    var data = $('.mybox form.opt').serialize();       
    return data;
}

var Explorer = Backbone.Router.extend(
{
    routes:
    {
        /* Upload new file */
        "upload": "upload",
        
        /* Browse all uploaded */
        "browse/:page": "browse",
        
        /* Edit photo after upload */
        "edit/:id": "edit"
    },
    
    upload: function()
    {
        set_classname($('.mybox .sidebar-upload').eq(0), 'selected');
        
        $('.mybox-response').html(loading());
        
        $.post('/dashboard/file/writer/ui_upload', getopt(), function(ui_upload)
        {
            $('.mybox-response').html(ui_upload);
                
            /* Resize height */
            $('#dropbox').height(Math.ceil(screen.height / 1.5));
                
            fix_width();
            fix_height($('.mybox').height());
        });
    },
        
    browse: function(page)
    {        
        set_classname($('.mybox .sidebar-browse'), 'selected');
        
        $('.mybox-response').html(loading());
        
        $.post('/dashboard/file/reader/ui_browse/' + page, getopt(), function(ui_browse) 
        {
            $('.mybox-response').html(ui_browse);
            
            fix_width();
            fix_height($('.mybox').height());
        });
    },
    
    edit: function(id)
    {   
        set_classname($('.mybox .sidebar-browse'), 'selected');
                    
        $('.mybox-response').html(loading());
            
        $.post('/dashboard/file/reader/ui_edit/' + id, getopt(), function(ui_edit)
        {
            $('.mybox-response').html(ui_edit);
       
            fix_width();
            fix_height($('.mybox').height());
        });
    }
});
    
/* Fork new router object */
var exe = new Explorer();
    
/* Allow push state engine */
Backbone.history.start();
    
$(function()
{
    $('.mybox').width(Math.ceil(screen.width / 1.3));
        
    $('.mybox .sidebar a').click(function()
    {
        /* Remove and add selected class */
        set_classname(this, 'selected');
        
        /* Get contetn URL */
        var href = $(this).attr('href');    
        href = href.substr(1, href.length);
            
        /* Router trigger */
        exe.navigate(href, {
            trigger: true
        });
            
        /* Prevent default action */
        return false;
    });
    
    /* Sidebar -------------------------------------------------------------- */
    
    $('.mybox .sidebar .selected').find('a').trigger('click');
    
    /* Action buttons ------------------------------------------------------- */
    
    $('.button-back').live('click', function()
    {
        history.back();
    });
});
    
/* End of file file.ui_explorer.js */
/* Location: ./application/assets/modules/Dashboard/js/file.ui_explorer.js */