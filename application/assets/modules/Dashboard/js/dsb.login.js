/**
 * Login
 * 
 * @package Dashboard
 * @subpackage JavaScript
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */

$(function() 
{
    $('#dsb_dashboard_login input[type="text"]').focus();
    
    /* Trigger enter button  ------------------------------------------------ */
       
    $('#dsb_dashboard_login input[type="text"]').keypress(function(event)
    {
        if(event.keyCode != null && event.keyCode == 13)
        {
            if($(this).val() != '')
            {
                $('#dsb_dashboard_login input[type="password"]').focus();
            }
        }
    });
    
    $('#dsb_dashboard_login input[type="password"]').keypress(function(event)
    {     
        if(event.keyCode != null && event.keyCode == 13)
        {
            if($(this).val() != '')
            {
                $('#dsb_dashboard_login form').submit();
            }
        }
    })
    
    /* Submit form ---------------------------------------------------------- */
    
    $('#dsb_dashboard_login form').submit(function()
    {
        var data = $(this).serialize();
        
        $.post($(this).attr('action'), data, function(response)
        {
            if(response.status == 'success')
            {
                window.location.href = '/dashboard';
            }
            else
            {
                /* Login error */
                $('#login_error').html(response.message).fadeIn('fast');
            }
        }, 'json');
            
        return false;
    });
});
    
/* End of file dsb.login.js */
/* Location: ./application/assets/modules/Dashboard/js/dsb.login.js */