<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "pages/cover_page";
$route['404_override'] = '';

/*
 | -------------------------------------------------------------------------
 | HMVC
 | -------------------------------------------------------------------------
 | This router for rewrite HMVC prefix URL for cleanly and readability
 |
 */

/* [Back-end] --------------------------------------------------------------- */

/* Auth routing */
$route['dashboard/auth/(:any)'] = "User/Dsb_Auth/$1";

/* User routing */
$route['dashboard/user/(:any)'] = "User/Dsb_User/$1";

/* File routing */
$route['dashboard/file/writer/(:any)'] = "File/Dsb_File_Writer/$1";
$route['dashboard/file/reader/(:any)'] = "File/Dsb_File_Reader/$1";
$route['dashboard/file/writer'] = "File/Dsb_File_Writer";
$route['dashboard/file/reader'] = "File/Dsb_File_Reader";

/* Image routing */
$route['dashboard/file/image/(:any)'] = "File/Dsb_Image/$1";
$route['dashboard/file/image'] = "File/Dsb_Image";

/* Post routing */
$route['dashboard/post/(:any)'] = "Post/Dsb_Post/$1";
$route['dashboard/post'] = "Post/Dsb_Post";

/* Recipe routing */
$route['dashboard/recipe/(:any)'] = "Post/Dsb_Recipe/$1";
$route['dashboard/recipe'] = "Post/Dsb_Recipe";

/* Page routing */
$route['dashboard/page/save/(:any)'] = "Page/Dsb_Page/save_$1";
$route['dashboard/page/edit/(:any)'] = "Page/Dsb_Page/edit_$1";
$route['dashboard/page/delete/(:any)'] = "Page/Dsb_Page/delete_$1";
$route['dashboard/page/(:any)'] = "Page/Dsb_Page/$1";
$route['dashboard/page'] = "Page/Dsb_Page";

/* Category routing */
$route['dashboard/category/(:any)'] = "Post/Dsb_Category/$1";
$route['dashboard/category'] = "Post/Dsb_Category";

/* Order routing */
$route['dashboard/order/(:any)'] = "Cart/Dsb_Order/$1";
$route['dashboard/order'] = "Cart/Dsb_Order";

/* Video routing */
$route['dashboard/video/(:any)'] = "Video/Dsb_Video/$1";
$route['dashboard/video'] = "Video/Dsb_Video";

/* Default */
$route['dashboard/(:any)'] = "Dashboard/Dsb_Dashboard/$1";
$route['dashboard'] = "Dashboard/Dsb_Dashboard";

/* [Fron-end] --------------------------------------------------------------- */

/* Recipe routing */
$route['recipe/(:any)/(:any)'] = "pages/detail_recipe/$1";

/* Detail of product routing */
$route['detail/(:any)/(:any)'] = "pages/detail/$1";

/* Route for video view */
$route['pages/video/view/(:num)'] = "pages/video_view/$1";

$route['(:any)/(:any)'] = "$1/$2";

/* End of file routes.php */
/* Location: ./application/config/routes.php */