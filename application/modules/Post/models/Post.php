<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Post
 * 
 * @package Post
 * @subpackage Models
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
class Post extends MY_Model
{

    var $table = 'post';

    public function save($data = NULL)
    {
        if (!isset($data))
        {
            /* Save as draft */
            $data = array(
                'name' => '[Draft] ' . date('F d Y H:i:s'),
                'content' => '',
                'type' => POST_PRODUCT,
                'status' => DRAFT
            );
        }

        return parent::save($data);
    }

    public function get($uid, $id = NULL, $offset = NULL, $limit = NULL, $order = 'asc')
    {
        $result = parent::get($uid, $id, $offset, $limit, $order);
        
        if (isset($id))
        {
            $result = array($result);
        }
        
        /* Fetch array for get post metadata */
        foreach ($result as &$post)
        {
            $metadata = $this->get_metadata($post['id']);
            $post['metadata'] = $metadata;
        }

        if (isset($id))
        {
            return (!empty($result) ? $result[0] : NULL);
        }

        return $result;
    }

    function find($uid = NULL, $cond = NULL, $offset = NULL, $limit = NULL, $order = 'asc')
    {
        $result = parent::find($uid, $cond, $offset, $limit, $order);

        if(!empty($cond['id']))
        {
            return (!empty($result) ? $result[0] : NULL);
        }
        
        return $result;
    }

    public function get_metadata($post_id, $name = NULL)
    {
        $this->db->where('post_id', $post_id);

        if (isset($name))
        {
            $this->db->where('name', $name);
        }

        $result = $this->db->get('post_metadata')->result_array();

        if (isset($name))
        {
            return (!empty($result) ? $result[0] : NULL);
        }

        /* Re-form metadata */
        foreach ($result as $index => $value)
        {
            $result[$value['name']] = $value;
            unset($result[$index]);
        }

        return $result;
    }

    public function set_metadata($post_id, $name, $value)
    {
        $result = $this->get_metadata($post_id, $name);

        $metadata = array(
            'post_id' => $post_id,
            'name' => $name,
            'value' => $value
        );

        if (!empty($result))
        {
            /* Let's update */

            $metadata['modified'] = date('Y-m-d H:i:s');

            $this->db->where('id', $result['id']);
            $this->db->update('post_metadata', $metadata);

            $metadata_id = $result['id'];
        }
        else
        {
            /* Let's insert */

            $metadata['created'] = date('Y-m-d H:i:s');
            $metadata['modified'] = date('Y-m-d H:i:s');

            $metadata_id = $this->db->insert('post_metadata', $metadata);
        }

        return $metadata_id;
    }

    public function get_file_post($post_id, $file_id = NULL, $offset = NULL, $limit = NULL)
    {
        $this->db->where('post_id', $post_id);
        
        if(isset($file_id))
        {
            $this->db->where('file_id', $file_id);
        }
        
        if(isset($offset) AND isset($limit))
        {
            $this->db->limit($limit, $offset);
        }
        
        $result = $this->db->get('file_post')->result_array();
        
        if(isset($file_id))
        {
            return (!empty($result) ? $result[0] : NULL);
        }
        
        return $result;
    }
    
    public function set_file_post($post_id, $file_id)
    {
        $result = $this->get_file_post($post_id, $file_id);
        
        if(empty($result))
        {
            $data = array(
                'post_id' => $post_id,
                'file_id' => $file_id,
                'created' => date('Y-m-d H:i:s')
            );
            
            $this->db->insert('file_post', $data);
        }
    }
}

/* End of file Post.php */
/* Location: ./application/modules/Post/models/Post.php */