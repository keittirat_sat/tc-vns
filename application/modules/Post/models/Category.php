<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Category
 * 
 * @package Post
 * @subpackage Models
 * @since 2.0 
 * @author Nomkhonwaan ComputerScience
 */
class Category extends MY_Model
{

    var $table = 'category';

}

/* End of file Category.php */
/* Location: ./application/modules/Post/models/Category.php */
