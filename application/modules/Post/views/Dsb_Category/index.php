<?php
/**
 * Index
 * 
 * @package Post
 * @subpackage Dsb_Category
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
?>

<div id="dsb_category_index">

    <div class="container_12 content clearfix">
        <div class="content-header grid_12">
            <p class="sub-label super-big">
                Category
            </p>
        </div><!-- content-header -->

        <div class="grid_12 content-body">
            <div class="category-add grid_6 alpha">
                <?php echo form_open('dashboard/category/save'); ?>

                <p class="sub-label big">Add</p>
                <div class="locale-tab clearfix">
                    <ul>
                        <?php if (!empty($enable_locale)): ?>
                            <?php foreach ($enable_locale as $index => $value): ?>
                                <?php $selected = (!strcmp($value['sign'], 'en_US') ? ' selected ' : NULL); ?>
                                <?php $latest = ($index + 1 == count($enable_locale) ? ' latest ' : NULL); ?>
                                <li class="<?php echo $selected, $latest; ?> { sign: '<?php echo $value['sign']; ?>' }">
                                    <div class="flag flag-<?php echo $value['sign']; ?>"></div>
                                    <?php echo $value['name']; ?>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div><!-- .locale-tab -->

                <?php if (!empty($enable_locale)): ?>
                    <?php foreach ($enable_locale as $index => $value): ?>

                        <div id="locale-<?php echo $value['sign']; ?>" class="locale-content clearfix hide">
                            <div class="grid_6 alpha omega begin">
                                <div class="section clearfix">
                                    <label class="div-label">Name (req):</label>
                                    <div class="metro-box">
                                        <?php echo form_input("category[{$value['sign']}][name]", NULL, 'class="require"'); ?>
                                    </div>
                                </div><!-- .section -->
                            </div>

                            <div class="grid_6 alpha omega">
                                <div class="section clearfix">
                                    <label class="div-label">Description:</label>
                                    <?php echo form_textarea("category[{$value['sign']}][description]"); ?>
                                </div><!-- .section -->
                            </div>

                            <div class="grid_6 alpha omega end-action">
                                <div class="section clearfix">
                                    <a class="button submit">Save</a>
                                    <span class="sub-label message error hide" style="margin-left: 3px;"></span>
                                </div>
                            </div>
                        </div>

                    <?php endforeach; ?>
                <?php endif; ?>        

                <?php echo form_close(); ?>
            </div><!-- .category-add -->

            <div class="category-list grid_6 omega">
                <p class="sub-label big">Category List</p>

                <?php if (!empty($category_list)): ?>
                    <ul>
                        <?php foreach ($category_list as $category): ?>
                            <li id="category-<?php echo $category['en_US']['id']; ?>" title="Edit">
                                <a href="<?php echo site_url("dashboard/category/edit/{$category['en_US']['id']}"); ?>">
                                    <?php echo $category['en_US']['name']; ?>
                                    <span class="sub-desc">
                                        -
                                        created on
                                        <?php $timestamp = strtotime($category['en_US']['created']); ?>
                                        <?php echo date('d/m/Y H:i', $timestamp); ?> 
                                    </span>
                                </a>

                                <div class="right">
                                    <div class="icon-trash { cat_id: <?php echo $category['en_US']['id']; ?> }" title="Delete"></div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div><!-- .content-body -->
    </div><!-- .content -->

</div><!-- #dsb_category_index -->

<!-- Hidden area -->
<div class="hidden">

    <!-- Confirm  dialog when delete this file -------------------------------->
    <div id="confirm-delete" class="confirm-dialog">
        <p class="sub-label">
            Are you sure you want to permanently delete this category?
        </p><!-- .sub-label -->

        <div class="confirm-panel clearfix">
            <div id="confirm-okay" class="confirm-button danger">Confirm</div>
            <div id="confirm-cancel" class="confirm-button" style="margin-left: 7px;">Cancel</div>
        </div><!-- confirm-button -->
    </div>
    <!-- #confirm -->

</div>
<!-- / -->

<?php echo js_asset('category.index.js', 'Dashboard'); ?>

<?php
/* End of file index.php */
/* Location: ./application/modules/Post/views/Dsb_Category/index.php */