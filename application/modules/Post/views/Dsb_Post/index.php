<?php
/**
 * Index 
 * 
 * @package Post
 * @subpackage Dsb_Post
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
?>

<div id="dsb_post_index">

    <div class="container_12 content clearfix">
        <div class="content-header grid_12">
            <p class="sub-label super-big">
                All Product
            </p>
        </div><!-- content-header -->

        <div class="grid_12 content-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Price (baht)</th>
                        <th>Stock</th>
                        <th>Category</th>
                        <th>Promotion</th>
                        <th>Status</th>
                        <th>Created</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($product_list)): ?>
                        <?php foreach ($product_list as $product): ?>
                            <tr class="<?php if (!empty($product['metadata']['stock']) AND $product['metadata']['stock']['value'] == 0): ?>out-of-stock<?php endif; ?>">
                                <td><div class="goog-checkbox list { post_id: <?php echo $product['id']; ?> }"></div></td>
                                <td><?php echo ellipsize($product['name'], 60); ?></td>
                                <td><?php echo (empty($product['metadata']['price']) ? NULL : number_format($product['metadata']['price']['value'])); ?></td>
                                <td><?php echo (empty($product['metadata']['stock']) ? 0 : $product['metadata']['stock']['value']); ?></td>
                                <td><?php echo $product['cat_name']; ?></td>
                                <td><div class="goog-checkbox promotion { post_id: <?php echo $product['id']; ?> } <?php echo (!empty($product['promotion']) ? 'goog-checkbox-checked' : NULL); ?>"></div></td>
                                <td><?php echo post_status($product['status']); ?></td>
                                <td>
                                    <?php $timestamp = strtotime($product['created']); ?>
                                    <?php echo date('F d Y H:i', $timestamp); ?>
                                </td>
                                <td>
                                    <a href="<?php echo site_url("dashboard/post/edit/{$product['id']}"); ?>" target="_blank">
                                        Click
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div><!-- .content-body -->

        <div class="content-footer grid_12">
            <a id="post-delete" class="button">Delete Select</a>

            <div class="page right">
                <?php echo $pagination; ?>
            </div>
        </div><!-- .content-footer -->
    </div><!-- .content -->

</div><!-- #dsb_post_index -->

<!-- Hidden area -->
<div class="hidden">

    <!-- Confirm  dialog when delete this file -------------------------------->
    <div id="confirm-delete" class="confirm-dialog">
        <p class="sub-label">
            Are you sure you want to permanently delete this product?
        </p><!-- .sub-label -->

        <div class="confirm-panel clearfix">
            <div id="confirm-okay" class="confirm-button danger">Confirm</div>
            <div id="confirm-cancel" class="confirm-button" style="margin-left: 7px;">Cancel</div>
        </div><!-- confirm-button -->
    </div>
    <!-- #confirm -->

</div>
<!-- / -->

<?php echo js_asset('post.index.js', 'Dashboard'); ?>

<?php
/* End of  file index.php */
/* Location: ./application/modules/Post/views/Dsb_Post/index.php */