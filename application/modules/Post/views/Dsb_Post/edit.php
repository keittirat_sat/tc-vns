<?php
/**
 * Edit
 * 
 * @package Post
 * @subpackage Dsb_Post
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
?>

<div id="dsb_post_edit">

    <?php echo form_open('dashboard/post/save'); ?>

    <div class="container_12 content clearfix">
        <div class="content-header grid_12">
            <p class="sub-label super-big">
                Edit
            </p>
        </div><!-- content-header -->

        <div class="grid_12 content-body">
            <div class="grid_4 alpha">

                <p class="sub-label big">Photo</p>
                <div class="big-thumb">
                    <?php if (!empty($post['image'][0])): ?>
                        <img src="/<?php echo $post['image'][0]['path'], $post['image'][0]['name']; ?>" alt="No image" width="292" />
                    <?php else: ?>
                        <img src="<?php echo image_asset_url('no_img.gif', 'Dashboard'); ?>" alt="No image" width="292" />
                    <?php endif; ?>
                    <input id="file_product_photo" type="hidden" name="file[id]" value="<?php echo (!empty($post['image'][0]) ? $post['image'][0]['id'] : NULL); ?>" />
                    <input id="post_parent_id" type="hidden" value="<?php echo $post['en_US']['id']; ?>" />
                </div><!-- .big-thumb -->

                <p class="sub-label big" style="margin-top: 35px;">Category</p>
                <div class="category">           
                    <?php echo form_dropdown('cat_id', $dropdown_list, $post['en_US']['cat_id']); ?>
                </div><!-- .category -->

                <p class="sub-label big" style="margin-top: 35px;">Save</p>
                <div class="save">
                    <a id="post-publish" class="button">Publish</a>
                    <input id="post_status_publish" type="hidden" name="status" value="<?php echo PUBLISH; ?>" />

                    <a id="post-draft" class="button" style="margin-left: 7px;">Draft</a>
                    <input id="post_status_draft" type="hidden" name="status" value="<?php echo DRAFT; ?>" />
                </div>
            </div><!-- .grid_4 -->

            <div class="grid_8 omega">
                <p class="sub-label big">Content</p>
                <div class="locale-tab clearfix">
                    <ul>
                        <?php if (!empty($enable_locale)): ?>
                            <?php foreach ($enable_locale as $index => $value): ?>
                                <?php $selected = (!strcmp($value['sign'], 'en_US') ? ' selected ' : NULL); ?>
                                <?php $latest = ($index + 1 == count($enable_locale) ? ' latest ' : NULL); ?>
                                <li class="<?php echo $selected, $latest; ?> { sign: '<?php echo $value['sign']; ?>' }">
                                    <div class="flag flag-<?php echo $value['sign']; ?>"></div>
                                    <?php echo $value['name']; ?>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div><!-- .locale-tab -->

                <div class="clearfix">
                    <?php if (!empty($enable_locale)): ?>
                        <?php foreach ($enable_locale as $index => $value): ?>

                            <div id="locale-<?php echo $value['sign']; ?>" class="locale-content clearfix hide">
                                <div class="grid_8 alpha omega begin">
                                    <div class="section clearfix hide" style="padding: 7px;">
                                        <div class="message-box error hide"></div>
                                    </div><!-- .section -->
                                </div>

                                <div class="grid_8 alpha omega">
                                    <div class="section clearfix">
                                        <label class="div-label">Name:</label>
                                        <div class="metro-box">
                                            <?php echo form_input("post[{$value['sign']}][name]", (empty($post[$value['sign']]['name']) ? NULL : $post[$value['sign']]['name']), 'class="require"'); ?>
                                        </div>
                                    </div><!-- .section -->
                                </div>

                                <?php if (!strcmp($value['sign'], 'en_US')): ?>
                                    <div class="grid_8 alpha omega">
                                        <div class="section clearfix">
                                            <label class="div-label">Price baht (req):</label>
                                            <div class="metro-box">
                                                <?php echo form_input("metadata[{$value['sign']}][price]", (empty($post[$value['sign']]['metadata']['price']) ? NULL : $post[$value['sign']]['metadata']['price']['value']), 'class="require"'); ?>
                                            </div>
                                        </div><!-- .section -->
                                    </div> 
                                <?php else: ?>
                                    <?php echo form_hidden("metadata[{$value['sign']}][price]", NULL); ?>
                                <?php endif; ?>

                                <?php if (!strcmp($value['sign'], 'en_US')): ?>
                                    <div class="grid_8 alpha omega">
                                        <div class="section clearfix">
                                            <label class="div-label">Stock:</label>
                                            <div class="metro-box">
                                                <?php echo form_input("metadata[{$value['sign']}][stock]", (empty($post[$value['sign']]['metadata']['stock']) ? 0 : $post[$value['sign']]['metadata']['stock']['value'])); ?>
                                            </div>
                                        </div><!-- .section -->
                                    </div> 
                                <?php else: ?>
                                    <?php echo form_hidden("metadata[{$value['sign']}][stock]", NULL); ?>
                                <?php endif; ?>

                                <div class="grid_8 alpha omega">
                                    <div class="section clearfix">
                                        <label class="div-label">Net weight:</label>
                                        <div class="metro-box">
                                            <?php echo form_input("metadata[{$value['sign']}][net_weight]", (empty($post[$value['sign']]['metadata']['net_weight']) ? NULL : $post[$value['sign']]['metadata']['net_weight']['value'])); ?>
                                        </div>
                                    </div><!-- .section -->
                                </div>

                                <div class="grid_8 alpha omega">
                                    <div class="section clearfix">
                                        <label class="div-label">Size of box:</label>
                                        <div class="metro-box">
                                            <?php echo form_input("metadata[{$value['sign']}][size_of_box]", (empty($post[$value['sign']]['metadata']['size_of_box']) ? NULL : $post[$value['sign']]['metadata']['size_of_box']['value'])); ?>
                                        </div>
                                    </div><!-- .section -->
                                </div>

                                <div class="grid_8 alpha omega">
                                    <div class="section clearfix">
                                        <label class="div-label">Weight W / bottle:</label>    
                                        <div class="metro-box">
                                            <?php echo form_input("metadata[{$value['sign']}][weight]", (empty($post[$value['sign']]['metadata']['weight']) ? NULL : $post[$value['sign']]['metadata']['weight']['value'])); ?>
                                        </div>
                                    </div><!-- .section -->
                                </div>

                                <div class="grid_8 alpha omega">
                                    <div class="section clearfix">
                                        <label class="div-label">Size of product:</label>
                                        <div class="metro-box">
                                            <?php echo form_input("metadata[{$value['sign']}][size_of_product]", (empty($post[$value['sign']]['metadata']['size_of_product']) ? NULL : $post[$value['sign']]['metadata']['size_of_product']['value'])); ?>
                                        </div>
                                    </div><!-- .section -->
                                </div>

                                <div class="grid_8 alpha omega">
                                    <div class="section clearfix">
                                        <label class="div-label">Amout of product / box:</label>    
                                        <div class="metro-box">
                                            <?php echo form_input("metadata[{$value['sign']}][amount_of_product]", (empty($post[$value['sign']]['metadata']['amount_of_product']) ? NULL : $post[$value['sign']]['metadata']['amount_of_product']['value'])); ?>
                                        </div>
                                    </div><!-- .section -->
                                </div>

                                <div class="grid_8 alpha omega end">
                                    <div class="section clearfix">
                                        <label class="div-label">Detail:</label>
                                        <?php echo form_textarea("post[{$value['sign']}][content]", (empty($post[$value['sign']]['content']) ? NULL : $post[$value['sign']]['content'])); ?>
                                    </div><!-- .section -->
                                </div>
                            </div><!-- .locale-content -->

                            <?php echo form_hidden("post[{$value['sign']}][id]", $post[$value['sign']]['id']); ?>

                        <?php endforeach; ?>
                    <?php endif; ?>                            
                </div><!-- .clearfix -->
            </div><!-- .grid_8 -->

        </div><!-- .content-body -->
    </div><!-- .container_12 -->

    <?php echo form_hidden('return_url', site_url('dashboard/post')); ?>
    <?php echo form_close(); ?>

</div><!-- #dsb_post_edit -->

<!-- Hidden area -->
<div class="hidden">

    <!-- Confirm  dialog when delete this file -------------------------------->
    <div id="warning" class="confirm-dialog">
        <p class="sub-label">
            When you upload new photo the old photo will delete automatically. Are you sure?
        </p><!-- .sub-label -->

        <div class="confirm-panel clearfix">
            <div id="confirm-okay" class="confirm-button danger">Let's delete and upload new</div>
            <div id="confirm-cancel" class="confirm-button" style="margin-left: 7px;">Cancel</div>
        </div><!-- confirm-button -->
    </div>
    <!-- #confirm -->

</div>
<!-- / -->

<?php echo js_asset('post.edit.js', 'Dashboard'); ?>

<?php
/* End of file edit.php */
/* Location: ./application/modules/Post/views/Dsb_Post/edit.php */
