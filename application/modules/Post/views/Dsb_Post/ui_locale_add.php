<?php
/**
 * UI_Locale_Add
 * 
 * @package Post
 * @subpackage Dsb_Post
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
?>

<div id="dsb_post_ui_locale_add">
    <?php echo form_open('dashboard/post/save'); ?>

    <div class="grid_8 alpha omega begin">
        <div class="section clearfix">
            <label for="post_name" class="div-label">Name:</label>
            <div class="metro-box">
                <?php echo form_input('post[name]', (empty($post['name']) ? NULL : $post['name']), 'id="post_name"'); ?>
            </div>
        </div><!-- .section -->
    </div>
    
    <div class="grid_8 alpha omega">
        <div class="section clearfix">
            <label for="metadata_net_weight" class="div-label">Net weight:</label>
            <div class="metro-box">
                <?php echo form_input('metadata[net_weight]', (empty($post['metadata']['net_weight']) ? NULL : $post['metadata']['net_weight']['value']), 'id="metadata_net_weight"'); ?>
            </div>
        </div><!-- .section -->
    </div>

    <div class="grid_8 alpha omega">
        <div class="section clearfix">
            <label for="metadata_size_of_box" class="div-label">Size of box:</label>
            <div class="metro-box">
                <?php echo form_input('metadata[size_of_box]', (empty($post['metadata']) ? NULL : $post['metadata']['size_of_box']['value']), 'id="metadata_size_of_box"'); ?>
            </div>
        </div><!-- .section -->
    </div>

    <div class="grid_8 alpha omega">
        <div class="section clearfix">
            <label for="metadata_weight" class="div-label">Weight W / bottle:</label>    
            <div class="metro-box">
                <?php echo form_input('metadata[weight]', (empty($post['metadata']['weight']) ? NULL : $post['metadata']['weight']['value']), 'id="metadata_weight"'); ?>
            </div>
        </div><!-- .section -->
    </div>

    <div class="grid_8 alpha omega">
        <div class="section clearfix">
            <label for="metadata_size_of_product" class="div-label">Size of product:</label>
            <div class="metro-box">
                <?php echo form_input('metadata[size_of_product]', (empty($post['metadata']['size_of_product']) ? NULL : $post['metadata']['size_of_product']['value']), 'id="metadata_size_of_product"'); ?>
            </div>
        </div><!-- .section -->
    </div>

    <div class="grid_8 alpha omega">
        <div class="section clearfix">
            <label for="metadata_amount_of_product" class="div-label">Amout of product / box:</label>    
            <div class="metro-box">
                <?php echo form_input('metadata[amount_of_product]', (empty($post['metadata']['amount_of_product']) ? NULL : $post['metadata']['amount_of_product']['value']), 'id="metadata_amount_of_product"'); ?>
            </div>
        </div><!-- .section -->
    </div>

    <div class="grid_8 alpha omega">
        <div class="section clearfix">
            <label for="post_content" class="div-label">Detail:</label>
            <?php echo form_textarea('post[content]', (empty($post['content']) ? NULL : $post['content']), 'id="post_content"'); ?>
        </div><!-- .section -->
    </div>

    <div class="grid_8 alpha omega">
        <div class="section clearfix">
            <label for="category" class="div-label">Category:</label>
            <?php echo form_dropdown(); ?>
        </div>
    </div>

    <div class="grid_8 alpha omega">
        <div class="section clearfix end">
            <div class="right">
                <a class="button submit"><?php echo (empty($post['id']) ? 'Save' : 'Update'); ?></a>
            </div>
        </div>
    </div>

    <?php echo form_hidden('post[id]', (empty($post['id']) ? NULL : $post['id'])); ?>
    <?php echo form_hidden('post[parent_id]', (empty($post['parent_id']) ? NULL : $post['parent_id'])); ?>
    <?php echo form_hidden('metadata[locale]', (empty($post['metadata']['locale']) ? 'en_US' : $post['locale'])); ?>

    <?php echo form_close(); ?>
</div><!-- #dsb_post_ui_locale_add -->

<?php
/* End of file ui_locale_add.php */
/* Location: ./application/modules/Post/views/Dsb_Post/ui_locale_add.php */