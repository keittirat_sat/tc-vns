<?php
/**
 * Add 
 * 
 * @package Recipe
 * @subpackage Dsb_Recipe
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
?>

<div id="dsb_post_add">

    <?php echo form_open('dashboard/post/save'); ?>

    <div class="container_12 content clearfix">
        <div class="content-header grid_12">
            <p class="sub-label super-big">
                Add
            </p>
        </div><!-- content-header -->

        <div class="grid_12 content-body">
            <div class="grid_4 alpha">

                <p class="sub-label big">Photo</p>
                <div class="big-thumb">
                    <?php if (!empty($post['image'][0])): ?>
                        <img src="/<?php echo $post['image'][0]['path'], $post['image'][0]['name']; ?>" alt="No image" width="292" />
                    <?php else: ?>
                        <img src="<?php echo image_asset_url('no_img.gif', 'Dashboard'); ?>" alt="No image" width="292" />
                    <?php endif; ?>
                    <input id="file_product_photo" type="hidden" name="file[id]" value="<?php echo (!empty($post['image'][0]) ? $post['image'][0]['id'] : NULL); ?>" />
                    <input id="post_parent_id" type="hidden" value="<?php echo $post['en_US']['id']; ?>" />
                </div><!-- .big-thumb -->

                <p class="sub-label big" style="margin-top: 35px;">Save</p>
                <div class="save">
                    <a id="post-publish" class="button">Publish</a>
                    <input id="post_status_publish" type="hidden" name="status" value="<?php echo PUBLISH; ?>" />

                    <a id="post-draft" class="button" style="margin-left: 7px;">Draft</a>
                    <input id="post_status_draft" type="hidden" name="status" value="<?php echo DRAFT; ?>" />
                </div>
            </div><!-- .grid_4 -->

            <div class="grid_8 omega">
                <p class="sub-label big">Content</p>
                <div class="locale-tab clearfix">
                    <ul>
                        <?php if (!empty($enable_locale)): ?>
                            <?php foreach ($enable_locale as $index => $value): ?>
                                <?php $selected = (!strcmp($value['sign'], 'en_US') ? ' selected ' : NULL); ?>
                                <?php $latest = ($index + 1 == count($enable_locale) ? ' latest ' : NULL); ?>
                                <li class="<?php echo $selected, $latest; ?> { sign: '<?php echo $value['sign']; ?>' }">
                                    <div class="flag flag-<?php echo $value['sign']; ?>"></div>
                                    <?php echo $value['name']; ?>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div><!-- .locale-tab -->

                <div class="clearfix">
                    <?php if (!empty($enable_locale)): ?>
                        <?php foreach ($enable_locale as $index => $value): ?>

                            <div id="locale-<?php echo $value['sign']; ?>" class="locale-content clearfix hide">
                                <div class="grid_8 alpha omega begin">
                                    <div class="section clearfix hide" style="padding: 7px;">
                                        <div class="message-box error hide"></div>
                                    </div><!-- .section -->
                                </div>

                                <div class="grid_8 alpha omega">
                                    <div class="section clearfix">
                                        <label class="div-label">Name (req):</label>
                                        <div class="metro-box">
                                            <?php echo form_input("post[{$value['sign']}][name]", (empty($post[$value['sign']]['name']) ? NULL : $post[$value['sign']]['name']), 'class="require"'); ?>
                                        </div>
                                    </div><!-- .section -->
                                </div>

                                <div class="grid_8 alpha omega">
                                    <div class="section clearfix">
                                        <label class="div-label">Ingredient:</label>
                                        <?php echo form_textarea("post[{$value['sign']}][content]", (empty($post[$value['sign']]['content']) ? NULL : $post[$value['sign']]['content'])); ?>
                                    </div><!-- .section -->
                                </div>

                                <div class="grid_8 alpha omega end">
                                    <div class="section clearfix">
                                        <label class="div-label">How to:</label>
                                        <?php echo form_textarea("metadata[{$value['sign']}][how_to]", (empty($post[$value['sign']]['metadata']['how_to']) ? NULL : $post[$value['sign']]['metadata']['how_to']['value'])); ?>
                                    </div><!-- .section -->
                                </div>
                            </div><!-- .locale-content -->

                            <?php echo form_hidden("post[{$value['sign']}][id]", $post[$value['sign']]['id']); ?>
                        <?php endforeach; ?>
                    <?php endif; ?>                            
                </div><!-- .clearfix -->
            </div><!-- .grid_8 -->
        </div><!-- .content-body -->
    </div><!-- .content -->

    <?php echo form_hidden('type', POST_RECIPE); ?>
    <?php echo form_hidden('return_url', site_url('dashboard/recipe')); ?>
    <?php echo form_close(); ?>

</div><!-- #dsb_post_add --> 

<?php echo js_asset('post.add.js', 'Dashboard'); ?>

<?php
/* End of  file add.php */
/* Location: ./application/modules/Post/views/Dsb_Recipe/add.php */
