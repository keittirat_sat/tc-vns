<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dsb_Category
 * 
 * @package Post
 * @subpackage Controllers
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
class Dsb_Category extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('Category');
    }

    public function index()
    {
        if (!Modules::run('User/Dsb_Auth/get_login_status', TRUE))
        {
            redirect('dashboard/login');
        }

        $enable_locale = Modules::run('Locale/Dsb_Locale/get_enable_locale');
        $this->template->set('enable_locale', $enable_locale);

        /* Get category list */
        $category_list = $this->get_category_list();

        foreach ($category_list as $index => $category)
        {
            if ($category['en_US']['id'] < 1)
            {
                unset($category_list[$index]);
            }
        }

        $this->template->set('category_list', $category_list);

        $this->template->set('title', 'Category');
        $this->template->load(NULL, 'dashboard');
    }

    public function edit($id)
    {
        if ($id < 1) return FALSE;

        $enable_locale = Modules::run('Locale/Dsb_Locale/get_enable_locale');
        $this->template->set('enable_locale', $enable_locale);

        /* Get category list */
        $category_list = $this->get_category_list();

        foreach ($category_list as $index => $category)
        {
            if ($category['en_US']['id'] < 1)
            {
                unset($category_list[$index]);
            }
        }

        $this->template->set('category_list', $category_list);

        /* Get category data */
        $category = $this->get_category($id);
        $this->template->set('category', $category);

        $this->template->set('title', 'Edit');
        $this->template->load(NULL, 'dashboard');
    }

    public function get_category($id, $sign = NULL)
    {
        $ss_uid = $this->session->userdata('uid');

        $parent = $this->Category->find($ss_uid, array(
            'id' => $id,
            'uid' => $ss_uid,
            'locale' => 'en_US',
            'parent_id' => 0)
        );

        $category = array();

        if (!empty($parent))
        {
            $parent = $parent[0];

            $category['en_US'] = $parent;

            $children = $this->Category->find($ss_uid, array('parent_id' => $id));

            if (!empty($children))
            {
                foreach ($children as $value)
                {
                    $category[$value['locale']] = $value;
                }
            }

            if (isset($sign))
            {
                return $category[$sign];
            }

            return $category;
        }

        return NULL;
    }

    public function get_category_list($sign = NULL)
    {
        $ss_uid = $this->session->userdata('uid');

        /* Get all parent category */

        $parent = $this->Category->find($ss_uid, array(
            'uid' => $ss_uid,
            'locale' => 'en_US',
            'parent_id' => 0)
        );

        if (!empty($parent))
        {
            $category_list = array();

            foreach ($parent as $value)
            {
                $category = $this->get_category($value['id'], $sign);
                array_push($category_list, $category);
            }

            return $category_list;
        }

        return NULL;
    }

    public function save()
    {
        $data = $this->input->post(NULL, TRUE);
        $ss_uid = $this->session->userdata('uid');

        $parent_id = (empty($data['en_US']['id']) ? NULL : $data['en_US']['id']);

        foreach ($data['category'] as $sign => $category)
        {
            $category['status'] = PUBLISH;
            $category['uid'] = $ss_uid;
            $category['locale'] = $sign;

            if (strcmp($sign, 'en_US'))
            {
                $category['parent_id'] = $parent_id;
            }

            $cat_id = $this->Category->save($category);

            /* Set parent id is en_US locale */

            if (!strcmp($sign, 'en_US') AND !isset($parent_id))
            {
                $parent_id = $cat_id;
            }
        }

        redirect('dashboard/category');
    }

    public function delete($id)
    {
        $ss_uid = $this->session->userdata('uid');
        $category = $this->get_category($id);

        if (!empty($category))
        {
            /* Get category children */

            foreach ($category as $sign => $value)
            {
                $this->Category->delete($value['id']);
            }

            $response = array(
                'status' => 'success',
                'message' => 'Delete category successfully.'
            );
        }
        else
        {
            $response = array(
                'status' => 'failed',
                'message' => 'Category not found.'
            );
        }

        $this->template->load('response', 'json', $response);
    }

}

/* End of file Dsb_Category.php */
/* Location: ./application/modules/Post/controlles/Dsb_Category.php */