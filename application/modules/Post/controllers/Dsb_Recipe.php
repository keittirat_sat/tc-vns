<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Recipe
 * 
 * @package Post
 * @subpackage Controllers
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */
class Dsb_Recipe extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Post');
    }

    public function index($page = 1)
    {
        if (!Modules::run('User/Dsb_Auth/get_login_status', TRUE))
        {
            redirect('dashboard/login');
        }
        
        $ss_uid = $this->session->userdata('uid');

        /* Get all post */

        $cond = array(
            'parent_id' => 0,
            'type' => POST_RECIPE
        );

        $product_list = $this->Post->find($ss_uid, $cond, ($page - 1) * 25, 25);
        $count_post = $this->Post->count($ss_uid, $cond);

        $pagination = get_pagination($page, $count_post, 25, 1, site_url('dashboard/post/index'));
        $this->template->set('pagination', $pagination);

        foreach ($product_list as &$product)
        {
            $category = Modules::run('Post/Dsb_Category/get_category', $product['cat_id'], 'en_US');
            $product['cat_name'] = (empty($category) ? NULL : $category['name']);

            $file = $this->Post->get_file_post($product['id']);
            $product['image'] = Modules::run('File/Dsb_File_Reader/get_file_detail', $file);
        }

        $this->template->set('product_list', $product_list);

        $this->template->set('title', 'All Recipe');
        $this->template->load(NULL, 'dashboard');
    }

    public function add()
    {
        $enable_locale = Modules::run('Locale/Dsb_Locale/get_enable_locale');
        $this->template->set('enable_locale', $enable_locale);

        $this->template->set('title', 'Add');
        $this->template->load(NULL, 'dashboard');
    }

    public function edit($id)
    {
        $ss_uid = $this->session->userdata('uid');
        $post = array();

        $parent = $this->Post->find($ss_uid, array(
            'id' => $id,
            'locale' => 'en_US',
            'type' => POST_RECIPE,
            'parent_id' => 0)
        );

        if (!empty($parent))
        {
            /* re-form array to $sign => $data */
            $post = array_merge($post, array('en_US' => $parent));

            /* Get file post */
            $file = $this->Post->get_file_post($id);
            $post['image'] = Modules::run('File/Dsb_File_Reader/get_file_detail', $file);

            /* Get another locale */
            $children = $this->Post->find($ss_uid, array('parent_id' => $parent['id']));

            foreach ($children as $data)
            {
                $post = array_merge($post, array($data['locale'] => $data));
            }

            $this->template->set('post', $post);

            $enable_locale = Modules::run('Locale/Dsb_Locale/get_enable_locale');
            $this->template->set('enable_locale', $enable_locale);

            $this->template->set('title', 'Edit');
            $this->template->load(NULL, 'dashboard');
        }
        else
        {
            /* An empty post */
            show_error('Post not found.', 404);
        }
    }

}

/* End of file Dsb_Recipe.php */
/* Location: ./application/modules/Post/controllers/Dsb_Recipe.php */