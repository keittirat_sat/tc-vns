<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dsb_Post
 * 
 * @package Post
 * @subpackage Controllers
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
class Dsb_Post extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Post');
    }

    public function index($page = 1)
    {
        if (!Modules::run('User/Dsb_Auth/get_login_status', TRUE))
        {
            redirect('dashboard/login');
        }

        $ss_uid = $this->session->userdata('uid');

        /* Get all post */

        $cond = array(
            'parent_id' => 0,
            'type' => POST_PRODUCT
        );

        $product_list = $this->Post->find($ss_uid, $cond, ($page - 1) * 25, 25);
        $count_post = $this->Post->count($ss_uid, $cond);

        $pagination = get_pagination($page, $count_post, 25, 1, site_url('dashboard/post/index'));
        $this->template->set('pagination', $pagination);

        foreach ($product_list as &$product)
        {
            $category = Modules::run('Post/Dsb_Category/get_category', $product['cat_id'], 'en_US');
            $product['cat_name'] = (empty($category) ? NULL : $category['name']);

            $file = $this->Post->get_file_post($product['id']);
            $product['image'] = Modules::run('File/Dsb_File_Reader/get_file_detail', $file);
        }

        $this->template->set('product_list', $product_list);

        $this->template->set('title', 'All Product');
        $this->template->load(NULL, 'dashboard');
    }

    public function save()
    {
        $data = $this->input->post(NULL, TRUE);
        $ss_uid = $this->session->userdata('uid');

        /* Save post data first */

        $parent_id = (empty($data['en_US']['id']) ? NULL : $data['en_US']['id']);
        $parent_price = 0;
        $parent_stock = 0;

        foreach ($data['post'] as $sign => $post)
        {
            $post['status'] = $data['status'];
            $post['cat_id'] = (empty($data['cat_id']) ? 0 : $data['cat_id']);
            $post['uid'] = $ss_uid;
            $post['locale'] = $sign;

            if (!strcmp($sign, 'en_US'))
            {
                if (empty($data['type']))
                {
                    $post['type'] = POST_PRODUCT;
                }
                else
                {
                    $post['type'] = $data['type'];
                }
            }
            else
            {
                if (empty($data['type']))
                {
                    $post['type'] = POST_PRODUCT;
                }
                else
                {
                    $post['type'] = $data['type'];
                }

                $post['parent_id'] = $parent_id;
            }

            $post_id = $this->Post->save($post);

            /* Set parent id is en_US locale */

            if (!strcmp($sign, 'en_US') AND !isset($parent_id))
            {
                $parent_id = $post_id;
            }

            /* Save metadata  */

            foreach ($data['metadata'][$sign] as $name => $value)
            {
                /* If metadata are exsiting system will replace and update them */

                if (!empty($value))
                {
                    $this->Post->set_metadata($post_id, $name, $value);
                }

                if (!strcmp($name, 'price') AND !strcmp($sign, 'en_US'))
                {
                    $parent_price = $value;
                }

                if (!strcmp($name, 'price') AND strcmp($sign, 'en_US'))
                {
                    $this->Post->set_metadata($post_id, $name, $parent_price);
                }

                if (!strcmp($name, 'stock') AND !strcmp($sign, 'en_US'))
                {
                    $parent_stock = $value;
                }

                if (!strcmp($name, 'stock') AND strcmp($sign, 'en_US'))
                {
                    $this->Post->set_metadata($post_id, $name, $parent_stock);
                }
            }
        }

        if (!empty($data['file']['id']))
        {
            $this->Post->set_file_post($parent_id, $data['file']['id']);
        }

        if (!empty($data['return_url']))
        {
            redirect($data['return_url']);
        }

        $response = array(
            'status' => 'success',
            'message' => 'Saving successfull.'
        );

        $this->template->load('response', 'ajax', $response);
    }

    public function promotion()
    {
        $ss_uid = $this->session->userdata('uid');
        $post_id = $this->input->post('id');

        $post = $this->Post->find($ss_uid, array('id' => $post_id));

        if (!empty($post))
        {
            $children = $this->Post->find($ss_uid, array('parent_id' => $post_id));

            if (empty($post['promotion']))
            {
                $this->Post->update($post_id, array('promotion' => 1));

                foreach ($children as $value)
                {
                    $this->Post->update($value['id'], array('promotion' => 1));
                }
            }
            else
            {
                $this->Post->update($post_id, array('promotion' => 0));

                foreach ($children as $value)
                {
                    $this->Post->update($value['id'], array('promotion' => 0));
                }
            }
        }
    }

    public function add()
    {
        $enable_locale = Modules::run('Locale/Dsb_Locale/get_enable_locale');
        $this->template->set('enable_locale', $enable_locale);

        $category_list = Modules::run('Post/Dsb_Category/get_category_list', 'en_US');
        $dropdown_list = array();

        if (!empty($category_list))
        {
            /* Create dropdown list */
            foreach ($category_list as $category)
            {
                $dropdown_list[$category['id']] = $category['name'];
            }
        }

        $this->template->set('dropdown_list', $dropdown_list);

        $this->template->set('title', 'Add');
        $this->template->load(NULL, 'dashboard');
    }

    public function edit($id)
    {
        $ss_uid = $this->session->userdata('uid');
        $post = array();

        $parent = $this->Post->find($ss_uid, array(
            'id' => $id,
            'locale' => 'en_US',
            'type' => POST_PRODUCT,
            'parent_id' => 0)
        );

        if (!empty($parent))
        {
            /* re-form array to $sign => $data */
            $post = array_merge($post, array('en_US' => $parent));

            /* Get file post */
            $file = $this->Post->get_file_post($id);
            $post['image'] = Modules::run('File/Dsb_File_Reader/get_file_detail', $file);

            /* Get another locale */
            $children = $this->Post->find($ss_uid, array('parent_id' => $parent['id']));

            foreach ($children as $data)
            {
                $post = array_merge($post, array($data['locale'] => $data));
            }

            $this->template->set('post', $post);

            $enable_locale = Modules::run('Locale/Dsb_Locale/get_enable_locale');
            $this->template->set('enable_locale', $enable_locale);

            $category_list = Modules::run('Post/Dsb_Category/get_category_list', 'en_US');
            $dropdown_list = array();

            if (!empty($category_list))
            {
                /* Create dropdown list */
                foreach ($category_list as $category)
                {
                    $dropdown_list[$category['id']] = $category['name'];
                }
            }

            $this->template->set('dropdown_list', $dropdown_list);

            $this->template->set('title', 'Edit');
            $this->template->load(NULL, 'dashboard');
        }
        else
        {
            /* An empty post */
            show_error('Post not found.', 404);
        }
    }

    public function delete($id = NULL)
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $id = $this->input->post('id');
        }

        /* Transform to array */
        if (!is_array($id))
        {
            $id = array($id);
        }

        if (empty($id)) return FALSE;

        $ss_uid = $this->session->userdata('uid');

        foreach ($id as $post_id)
        {
            /* Check permission */
            $parent = $this->Post->get($ss_uid, $post_id);

            if (!empty($parent))
            {
                /* Delete children */

                $children = $this->Post->find($ss_uid, array('parent_id' => $post_id));

                if (!empty($children))
                {
                    foreach ($children as $child)
                    {
                        $this->Post->delete($child['id']);
                    }
                }

                /* Delete file relation */

                $file_post = $this->Post->get_file_post($post_id);

                if (!empty($file_post))
                {
                    foreach ($file_post as $file)
                    {
                        Modules::run('File/Dsb_File_Writer/delete', $file['file_id']);
                    }
                }

                /* Delete self */

                $this->Post->delete($post_id);
            }
        }
    }

    public function reduce_stock($post_id, $quantity)
    {

        $parent = $this->dashboard->get_product($post_id);

        if (!empty($parent))
        {
            $parent = $parent[0];
            $stock = $parent['metadata']['stock']['value'];

            if ($stock >= $quantity)
            {
                $stock -= $quantity;
                $this->Post->set_metadata($post_id, 'stock', $stock);

                /* Update stock on children */
                $children = $this->Post->find(NULL, array('parent_id' => $post_id));

                if (!empty($children))
                {
                    foreach ($children as $c)
                    {
                        $this->Post->set_metadata($c['id'], 'stock', $stock);
                    }
                }

                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }

}

/* End of file Dsb_Post.php */
/* Location: ./application/modules/Post/controllers/Dsb_Post.php */
