<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dsb_Dashboard
 * 
 * @package Dashboard
 * @subpackage Controllers
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */
class Dsb_Dashboard extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if (!Modules::run('User/Dsb_Auth/get_login_status', TRUE))
        {
            redirect('dashboard/login');
        }

        redirect('dashboard/post');
        
        $this->template->set('title', 'Dashboard+');
        $this->template->load(NULL, 'dashboard');
    }

    public function login()
    {
        if (Modules::run('User/Dsb_Auth/get_login_status', TRUE))
        {
            redirect('dashboard');
        }

        $this->template->set('title', 'Login');
        $this->template->load(NULL, 'dashboard_auth');
    }

    public function logout()
    {
        if (!Modules::run('User/Dsb_Auth/get_login_status', TRUE))
        {
            redirect('dashboard/login');
        }

        $this->session->sess_destroy();

        set_cookie('uid', NULL, time() - (3600));
        set_cookie('token', NULL, time() - (3600));
        set_cookie('forgetmenot', NULL, time() - (3600));

        redirect('dashboard');
    }

}

/* End of file Dsb_Dashboard.php */
/* Location: ./application/modules/Dashboard/Dsb_Dashboard.php */
