<?php
/**
 * Index
 * 
 * @package Dashboard
 * @subpackage Dsb_Dashboard
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */
?>

<div id="dsb_dashboard_index">

    <div class="container_12 content clearfix">
        <div class="content-header grid_12">
            <p class="sub-label super-big">
                Overview
            </p>
        </div><!-- content-header -->
        
        <div class="grid_12 content-body">
            
        </div><!-- .content-body -->
    </div>

</div><!-- #dsb_dashboard_index -->

<?php
/* End of file index.php */
/* Location: ./application/modules/Dashboard/views/DSb_Dashboard/index.php */
