<?php
/**
 * Login
 * 
 * @package Dashboard
 * @subpackage Dsb_Dashboard
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
?>

<div id="dsb_dashboard_login" class="clearfix">
    <div class="container_12">
        
        <div class="grid_4 push_4 round-corner">

            <h1 class="cufon round-corner">Authentication Require</h1>

            <?php echo form_open('dashboard/auth/login'); ?>

            <div class="control-group">
                <div class="metro-box">
                    <?php echo form_input('username', NULL, 'id="username" placeholder="Username"'); ?>
                </div>
            </div>

            <div class="control-group">
                <div class="metro-box">
                    <?php echo form_password('password', NULL, 'id="password" placeholder="Password"'); ?>
                </div>
            </div>

            <div id="login_error" class="hide control-group message-box error">
                
            </div>
            
            <div class="control-group clearfix" style="margin-bottom: 9px;">
                <div class="left" style="height: 30px; line-height: 30px; color: #999;">
                    <div id="forgetmenot" class="left goog-checkbox { trigger: '#forgetmenot-label' }" style="margin-top: 8px;"></div>  
                    <div id="forgetmenot-label" class="left div-label inline { label: '#forgetmenot', name: 'forgetmenot' }">Remember Me</div>
                </div>

                <div class="right">
                    <a onclick="javascript: $('form').submit();" class="submit button">Submit</a>
                </div>
            </div>

            <?php echo form_close(); ?>

        </div>

    </div><!-- .container_12 -->
</div><!-- #dsb_dashboard_login -->

<?php echo js_asset('dsb.login.js', 'Dashboard'); ?>

<?php
/* End of file login.php */
/* Location: ./application/modules/Dashboard/views/Dsb_Dashboard/login.php */
