<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dsb_File_Reader
 * 
 * @package File
 * @subpackage Controllers
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */
class Dsb_File_Reader extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!Modules::run('User/Dsb_Auth/get_login_status', TRUE))
        {
            redirect('dashboard/login');
        }

        $this->load->model('File');
    }

    public function ui_explorer()
    {
        /* Default explorer option */
        $default_opt = array(
            'crop' => FALSE,
            'multiple' => FALSE,
            'ratio' => FALSE,
            'readonly' => FALSE,
            'writeonly' => FALSE
        );

        $opt = $this->input->post();

        /* Merge and set option */
        
        if(!empty($opt)) $default_opt = array_merge($default_opt, $opt);
        
        $this->template->set('opt', $default_opt);

        $this->template->load('Dsb_File_Reader/ui_explorer', 'ajax');
    }

    public function ui_browse($page = 1)
    {
        $opt = $this->input->post();
        $this->template->set('opt', $opt);

        $ss_uid = $this->session->userdata('uid');

        /* Get file data ---------------------------------------------------- */

        $cond = array(
            'parent_id' => 0,
            'type !=' => CHILDREN
        );

        $db_file = $this->File->find($ss_uid, $cond, ($page - 1) * 15, 15, 'desc');

        /* Group file by created date --------------------------------------- */

        $files = array();

        foreach ($db_file as $index => $file)
        {
            $timestamp = strtotime($file['created']);

            if (!empty($files[date('F d Y', $timestamp)]))
            {
                array_push($files[date('F d Y', $timestamp)], $file);
            }
            else
            {
                $files[date('F d Y', $timestamp)] = array();
                array_push($files[date('F d Y', $timestamp)], $file);
            }
        }

        $this->template->set('files', $files);

        /* Pagination ------------------------------------------------------- */

        $count = $this->File->count($ss_uid);
        $this->template->set('count', $count);

        $this->template->set('page', $page);
        $this->template->set('max', ceil($count / 15));

        $pagination = get_pagination($page, $count, 15, 1, '#browse');
        $this->template->set('pagination', $pagination);

        $this->template->load('Dsb_File_Reader/ui_browse', 'ajax');
    }

    public function ui_edit($id)
    {
        $opt = $this->input->post();
        $this->template->set('opt', $opt);

        $ss_uid = $this->session->userdata('uid');

        $db_file = $this->File->get($ss_uid, $id);

        if (!empty($db_file))
        {
            $this->template->set('file', $db_file);

            /* Get file detail ---------------------------------------------- */

            /* Get file resolution if image */
            if (preg_match('/image/', $db_file['type']))
            {
                $size = Modules::run('File/Dsb_Image/get_size', $db_file['path'] . $db_file['name']);
                $this->template->set('size', $size);
            }

            $this->template->load('Dsb_File_Reader/ui_edit', 'ajax');
        }
        else
        {
            $response = array(
                'status' => 'failed',
                'message' => 'File not found.'
            );

            $this->template->load('response', 'json', $response);
        }
    }

    public function get_file_detail($args)
    {
        if (is_array($args))
        {
            /* Param is a file array from function Post::get_file_post(); */

            $file = array();

            foreach ($args as $index => $value)
            {
                $db_file = $this->File->get(NULL, $value['file_id']);

                if (!empty($db_file))
                {
                    array_push($file, $db_file);
                }
                else
                {
                    unset($args[$index]);
                }
            }
        }
        else
        {
            /* Params is a file id */

            $file = $this->File->get(NULL, $args);
        }

        return $file;
    }

}

/* End of file Dsb_File_Reader.php */
/* Location: ./application/modules/File/controllers/Dsb_File_Reader.php */