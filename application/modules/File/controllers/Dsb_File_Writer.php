<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dsb_File_Writer
 * 
 * @package File
 * @subpackage Controllers
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
class Dsb_File_Writer extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!Modules::run('User/Dsb_Auth/get_login_status', TRUE))
        {
            redirect('dashboard/login');
        }

        $this->load->model('File');
    }

    public function save()
    {
        $year = date('Y');
        $month = date('m');
        $upload_dir = APPPATH . "uploads/{$year}/{$month}/";
        $ss_uid = $this->session->userdata('uid');

        // force create folder year 
        @mkdir(APPPATH . 'uploads/' . $year);

        // force create folder month
        @mkdir(APPPATH . 'uploads/' . $year . '/' . $month);

        if (isset($_FILES['file']))
        {
            /* Count all of file from filename */
            $total = count($_FILES['file']['name']);

            /* Return data */
            $data = array();

            /* Loop upload file at lease 1 time */
            for ($i = 0; $i < $total; $i++)
            {
                $name = $_FILES['file']['name'][$i];
                $tmp_name = $_FILES['file']['tmp_name'][$i];

                /* Get file extension */
                $ext = $this->get_file_ext($name);

                /* Set hash file name, prevent file name collision */
                $real_name = $name;
                $hash_name = md5(time() . $name) . '.' . $ext;

                /* Let's move file to upload folder */
                if (move_uploaded_file($tmp_name, $upload_dir . $hash_name))
                {
                    /* Get file type */
                    $type = get_mime_by_extension($upload_dir . $hash_name);

                    /* Save */
                    $record_id = $this->File->save(array(
                        'name' => $hash_name,
                        'real_name' => $real_name,
                        'extension' => $ext,
                        'path' => $upload_dir,
                        'type' => $type,
                        'status' => PUBLISH,
                        'uid' => $ss_uid)
                    );

                    /* Auto resize file if is image */
                    if (preg_match('/image/', $type))
                    {
                        Modules::run('File/Dsb_Image/create_original', $record_id);
                        Modules::run('File/Dsb_Image/create_thumbnail', $record_id);
                    }

                    array_push($data, array(
                        'id' => $record_id,
                        'path' => $upload_dir,
                        'name' => $hash_name,
                        'real_name' => $real_name,
                        'type' => $type
                    ));

                    $response = array(
                        'status' => 'success',
                        'message' => $data
                    );
                }
                else
                {
                    $response = array(
                        'status' => 'failed',
                        'message' => 'Something went wrong with your upload!'
                    );

                    break;
                }
            }
        }
        else
        {
            $response = array(
                'status' => 'failed',
                'message' => 'Something went wrong with your upload!'
            );
        }

        $this->template->load('response', 'json', $response);
    }

    public function get_file_ext($filename)
    {
        $ext = explode('.', $filename);
        $ext = array_pop($ext);

        return strtolower($ext);
    }

    public function ui_upload()
    {
        $opt = $this->input->post();
        $this->template->set('opt', $opt);

        $this->load->library('user_agent');
        $this->template->set('agent', $this->agent->browser());

        $this->template->load('Dsb_File_Writer/ui_upload', 'ajax');
    }

    public function delete($id, $post_id = NULL)
    {
        $ss_uid = $this->session->userdata('uid');
        $db_file = $this->File->get($ss_uid, $id);

        if (!empty($db_file))
        {
            $db_children = $this->File->find($ss_uid, array('parent_id' => $id));

            if (!empty($db_children))
            {
                /* Looping for remove file */
                foreach ($db_children as $children)
                {
                    /* Hard delete from HDD */
                    unlink($children['path'] . $children['name']);

                    /* Hard delete from database */
                    $this->File->delete($children['id']);
                }
            }

            /* Let's delete parent file */
            unlink($db_file['path'] . $db_file['name']);

            /* Also delete from database */
            $this->File->delete($db_file['id']);
            
            if(isset($post_id))
            {
                /* Also delete from file_post table */
                $this->File->delete_file_post($db_file['id'], $post_id);
            }
            
            $response = array(
                'status' => 'success',
                'message' => 'Delete file completed.'
            );
        }
        else
        {
            $response = array(
                'status' => 'failed',
                'message' => 'File not found.'
            );
        }

        $this->template->load('response', 'json', $response);
    }

}

/* End of file Dsb_File_Writer.php */
/* Location: ./application/modules/File/controllers/Dsb_File_Writer.php */
