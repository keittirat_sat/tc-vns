<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dsb_Image
 * 
 * @package File
 * @subpackage Controllers
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */
class Dsb_Image extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!Modules::run('User/Dsb_Auth/get_login_status', TRUE))
        {
            redirect('dashboard/login');
        }

        $this->load->model('File');
    }

    public function ui_crop_image($id = NULL)
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $id = $this->input->post('id');
            $screen = $this->input->post('screen');
            $mybox_width = $this->input->post('mybox_width');
        }

        $ss_uid = $this->session->userdata('uid');

        $db_file = $this->File->get($ss_uid, $id);

        if (!empty($db_file))
        {
            $this->template->set('file', $db_file);

            /* Get file size */
            $size = $this->get_size($db_file['path'] . $db_file['name']);

            $width = ($size[0] > $mybox_width ? $mybox_width - 30 : $size[0]);
            $height = ceil(($width * 100 / $size[0]) / 100 * $size[1]);

            $this->template->set('width', $width);
            $this->template->set('height', $height);

            $this->template->load('Dsb_Image/ui_crop_image', 'ajax');
        }
    }

    public function ui_crop($data = array())
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $data = $this->input->post();
        }

        if (!empty($data))
        {
            foreach ($data['message'] as $index => &$image)
            {
                if (preg_match('/image/', $image['type']))
                {
                    $image['size'] = $this->get_size($image['path'] . $image['name']);
                }
                else
                {
                    unset($data['message'][$index]);
                }
            }

            $this->template->set('data', $data['message']);
            $this->template->load('Dsb_Image/ui_crop', 'ajax');
        }
    }

    public function crop()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $data = $this->input->post();
            $db_file = $this->File->find_id($data['id']);

            if (!empty($db_file) AND (!empty($data['src_width']) OR !empty($data['src_height']) OR !empty($data['dst_width']) OR !empty($data['dst_height'])))
            {
                /* Fixed image are scalable */
                $response = $this->resize($db_file['name'], NULL, $db_file['path'], 0, 0, NULL, NULL, $data['src_width'], $data['src_height']);

                /* Crop */
                $response = $this->resize($db_file['name'], NULL, $db_file['path'], $data['src_x'], $data['src_y'], $data['dst_width'], $data['dst_height'], $data['dst_width'], $data['dst_height']);

                /* Re-create thumbnail */
                $response = $this->create_thumbnail($data['id']);
            }
            else
            {
                $response = array(
                    'status' => 'failed',
                    'message' => 'An error encounted.'
                );
            }

            $this->template->load('response', 'json', $response);
        }
    }

    public function create_original($id)
    {
        $ss_uid = $this->session->userdata('uid');
        $db_file = $this->File->get($ss_uid, $id);

        if (!empty($db_file))
        {
            $size = $this->get_size($db_file['path'] . $db_file['name']);

            if ($size[0] /* Width */ > 1024 OR $size[1] /* Height */ > 768)
            {
                /* Aspect ratio by width */
                if ($size[0] > 1024)
                {
                    $response = $this->resize($db_file['name'], NULL, $db_file['path'], 0, 0, NULL, NULL, 1024, NULL);
                }

                /* Aspect ratio by height */
                elseif ($size[1] > 1024)
                {
                    $response = $this->resize($db_file['name'], NULL, $db_file['path'], 0, 0, NULL, NULL, NULL, 1024);
                }
                else
                {
                    $response = array(
                        'status' => 'success',
                        'message' => 'File upload completed.'
                    );
                }
            }
            else
            {
                $response = array(
                    'status' => 'failed',
                    'message' => 'This image are smaller.'
                );
            }
        }
        else
        {
            $response = array(
                'status' => 'failed',
                'message' => 'File not found.'
            );
        }

        return $response;
    }

    public function create_thumbnail($id)
    {
        $ss_uid = $this->session->userdata('uid');
        $db_file = $this->File->get($ss_uid, $id);

        if (!empty($db_file))
        {
            $response = $this->resize($db_file['name'], "thumb-{$db_file['name']}", $db_file['path'], 0, 0, NULL, NULL, 200);

            if (!strcmp($response['status'], 'success'))
            {
                /* Update file children */
                $db_children = $this->File->find($ss_uid, array(
                    'name' => "thumb-{$db_file['name']}",
                    'parent_id' => $id,
                    'type' => CHILDREN
                        )
                );

                if (empty($db_children))
                {
                    /* Update children record */
                    $data = $db_file;

                    unset($data['id']);
                    unset($data['modified']);

                    $data['name'] = "thumb-{$db_file['name']}";
                    $data['parent_id'] = $id;
                    $data['type'] = CHILDREN;

                    $this->File->save($data);
                }
            }
        }
        else
        {
            $response = array(
                'status' => 'failed',
                'message' => 'File not found.'
            );
        }

        return $response;
    }

    private function resize($src_filename, $dst_filename, $filepath, $src_x = 0, $src_y = 0, $src_width = NULL, $src_height = NULL, $dst_width = NULL, $dst_height = NULL)
    {
        /* Replace original file */
        if (!isset($dst_filename))
        {
            $dst_filename = $src_filename;
        }

        /* Get file size */
        $size = $this->get_size($filepath . $src_filename);
        $org_width = $size[0];
        $org_height = $size[1];
        $org_ratio = $org_width / $org_height;

        if (!isset($src_width))
        {
            /* Set to default */
            $src_width = $org_width;
        }

        if (!isset($src_height))
        {
            /* Set to default */
            $src_height = $org_height;
        }

        if (isset($dst_width) AND !isset($dst_height))
        {
            /* Resize ratio by width */
            $dst_height = (int) round($dst_width / $org_ratio);
        }

        if (isset($dst_height) AND !isset($dst_width))
        {
            /* Resize ration by height */
            $dst_width = (int) round($dst_height * $org_ratio);
        }

        if (isset($dst_width))
        {
            (int) round($dst_width);
        }

        if (isset($dst_height))
        {
            (int) round($dst_height);
        }

        if (isset($dst_width) AND isset($dst_height))
        {
            /* Source image resource */
            $res = $this->get_resource($filepath . $src_filename);

            /* Destination image resource */
            $image = imagecreatetruecolor($dst_width, $dst_height);

            /* Set background transparent */
            imagesavealpha($image, TRUE);
            $color = imagecolorallocatealpha($image, 0x00, 0x00, 0x00, 127);
            imagefill($image, 0, 0, $color);

            /* Copy image */
            imagecopyresampled($image, $res, 0, 0, $src_x, $src_y, $dst_width, $dst_height, $src_width, $src_height);

            /* Create new image and move to destination folder ($filepath) */
            $this->moveto($image, $dst_filename, $filepath);

            $response = array(
                'status' => 'success',
                'message' => 'Successfully resize.'
            );
        }
        else
        {
            $response = array(
                'status' => 'failed',
                'message' => 'Couldn\'t determind image size.'
            );
        }

        return $response;
    }

    private function get_resource($fullpath /* Include file name */)
    {
        $ext = Modules::run('File/Dsb_File_Writer/get_file_ext', $fullpath);
        $res = NULL;

        switch ($ext)
        {
            case 'jpg':
            case 'jpeg':
                $res = imagecreatefromjpeg($fullpath);
                break;
            case 'png':
                $res = imagecreatefrompng($fullpath);
                break;
            case 'gif':
                $res = imagecreatefromgif($fullpath);
                break;
            default: break;
        }

        return $res;
    }

    private function moveto($image /* New image resource */, $dst_filename, $filepath)
    {
        $ext = Modules::run('File/Dsb_File_Writer/get_file_ext', $dst_filename);

        switch ($ext)
        {
            case 'jpg':
            case 'jpeg':
                imagejpeg($image, $filepath . $dst_filename);
                break;
            case 'png':
                imagepng($image, $filepath . $dst_filename);
                break;
            case 'gif':
                imagegif($image, $filepath . $dst_filename);
                break;
            default: break;
        }
    }

    public function get_size($filepath)
    {
        return getimagesize($filepath);
    }

}

/* End of file Dsb_Image.php */
/* Location: ./application/modules/File/controllers/Dsb_Image.php */