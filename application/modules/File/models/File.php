<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @package File
 * @subpackage Models
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
class File extends MY_Model
{

    var $table = 'file';

    public function delete_file_post($file_id, $post_id)
    {
        $this->db->where('file_id', $file_id);
        $this->db->where('post_id', $post_id);
        
        $this->db->delete('file_post');
    }

}

/* End of file File.php */
/* Location: ./application/modules/File/models/File.php */