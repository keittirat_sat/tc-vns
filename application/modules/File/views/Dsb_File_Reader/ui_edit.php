<?php
/**
 * UI_Edit
 * 
 * @package File
 * @subpackage Dsb_File_Reader
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */
?>

<div id="dsb_file_reader_ui_edit">

    <div class="edit-panel clearfix">
        <div class="left">
            <a class="submit button button-back"><div class="icon-arrow-left" style="margin-right: 8px;"></div> Back</a>
        </div>

        <div class="right">
            <a class="submit button button-delete { id: <?php echo $file['id']; ?> }"><div class="icon-trash" style="margin-right: 10px;"></div>Delete</a>
        </div>
    </div><!-- .edit-panel -->

    <div class="edit-body clearfix">
        <?php if(preg_match('/image/', $file['type'])): ?>
            <img class="edit-thumb" src="/<?php echo $file['path'], 'thumb-', $file['name']; ?>" alt="<?php echo $file['real_name']; ?>" />
        <?php else: ?>
            <img class="edit-thumb" width="200" src="<?php echo image_asset_url('no_img.gif', 'Dashboard'); ?>" alt="<?php echo $file['real_name']; ?>" />
        <?php endif; ?>

        <div class="edit-detail">
            <div class="clearfix">
                <label>Name:</label>
                <p class="edit-disabled"><?php echo ellipsize($file['real_name'], 30, .5); ?></p>
            </div>

            <div class="clearfix">
                <label>Type:</label>
                <p class="edit-disabled"><?php echo $file['type']; ?></p>
            </div>

            <?php if(isset($size)): ?>
                <div class="clearfix">
                    <label>Resolution:</label>
                    <p class="edit-disabled"><?php echo $size[0], ' x ', $size[1]; ?> pixels</p>
                </div>
            <?php endif; ?>

            <div class="clearfix">
                <label>Created:</label>
                <p class="edit-disabled">
                    <?php $timestamp = strtotime($file['created']); ?>
                    <?php echo date('\o\n F m Y - H:i', $timestamp); ?>
                </p>
            </div>

            <?php if(strcmp($file['created'], $file['modified'])): ?>
                <div class="clearfix">
                    <label>Modified:</label>
                    <p class="edit-disabled">
                        <?php $timestamp = strtotime($file['modified']); ?>
                        <?php echo date('\o\n F m Y - H:i', $timestamp); ?>
                    </p>
                </div>
            <?php endif; ?>

            <div class="clearfix">
                <label for="url" class="left">URL</label>
                <div class="right">
                    <div class="metro-box">
                        <input type="text" id="url" value="<?php echo site_url() . $file['path'] . $file['name']; ?>" />  
                    </div>
                </div>
            </div>
        </div><!-- .edit-detail -->
    </div><!-- .edit-body -->
</div>

<!-- Hidden area -->
<div class="hidden">

    <!-- Confirm  dialog when delete this file -------------------------------->
    <div id="confirm-delete" class="confirm-dialog">
        <p class="sub-label">
            Are you sure you want to permanently delete this file?
        </p><!-- .sub-label -->

        <div class="confirm-panel clearfix">
            <div id="confirm-okay" class="confirm-button danger { id: <?php echo $file['id']; ?> }">Confirm</div>
            <div id="confirm-cancel" class="confirm-button" style="margin-left: 7px;">Cancel</div>
        </div><!-- confirm-button -->
    </div>
    <!-- #confirm -->

</div>
<!-- / -->

<?php echo js_asset('file.ui_edit.js', 'Dashboard'); ?>

<?php
/* End of file ui_edit.php */
/* Location: ./application/modules/File/views/Dsb_File_Reader/ui_edit.php */