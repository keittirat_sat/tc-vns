<?php
/**
 * UI_Explorer
 * 
 * @package File
 * @subpackage Dsb_File_Reader
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
?>

<div id="dsb_file_reader_ui_explorer" class="mybox">
    <div class="mybox-header">
        <h1 class="title cufon">File Browser</h1>
        <div class="mybox-close icon-remove"></div>
    </div><!-- .mybox-header -->

    <div class="divider solid soft" style="margin: 0;"></div>

    <div class="mybox-body clearfix">
        <ul class="sidebar">
            <?php if (!$opt['readonly']): ?>
                <li class="sidebar-upload selected"><a href="#upload">Upload</a></li>
            <?php endif; ?>

            <?php if (!$opt['writeonly']): ?>
                <li class="sidebar-browse"><a href="#browse/1">Uploaded Files</a></li>
            <?php endif; ?>
        </ul><!-- .sidebar -->

        <div class="mybox-response">
        </div><!-- .mybox-response -->
    </div><!-- .mybox-body -->

    <div class="hidden">
        <?php echo form_open(NULL, 'class="opt"', $opt); ?>
        <?php echo form_close(); ?>
    </div>
</div><!-- #dsb_file_reader_ui_explorer -->

<?php echo js_asset('underscore-min.js'); ?>
<?php echo js_asset('backbone-min.js'); ?>
<?php echo js_asset('file.ui_explorer.js', 'Dashboard'); ?>

<?php
/* End of file ui_explorer.php */
/* Location: ./application/modules/File/views/Dsb_File_Reader/ui_explorer.php */
