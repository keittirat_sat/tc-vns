<?php
/**
 * UI_Browse
 * 
 * @package File
 * @subpackage Dsb_File_Reader
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
?>

<div id="dsb_file_reader_ui_browse">

    <div class="browse-panel clearfix">
        <p class="sub-label"><?php echo $count; ?> Files total <?php if ($max): ?>(<?php echo $page; ?> of <?php echo $max; ?>)<?php endif; ?></p>
        <ul class="right page">
            <?php echo $pagination; ?>
        </ul>
    </div>

    <div class="browse-cat clearfix">
        <?php foreach ($files as $created => $file): ?>
            <div class="clear"></div>
            <p class="cat-label"><?php echo $created; ?></p>
            <div class="thumb-inner clearfix">
                <?php foreach ($file as $f): ?>
                    <div class="thumb-wrapper">
                        <p class="title">
                            <a href="#edit/<?php echo $f['id']; ?>"><?php echo ellipsize($f['real_name'], 30, 0.5); ?></a>
                        </p>

                        <a href="#edit/<?php echo $f['id']; ?>">
                            <?php if (preg_match('/image/', $f['type'])): ?>
                                <img class="thumb" src="/<?php echo $f['path'], 'thumb-' . $f['name']; ?>" alt="<?php echo $f['real_name']; ?>" />
                            <?php else: ?>
                                <img class="thumb" src="<?php echo image_asset_url('no_img.gif', 'Dashboard') ?>" alt="<?php echo $f['real_name']; ?>" />
                            <?php endif; ?>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
    </div><!-- .browse-cat -->

</div><!-- #dsb_file_reader_ui_browse -->

<?php
/* End of file ui_browse.php */
/* Location: ./application/modules/File/views/Dsb_File_Reader/ui_browse.php */