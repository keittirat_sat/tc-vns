<?php
/**
 * UI_File_Upload
 * 
 * @package File
 * @subpackage Dsb_File_Reader
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */
?>

<div id="dsb_file_writer_ui_upload">
    <div id="dropbox" <?php if (!strcmp($agent, 'Internet Explorer')): ?>disabled="disabled"<?php endif; ?>>
        <div class="align-center">
            <?php if (!strcmp($agent, 'Internet Explorer')): ?>
                <div class="sub-label super-big">Sorry, this browser couldn't supported.</div>
                <div class="sub-label">Try to use <a href="https://www.google.com/intl/en/chrome/browser/">Google Chrome</a> or <a href="http://www.mozilla.org/en-US/firefox/new/">Mozilla Firefox</a></div>
            <?php else: ?>
                <div class="sub-label super-big">Drag file to here</div>
                <div class="sub-label small" style="margin-top: 15px;">Or, if your prefer...</div>
                <a class="button" style="margin-top: 5px;">Select file from your computer</a>
            <?php endif; ?>
        </div>
    </div><!-- #dropbox -->

    <div class="hidden">
        <?php echo form_open_multipart('dashboard/file/writer/save'); ?>
        <input type="file" name="file[]" <?php echo ($opt['multiple'] ? 'multiple' : NULL); ?> />
        <?php echo form_close(); ?>
    </div>  

    <div id="progress" class="hide align-center">
        <h2 class="sub-label">Uploading</h2>
        <p class="sub-label small">This method will take a little bit time, please wait while uploading</p>
        <p class="sub-label small progress-counter message bold">( 0% )</p>
        <div class="progress progress-striped active">
            <div class="bar" style="width: 0%;"></div>
        </div>
    </div><!-- #progress -->

    <?php if ($opt['crop']): ?>
        <div id="crop" class="align-center">
            <div class="hide"></div>
        </div><!-- #crop -->
    <?php endif; ?>
</div><!-- #dsb_file_writer_ui_file_upload -->

<script type="text/javascript">
    var ratio = <?php echo ($opt['ratio'] ? $opt['ratio'] : 'null'); ?>;
</script>

<?php echo js_asset('jquery.form.js'); ?>
<?php echo js_asset('file.ui_upload.js', 'Dashboard'); ?>

<?php
/* End of file ui_upload.php */
/* Location: ./application/modules/File/views/Dsb_File_Writer/ui_upload.php */