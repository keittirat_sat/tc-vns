<?php
/**
 * UI_Crop_Image
 * 
 * @package File
 * @subpackage Dsb_Image 
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */
?>

<div id="dsb_image_ui_crop_image" class="align-center">
    <img class="crop-image" src="/<?php echo $file['path'], $file['name']; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" alt="<?php echo $file['real_name']; ?>" />
</div><!-- #dsb_image_ui_crop_image -->

<?php
/* End of file ui_crop_image.php */
/* Location: ./application/modules/File/views/Dsb_Image/ui_crop_image.php */
