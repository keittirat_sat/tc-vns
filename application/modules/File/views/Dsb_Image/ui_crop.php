<?php
/**
 * UI_Crop
 * 
 * @package Image
 * @subpackage Dsb_Image
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
?>

<div id="dsb_image_ui_crop">

    <?php if (!empty($data)): ?>
        <div class="crop-panel clearfix">
            <p class="sub-label">Drag your mouse over the image to crop and resize.</p>

            <div class="right crop-finish hide">
                <a class="submit button">Finish</a>
            </div>

            <?php echo form_open('dashboard/file/image/crop', 'id="form-crop" class="right"'); ?>
            <input type="hidden" name="id" id="id" />
            <input type="hidden" name="src_x" id="src_x" value="0" />
            <input type="hidden" name="src_y" id="src_y" value="0" />
            <input type="hidden" name="src_width" id="src_width" />
            <input type="hidden" name="src_height" id="src_height" />
            <input type="hidden" name="dst_width" id="dst_width" />
            <input type="hidden" name="dst_height" id="dst_height" />
            <a onclick="javascript: $('#dsb_image_ui_crop form').submit();" class="submit button">Crop</a>

            <?php if (isset($modifed)): ?>
                <?php echo form_hidden('modified', TRUE); ?>
            <?php endif; ?>

            <?php echo form_close(); ?>
        </div>

        <div class="crop-body"></div>

        <ul class="group-thumb clearfix">
            <?php foreach ($data as $index => $image): ?>
                <li class="<?php echo (!$index ? 'selected' : NULL); ?> { id: <?php echo $image['id']; ?> }">
                    <img height="50" src="/<?php echo $image['path'], 'thumb-' . $image['name']; ?>" alt="<?php echo $image['real_name']; ?>" />
                </li>
            <?php endforeach; ?>
        </ul><!-- .group-thumb -->
    <?php else: ?>
        <div class="crop-panel clearfix">
            <p class="sub-label">Can't crop or resize, this file is not image!</p>

            <div class="right crop-finish">
                <a class="submit button">Finish</a>
            </div>
        </div>
    <?php endif; ?>

</div><!-- #dsb_image_ui_crop -->

<?php echo css_asset('jquery.Jcrop.min.css', 'Jcrop'); ?>
<?php echo js_asset('jquery.Jcrop.min.js', 'Jcrop'); ?>

<?php echo js_asset('image.ui_crop.js', 'Dashboard'); ?>

<?php
/* End of file ui_crop.php */
/* Location: ./application/modules/File/views/Dsb_Image/ui_crop.php */