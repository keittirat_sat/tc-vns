<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dsb_Page
 * 
 * @package Page
 * @subpackage Controllers
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
class Dsb_Page extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Page');
    }

    public function index()
    {
        $this->template->load(NULL, 'dashboard');
    }

    public function slide()
    {
        $slide = $this->dashboard->get_slide();
        $this->template->set('slide', $slide);

        $this->template->set('title', 'Slide');
        $this->template->load(NULL, 'dashboard');
    }

    public function save_slide()
    {
        $data = $this->input->post(NULL, TRUE);

        if (isset($data['slide_sorting']))
        {
            /* Reorder existing slide */

            $slide = array();

            foreach ($data['slide'] as $value)
            {
                array_push($slide, $value);
            }

            $this->dashboard->set_option('slide', json_encode($slide));
        }
        else
        {
            $slide = $this->dashboard->get_option('slide');
            $slide = json_decode($slide, TRUE);

            /* Save a new slide */

            if (empty($slide))
            {
                /* Create new */
                $this->dashboard->set_option('slide', json_encode(array(
                            array(
                                'file_id' => $data['file_id'],
                                'content' => $data['content'])
                                )
                        )
                );
            }
            else
            {
                $new = array(
                    array(
                        'file_id' => $data['file_id'],
                        'content' => $data['content']
                    )
                );

                foreach ($slide as $value)
                {
                    array_push($new, $value);
                }

                $this->dashboard->set_option('slide', json_encode($new));
            }
        }

        redirect(site_url('dashboard/page/slide'));
    }

    public function delete_slide($file_id)
    {
        $slide = $this->dashboard->get_option('slide');
        $slide = json_decode($slide, TRUE);

        foreach ($slide as $index => $value)
        {
            if ($value['file_id'] == $file_id)
            {
                unset($slide[$index]);
                break;
            }
        }

        $this->dashboard->set_option('slide', json_encode($slide));

        Modules::run('File/Dsb_File_Writer/delete', $file_id);
    }

    public function promotion()
    {
        $enable_locale = Modules::run('Locale/Dsb_Locale/get_enable_locale');
        $this->template->set('enable_locale', $enable_locale);

        $promotion_cover = $this->dashboard->get_option('promotion_cover');

        if (!empty($promotion_cover))
        {
            $promotion_cover = Modules::run('File/Dsb_File_Reader/get_file_detail', $promotion_cover);
        }

        $this->template->set('promotion_cover', $promotion_cover);

        $promotion_index = json_decode($this->dashboard->get_option('promotion_index'), TRUE);
        
        $pattern = array('/&gt;/', '/&lt;/');
        $replace = array('>', '<');
                
        foreach($promotion_index as $locale => $value)
        {
            $promotion_index[$locale] = preg_replace($pattern, $replace, $value);
        }
        
        $this->template->set('promotion_index', $promotion_index);

        $this->template->set('title', 'Promotion');
        $this->template->load(NULL, 'dashboard');
    }

    public function save_promotion()
    {
        $data = $this->input->post(NULL, TRUE);
        
        if (!empty($data))
        {
            foreach ($data as $name => $value)
            {
                if (is_array($value))
                {  
                    $value = json_encode($value);
                }

                $this->dashboard->set_option($name, $value);
            }
        }
    }

    public function technology()
    {
        $enable_locale = Modules::run('Locale/Dsb_Locale/get_enable_locale');
        $this->template->set('enable_locale', $enable_locale);

        $cond = array(
            'parent_id' => 0,
            'type' => POST_TECHNOLOGY
        );

        $technology_list = $this->Page->find(NULL, $cond, NULL, NULL, 'desc');

        foreach ($technology_list as &$technology)
        {
            $file = $this->Page->get_file_post($technology['id']);
            $technology['image'] = Modules::run('File/Dsb_File_Reader/get_file_detail', $file);
        }

        $this->template->set('technology_list', $technology_list);

        $this->template->set('title', 'Technology');
        $this->template->load(NULL, 'dashboard');
    }

    public function advertisement()
    {
        $advertisement_list = $this->dashboard->get_option('advertisement');
        $advertisement_list = json_decode($advertisement_list, TRUE);

        if (!empty($advertisement_list))
        {
            foreach ($advertisement_list as &$advertisement)
            {
                $file = Modules::run('File/Dsb_File_Reader/get_file_detail', $advertisement['file_id']);
                $advertisement['image'] = $file;
            }
        }

        $this->template->set('advertisement_list', $advertisement_list);

        $this->template->set('title', 'Advertisment');
        $this->template->load(NULL, 'dashboard');
    }

    public function save_advertisement()
    {
        $data = $this->input->post(NULL, TRUE);

        $advertisement = $this->dashboard->get_option('advertisement');
        $advertisement = json_decode($advertisement, TRUE);

        $new = array(
            array(
                'file_id' => $data['file_id'],
                'URL' => $data['URL']
            )
        );

        if (!empty($advertisement))
        {
            foreach ($advertisement as $value)
            {
                array_push($new, $value);
            }
        }

        $this->dashboard->set_option('advertisement', json_encode($new));

        redirect('dashboard/page/advertisement');
    }

    public function delete_advertisement()
    {
        $index = $this->input->post('index');

        $advertisement_list = $this->dashboard->get_option('advertisement');
        $advertisement_list = json_decode($advertisement_list, TRUE);

        $advertisement = $advertisement_list[$index];

        if (!empty($advertisement['file_id']))
        {
            Modules::run('File/Dsb_File_Writer/delete', $advertisement['file_id']);
        }
        
        unset($advertisement_list[$index]);
        
        $ads = array();
        
        foreach($advertisement_list as $value)
        {
            array_push($ads, $value);
        }
        
        $this->dashboard->set_option('advertisement', json_encode($ads));
    }

}

/* End of file Dsb_Page.php */
/* Location: ./application/modules/Page/controllers/Dsb_Page.php */