<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Page
 * 
 * @package Page
 * @subpackage Controllers
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
class Page extends MY_Model
{

    var $table = 'post';

    public function get_file_post($post_id, $file_id = NULL, $offset = NULL, $limit = NULL)
    {
        $this->db->where('post_id', $post_id);

        if (isset($file_id))
        {
            $this->db->where('file_id', $file_id);
        }

        if (isset($offset) AND isset($limit))
        {
            $this->db->limit($limit, $offset);
        }

        $result = $this->db->get('file_post')->result_array();

        if (isset($file_id))
        {
            return (!empty($result) ? $result[0] : NULL);
        }

        return $result;
    }

}
