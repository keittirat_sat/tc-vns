<?php
/**
 * Technology
 * 
 * @package Page
 * @subpackage Technology 
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */
?>

<div id="dsb_page_technology">

    <div class="container_12 content clearfix">
        <div class="content-header grid_12">
            <p class="sub-label super-big">
                Technology
            </p>
        </div><!-- content-header -->

        <div class="content-body grid_12">

            <?php if (empty($technology_list) OR count($technology_list) < 4): ?>

                <?php echo form_open('dashboard/post/save', 'class="clearfix"'); ?>

                <div class="grid_12 alpha omega">
                    <p class="sub-label big">Add</p>
                </div>

                <div class="grid_4 alpha">
                    <div class="big-thumb">
                        <?php if (!empty($post['image'][0])): ?>
                            <img src="/<?php echo $post['image'][0]['path'], $post['image'][0]['name']; ?>" alt="No image" width="292" />
                        <?php else: ?>
                            <img src="<?php echo image_asset_url('no_img.gif', 'Dashboard'); ?>" alt="No image" width="292" />
                        <?php endif; ?>
                        <input id="file_product_photo" type="hidden" name="file[id]" />
                    </div><!-- .big-thumb -->

                </div><!-- .grid_4 -->

                <div class="grid_8 omega">
                    <div class="locale-tab clearfix">
                        <ul>
                            <?php if (!empty($enable_locale)): ?>
                                <?php foreach ($enable_locale as $index => $value): ?>
                                    <?php $selected = (!strcmp($value['sign'], 'en_US') ? ' selected ' : NULL); ?>
                                    <?php $latest = ($index + 1 == count($enable_locale) ? ' latest ' : NULL); ?>
                                    <li class="<?php echo $selected, $latest; ?> { sign: '<?php echo $value['sign']; ?>' }">
                                        <div class="flag flag-<?php echo $value['sign']; ?>"></div>
                                        <?php echo $value['name']; ?>
                                    </li>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </ul>
                    </div><!-- .locale-tab -->

                    <div class="clearfix">
                        <?php if (!empty($enable_locale)): ?>
                            <?php foreach ($enable_locale as $index => $value): ?>
                                <div id="locale-<?php echo $value['sign']; ?>" class="locale-content clearfix hide">
                                    <div class="grid_8 alpha omega begin">
                                        <div class="section clearfix hide" style="padding: 7px;">
                                            <div class="message-box error hide"></div>
                                        </div><!-- .section -->
                                    </div>

                                    <div class="grid_8 alpha omega">
                                        <div class="section clearfix" style="padding-top: 10px;">
                                            <label class="div-label">Title (req):</label>
                                            <div class="metro-box">
                                                <?php echo form_input("post[{$value['sign']}][name]", NULL, 'class="require"'); ?>
                                            </div>
                                        </div><!-- .section -->
                                    </div>

                                    <div class="grid_8 alpha omega end">
                                        <div class="section clearfix" style="padding-top: 10px;">
                                            <label class="div-label">Detail:</label>
                                            <?php echo form_textarea("post[{$value['sign']}][content]"); ?>
                                        </div><!-- .section -->
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div><!-- .grid_8 -->

                <div class="grid_12 alpha omega" style="margin-top: 10px; border-top: 1px solid #ccc; padding-top: 10px;">
                    <div class="save">
                        <a id="post-publish" class="button">Save</a>
                        <input id="post_status_publish" type="hidden" name="status" value="<?php echo PUBLISH; ?>" />
                    </div>
                </div><!-- .grid_12 -->

                <?php echo form_hidden('type', POST_TECHNOLOGY); ?>
                <?php echo form_hidden('return_url', site_url('dashboard/page/technology')); ?>
                <?php echo form_close(); ?>

            <?php else: ?>

                <div class="grid_12 alpha omega">
                    <p class="sub-label">Technology are limit. You can remove it and upload new.</p>
                </div>

            <?php endif; ?>
        </div><!-- .content-body -->

        <div class="technology-list">
            <div class="grid_12">
                <p class="sub-label big" style="margin-top: 35px;">Technology List</p>
            </div>

            <?php if (!empty($technology_list)): ?>
                <?php foreach ($technology_list as $technology): ?>
                    <div class="grid_3">
                        <?php if (!empty($technology['image'][0])): ?>
                            <img src="/<?php echo $technology['image'][0]['path'], 'thumb-', $technology['image'][0]['name']; ?>" width="220" height="220" />
                        <?php else: ?>
                            <img src="<?php echo image_asset_url('no_img.gif', 'Dashboard'); ?>" width="220" height="220" />
                        <?php endif; ?>

                        <div class="technology-panel clearfix">
                            <div class="technology-delete { post_id: <?php echo $technology['id']; ?> }" style="cursor: pointer;">
                                <div class="icon-trash"></div>
                                Delete
                            </div>
                        </div>

                        <div class="technology-detail">
                            <p class="sub-label"><?php echo $technology['name']; ?></p>
                            <p style="margin-top: 3px;"><?php echo $technology['content']; ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div><!-- #dsb_page_technology -->

<!-- Hidden area -->
<div class="hidden">

    <!-- Confirm  dialog when delete this file -------------------------------->
    <div id="warning" class="confirm-dialog">
        <p class="sub-label">
            Are you sure you want to permanently delete this technology?
        </p><!-- .sub-label -->

        <div class="confirm-panel clearfix">
            <div id="confirm-okay" class="confirm-button danger">Confirm</div>
            <div id="confirm-cancel" class="confirm-button" style="margin-left: 7px;">Cancel</div>
        </div><!-- confirm-button -->
    </div>
    <!-- #confirm -->

</div>
<!-- / -->

<script type="text/javascript">
    $(function()
    {
        /* Global ----------------------------------------------------------- */
        
        var global = {};
        
        /* Product photo ---------------------------------------------------- */
        
        $('.big-thumb img').click(function()
        {
            var self = this;
            
            explorer(
            {
                crop: true,
                ratio: 1,
                writeonly: true
            },
            {
                onCropped: function(event, args)
                {
                    var id = $('#file_product_photo').val();
                    
                    /* Remove old file uploaded */
                    if(id != '')
                    {
                        $.post('/dashboard/file/writer/delete/' + id);
                    }
                    
                    var image = args['message'][0];
                    
                    $(self).attr('src', '/' + image['path'] + image['name']);
                    $('#file_product_photo').val(image['id']);

                    explorer({
                        close: true
                    });
                }
            });
        });
        
        /* Multiple locale -------------------------------------------------- */
        
        $('#dsb_page_technology .locale-tab li').click(function() 
        {   
            var sign = $(this).metadata().sign;
            set_classname($(this), 'selected');
            set_classname($('#locale-' + sign), 'selected');
        });
        
        $('#dsb_page_technology .locale-tab li.selected').trigger('click');
        
        /* Form action ------------------------------------------------------ */
        
        $('#post-publish').click(function()
        {
            $('#post_status_draft').remove();
            $('#dsb_page_technology form').submit();
        });
       
        $('#dsb_page_technology form').live('submit', function()
        {
            var required = true;
            
            for(var i = 0; i < $('.require').length; i++)
            {
                if($('.require').eq(i).val() == '')
                {                    
                    required = false;
                    break;
                }
            }
            
            if(required)
            {
                return true;
            }
            else
            {
                $('.message-box.error').parent().show();
                $('.message-box.error').text('Please input data on require field (req).').show();
                return false;
            }
        });
        
        /* Confirm ---------------------------------------------------------- */
        
        $('.technology-delete').click(function()
        {
            block_ui(null, $('#warning')); 
            
            var post_id = $(this).metadata().post_id;
            
            global.post_id = post_id;
        });
        
        $('#confirm-okay').click(function()
        { 
            if(global.post_id != '')
            {
                $.post('/dashboard/post/delete', { id: global.post_id }, function(response)
                {
                    window.location.reload();
                });
            }
        });
        
        $('#confirm-cancel').click(function()
        {
            unblock_ui();
        });
    });
</script>

<?php
/* End of file technology.php */
/* Location: ./application/modules/Pages/views/Dsb_Page/technology.php */