<?php
/**
 * Advertisment
 * 
 * @package Page
 * @subpackage Dsb_Page
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
?>

<div id="dsb_page_advertisement">
    <div class="container_12 content clearfix">
        <div class="content-header grid_12">
            <p class="sub-label super-big">
                Advertisement
            </p>
        </div><!-- content-header -->

        <div class="content-body grid_12">

            <?php if (empty($advertisement_list) OR count($advertisement_list) < 3): ?>
                <?php echo form_open('dashboard/page/save/advertisement', 'class="clearfix"'); ?>

                <p class="sub-label big">Add</p>
                <div class="add-advertisement">
                    <div class="grid_4 alpha">
                        <p class="sub-label">Upload photo size 250px * 80px. <a class="upload">Click here.</a></p>
                        <div class="preview-slide" style="width: 250px; height: 80px;"></div>
                        <input type="hidden" name="file_id" id="file_id" />
                    </div>
                    <div class="grid_8 omega">
                        <p class="sub-label">Advertisement URL except http:// or https://</p>
                        <p class="sub-label small" style="color: #a0a0a0;">(Example: www.gdidevelop.com)</p>
                        <div class="metro-box">
                            <?php echo form_input('URL', NULL, 'style="width: 610px;"'); ?>
                        </div>
                        <a class="button submit" style="margin-top: 35px;">Save</a>
                    </div>
                </div>

                <?php echo form_close(); ?>
            <?php else: ?>
                <p class="sub-label">Advertisement are limit. You can remove it and upload new.</p>
            <?php endif; ?>

            <p class="sub-label big" style="margin-top: 35px;">Advertisement List</p>
        </div>

        <?php if (!empty($advertisement_list)): ?>
            <div id="advertisement-list">
                <?php foreach ($advertisement_list as $index => $advertisement): ?>
                <div class="grid_4 <?php echo (($index) ? ($index == 2 ? 'align-right' : 'align-center') : 'align-left'); ?>">
                        <?php if (!empty($advertisement['image'])): ?>
                            <img src="/<?php echo $advertisement['image']['path'], $advertisement['image']['name']; ?>" style="width: 250px; height: 80px;" />
                        <?php else: ?>
                            <img src="<?php echo image_asset_url('blank.png', 'Dashboard'); ?>" style="width: 250px; height: 80px;" />
                        <?php endif; ?>

                        <div class="advertisement-panel" style="cursor: pointer; <?php echo (($index) ? ($index == 2 ?  'margin-left: 50px;' : 'margin-left: 25px;') : NULL); ?>">
                            <div class="advertisement-delete { index: <?php echo $index; ?> }">
                                <div class="icon-trash"></div>
                                Delete
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div><!-- #advertisement-list -->
        <?php endif; ?>
    </div>
</div><!-- #dsb_page_advertisement -->

<!-- Hidden area -->
<div class="hidden">

    <!-- Confirm  dialog when delete this file -------------------------------->
    <div id="warning" class="confirm-dialog">
        <p class="sub-label">
            Are you sure you want to permanently delete this advertisement?
        </p><!-- .sub-label -->

        <div class="confirm-panel clearfix">
            <div id="confirm-okay" class="confirm-button danger">Confirm</div>
            <div id="confirm-cancel" class="confirm-button" style="margin-left: 7px;">Cancel</div>
        </div><!-- confirm-button -->
    </div>
    <!-- #confirm -->

</div>
<!-- / -->

<script type="text/javascript">
    
    $(function()
    {
        var global = {};
        
        $('.upload').click(function()
        {
            explorer({ writeonly: true, ratio: 3.12, crop: true },
            {
                onCropped: function(event, response)
                {
                    var data = response.message[0];
                    var image = $('<img>').attr(
                    {
                        src: '/' + data.path + data.name,
                        style: 'width: 850px; height: 80px;'
                    });
                    
                    $('.add-advertisement .preview-slide').html(image);
                    $('#file_id').val(data.id);
                    
                    explorer({ close: true });
                }
            }) 
        });
        
        $('#dsb_page_advertisement .submit').click(function()
        {
            $('#dsb_page_advertisement form').submit();
        });
        
        /* Confirm ---------------------------------------------------------- */
        
        $('.advertisement-delete').click(function()
        {
            block_ui(null, $('#warning')); 
            
            var index = $(this).metadata().index;
            
            global.index = index;
        });
        
        $('#confirm-okay').click(function()
        { 
            if(global.index != null)
            {
                $.post('/dashboard/page/delete/advertisement', { index: global.index }, function(response)
                {
                    window.location.reload();
                });
            }
        });
        
        $('#confirm-cancel').click(function()
        {
            unblock_ui();
        });
    });

</script>

<?php
/* End of file advertisment.php */
/* Location: ./application/modules/Page/views/Dsb_Page/advertisment.php */