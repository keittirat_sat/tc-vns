<?php
/**
 * Promotion
 * 
 * @package Page
 * @subpackage Dsb_Page
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
?>

<div id="dsb_page_promotion">

    <div class="container_12 content clearfix">
        <div class="content-header grid_12">
            <p class="sub-label super-big">
                Promotion
            </p>
        </div><!-- content-header -->

        <div class="content-body grid_12">
            <p class="sub-label big">Cover</p>
            <div class="cover-promotion">
                <?php echo form_open('dashboard/page/save/promotion'); ?>
                <p class="sub-label">Upload promotion photo. <a class="upload { field_name: 'promotion_cover' }">Click here.</a></p>
                <div class="preview-slide">
                    <?php if(!empty($promotion_cover)): ?>
                        <img src="/<?php echo $promotion_cover['path'], $promotion_cover['name']; ?>" style="width: 900px; height: 400px;" />
                    <?php endif; ?>
                </div>
                <a class="button submit" style="margin-top: 10px;">Save</a>
                <input type="hidden" id="promotion_cover" name="promotion_cover" value="<?php echo (empty($promotion_cover) ? NULL : $promotion_cover['id']); ?>" />
                <?php echo form_close(); ?>
            </div><!-- .new-promotion -->

            <p class="sub-label big" style="margin-top: 35px;">Index</p>
            <p>Browse images. <a class="upload browse">Click here.</a></p>
            <br />
            <div class="index-promotion">

                <?php echo form_open('dashboard/page/save/promotion'); ?>

                <div class="locale-tab clearfix">
                    <ul>
                        <?php if(!empty($enable_locale)): ?>
                            <?php foreach($enable_locale as $index => $value): ?>
                                <?php $selected = (!strcmp($value['sign'], 'en_US') ? ' selected ' : NULL); ?>
                                <?php $latest = ($index + 1 == count($enable_locale) ? ' latest ' : NULL); ?>
                                <li class="<?php echo $selected, $latest; ?> { sign: '<?php echo $value['sign']; ?>' }">
                                    <div class="flag flag-<?php echo $value['sign']; ?>"></div>
                                    <?php echo $value['name']; ?>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div><!-- .locale-tab -->

                <div class="clearfix">
                    <?php if(!empty($enable_locale)): ?>
                        <?php foreach($enable_locale as $index => $value): ?>
                            <div id="locale-<?php echo $value['sign']; ?>" class="locale-content clearfix hide">
                                <?php echo form_textarea("promotion_index[{$value['sign']}]", (empty($promotion_index[$value['sign']]) ? NULL : $promotion_index[$value['sign']]), 'class="tinymce" style="width: 930px;"'); ?>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>

                <a class="button submit" style="margin-top: 10px;">Save</a>

                <?php echo form_close(); ?>
            </div><!-- .index-promotion -->
        </div><!-- .content-body -->
    </div>

</div><!-- #dsb_page_promotion -->

<script type="text/javascript" src="<?php echo other_asset_url('jquery.tinymce.js', 'tiny_mce'); ?>"></script>
<script type="text/javascript" src="<?php echo other_asset_url('tiny_mce.js', 'tiny_mce'); ?>"></script>
<?php echo js_asset('jquery.hotkeys.js'); ?>
<script type="text/javascript">

    $(function()
    {    
        /* Multiple locale -------------------------------------------------- */
        
        $('#dsb_page_promotion .locale-tab li').click(function() 
        {   
            var sign = $(this).metadata().sign;
            set_classname($(this), 'selected');
            set_classname($('#locale-' + sign), 'selected');
        });
        
        $('#dsb_page_promotion .locale-tab li.selected').trigger('click');
        
        $('a.button.submit').click(function()
        {
            $(this).parent().submit();
        });
        
        $('#dsb_page_promotion form').submit(function()
        {
            block_ui();
            
            var data = $(this).serialize();
            
            $.post($(this).attr('action'), data, function() { unblock_ui(); });
            
            return false;
        });
    
        $('#dsb_page_promotion .upload').not('.browse').click(function()
        {
            var self = this;
            var field_name = $(this).metadata().field_name;
            var field_id = $(self).siblings('#promotion_cover').val();
            
            explorer({ writeonly: true, ratio: 2.25, crop: true },
            {
                onCropped: function(event, response)
                {
                    if(field_id != '') $.post('/dashboard/file/writer/delete/' + field_id);   
            
                    var data = response.message[0];
                    var image = $('<img>').attr(
                    {
                        src: '/' + data.path + data.name,
                        style: 'width: 900px; height: 400px;'
                    });
                    
                    $(self).parent().siblings('#' + field_name).val(data.id);
                    $(self).parent().siblings('.preview-slide').html(image);
                    
                    explorer({ close: true });
                }
            });
        });
        
        $('textarea.tinymce').tinymce({
            // General options
            mode : "textareas",
            theme : "advanced",
            
            plugins : "layer,advimage,advlink,inlinepopups,preview,media",

            width: 940,
            height: 500,
            
            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,formatselect,fontsizeselect,|,image,media,|,anchor",
            theme_advanced_buttons2 : "",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true
        });
        
        $(document).bind('keydown', 'ctrl+s', function(e) 
        {
            e.preventDefault();
            $('#dsb_page_promotion form').trigger('submit');
        });
        
        $('.upload.browse').click(function()
        {
            explorer({}, { onUploaded: function()
                {
                    explorer({ exit: true });
                }
            });
        });
    });
</script>
<?php
/* End of file promotion.php */
/* Location: ./application/modules/Page/views/Dsb_Page/promotion.php */