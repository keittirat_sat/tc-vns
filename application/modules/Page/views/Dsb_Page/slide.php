<?php
/**
 * Slide
 * 
 * @package Dsb_Page
 * @subpackage Views
 * @since 2.0 
 * @author Nomkhonwaan ComputerScience
 */
?>

<div id="dsb_page_slide">

    <div class="container_12 content clearfix">
        <div class="content-header grid_12">
            <p class="sub-label super-big">
                Slide
            </p>
        </div><!-- content-header -->

        <div class="content-body grid_12">
            <p class="sub-label big">New Slide</p>
            <div class="new-slide">
                <p class="sub-label">Upload your photo and crop to create new slide. <a id="create-slide">Click here</a>.</p>

                <div class="hide">
                    <?php echo form_open('dashboard/page/save/slide'); ?>
                    <div class="preview-slide"></div><!-- #preview-slide -->
                    <input id="file_id" type="hidden" name="file_id" />
                    <div class="clearfix">
                        <p class="sub-label" style="margin-top: 10px;">Description</p>
                        <div class="metro-box">
                            <?php echo form_input('content', NULL, 'style="width: 930px;"'); ?>
                        </div>
                    </div>
                    <a class="button submit" style="margin-top: 10px;">Save</a>
                    <?php echo form_close(); ?>
                </div><!-- .hide -->
            </div><!-- .new-slide -->

            <p class="sub-label big" style="margin-top: 35px;">Slide List</p>
            <div class="slide-list">
                <?php echo form_open('dashboard/page/save/slide'); ?>
                <?php if (!empty($slide)): ?>
                    <p class="sub-label">Drag and drop slide image to reorder.</p>
                    <div id="change-slide" class="confirm-dialog round-corner very hide">
                        <p class="sub-label">You can save a new change or cancel.</p>
                        <div style="margin-top: 15px;" class="align-center">
                            <div id="slide-change-confirm" class="confirm-button danger">Save Change</div>
                            <div id="slide-change-cancel" class="confirm-button" style="margin-left: 10px;">Cancel</div>
                        </div>
                    </div>
                    <ul>
                        <?php foreach ($slide as $index => $value): ?>
                            <li>
                                <img src="/<?php echo $value['file']['path'], $value['file']['name']; ?>" width="918" height="313" />
                                <div class="panel">
                                    <div class="delete { file_id: <?php echo (empty($value['file']['id']) ? 0 : $value['file']['id']); ?> }">
                                        <div class="icon-trash"></div>
                                        Delete this slide.
                                    </div>
                                </div>
                                <?php if (!empty($value['content'])): ?>
                                    <div class="slide-content">
                                        <?php echo $value['content']; ?>
                                    </div>
                                <?php endif; ?>
                                <?php echo form_hidden("slide[{$index}][file_id]", $value['file']['id']); ?>
                                <?php echo form_hidden("slide[{$index}][content]", $value['content']); ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
                <?php echo form_hidden('slide_sorting', TRUE); ?>
                <?php echo form_close(); ?>
            </div><!-- .slide-list -->
        </div><!-- .content-body -->
    </div>

</div><!-- #dsb_page_slide -->

<!-- Hidden area -->
<div class="hidden">

    <!-- Confirm  dialog when delete this file -------------------------------->
    <div id="warning" class="confirm-dialog">
        <p class="sub-label">
            Are you sure you want to permanently delete this slide?
        </p><!-- .sub-label -->

        <div class="confirm-panel clearfix">
            <div id="confirm-okay" class="confirm-button danger">Confirm</div>
            <div id="confirm-cancel" class="confirm-button" style="margin-left: 7px;">Cancel</div>
        </div><!-- confirm-button -->
    </div>
    <!-- #confirm -->

</div>
<!-- / -->

<?php echo js_asset('jquery-ui-1.8.18.min.js'); ?>
<script type="text/javascript">
    
    var global = {};
    
    $(function()
    {
        $('#create-slide').click(function()
        {
            $('.new-slide .hide').show();
            
            explorer({ writeonly: true, ratio: 2.9, crop: true }, 
            {
                onCropped: function(event, response)
                {
                    if(global.id != '') $.post('/dashboard/file/writer/delete/' + global.id);
                    
                    var data = response.message[0];
                    var image = $('<img>').attr('src',  '/' + data.path + data.name);
                    
                    $('#file_id').val(data.id);
                    global.id = data.id;
                    
                    $('.preview-slide').html(image);
                    
                    explorer({ close: true });
                }
            });
        });
        
        $('.new-slide a.button.submit').click(function()
        {            
            if($('#file_id').val() != '')
            {
                $('.new-slide form').submit();
            }
        });
                
        /* Confirm dialog control ------------------------------------------- */
    
        $('#confirm-okay').live('click', function()
        {
            if(global.file_id != null)
            {
                $.post('/dashboard/page/delete/slide/' + global.file_id, function()
                {
                    window.location.reload();
                });
            }
        });

        $('#confirm-cancel').live('click', function()
        {
            unblock_ui();
        });
        
        $('.slide-list .delete').click(function()
        {
            var file_id = $(this).metadata().file_id;
            
            global.file_id = file_id;
            
            block_ui(null, $('#warning'));
        });
    
        /* Drag and drop reorder ------------------------------------------------ */
    
        $(".slide-list ul").sortable(
        {
            placeholder: "slide-placeholder",
            change: function(event, ui) 
            { 
                $('#change-slide').show();
            }
        });
        
        $('#slide-change-confirm').click(function()
        {
            $('.slide-list form').submit();
        });
        
        $('#slide-change-cancel').click(function()
        {
            window.location.reload(); 
        });
        //    
        //    $("#slide_sorting").disableSelection();
        //    
        //    $('.slide-list li').draggable();
    });
</script>

<?php
/* End of file slide.php */
/* Location: ./application/modules/Page/views/Dsb_Page/slide.php */