<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dsb_Video extends MY_Controller {

    public function index($page = 1) {

        if (!Modules::run('User/Dsb_Auth/get_login_status', TRUE)) {
            redirect('dashboard/login');
        }

        // Get video
        $limit = 10;
        $offset = ($page - 1) * $limit;
        $video_data = $this->db->limit($limit)->offset($offset)->order_by('id', 'desc')->get('video')->result_array();

        $this->template->set('video_data', $video_data);

        $count_video = $this->db->count_all('video');
        $pagination = get_pagination($page, $count_video, $limit, 1, site_url('dashboard/video/index'));

        $this->template->set('pagination', $pagination);

        $this->template->set('title', 'All Video');
        $this->template->load(NULL, 'dashboard');
    }

    public function add() {

        if (!Modules::run('User/Dsb_Auth/get_login_status', TRUE)) {
            redirect('dashboard/login');
        }

        if (!strcmp($this->input->server('REQUEST_METHOD'), 'POST')) {
            $submit_data = $this->input->POST(NULL, TRUE);
            $this->db->insert('video', array(
                'title' => $submit_data['video_title'],
                'desc' => $submit_data['video_desc'],
                'URL' => $submit_data['video_url'],
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
                    )
            );

            redirect('dashboard/video');
        }

        $this->template->set('title', 'Add');
        $this->template->load(NULL, 'dashboard');
    }

    public function edit($id = NULL) {

        if (!Modules::run('User/Dsb_Auth/get_login_status', TRUE)) {
            redirect('dashboard/login');
        }

        if (!strcmp($this->input->server('REQUEST_METHOD'), 'POST')) {
            $submit_data = $this->input->post(NULL, TRUE);

            $this->db->where('id', $submit_data['id'])->update('video', array(
                'title' => $submit_data['video_title'],
                'desc' => $submit_data['video_desc'],
                'URL' => $submit_data['video_url'],
                'modified' => date('Y-m-d H:i:s')
                    )
            );

            redirect('dashboard/video');
        }

        $video = $this->db->where('id', $id)->get('video')->result_array();
        $video = $video[0];

        $this->template->set('video', $video);

        $this->template->set('title', 'Edit');
        $this->template->load(NULL, 'dashboard');
    }

    public function delete($id = NULL) {

        if (!Modules::run('User/Dsb_Auth/get_login_status', TRUE)) {
            redirect('dashboard/login');
        }

        if (!strcmp($this->input->server('REQUEST_METHOD'), 'POST')) {
            $id = $this->input->post('id');
        }

        if (!is_array($id))
            $id = array($id);

        if (empty($id))
            return FALSE;

        foreach ($id as $val) {
            $this->db->where('id', $val)->delete('video');
        }
    }

}
