<div id="dsb_video_add">

    <div class="container_12 content clearfix">
        <div class="content-header grid_12">
            <p class="sub-label super-big">
                Add
            </p>
        </div><!-- content-header -->

        <div class="grid_12 content-body">
            <?php echo form_open('dashboard/video/edit', NULL, array('id' => $video['id'])); ?>

                <div class="grid_4 alpha">
                    <p class="sub-label big">Preview</p>
                    <div class="big-thumb">
                         <iframe id="youtube-preview" width="292" src="http://www.youtube.com/embed/<?php echo preg_replace('/http:\/\/youtu.be\//', '', $video['URL']); ?>" frameborder="0" allowfullscreen></iframe>
                    </div>

                    <p class="sub-label big" style="margin-top: 35px;">Save</p>
                    <div class="save">
                        <a id="video-submit" class="button">Save</a>
                    </div>
                </div>

                <div class="grid_8 omega">
                    <div class="clearfix">
                        <p class="sub-label big">YouTube Video URL</p>
                        <div class="clearfix section" style="border: 1px solid #ddd;">
                            <div class="grid_8 alpha omega">
                                <label class="div-label" for="youtube-link">URL:</label>
                                <div class="metro-box">
                                    <?php echo form_input('video_url', $video['URL'], 'class="require" id="youtube-link"'); ?>
                                </div>
                            </div>

                            <div class="grid_8 alpha omege" style="margin-top: 15px;">
                                <a id="load-preview" class="button">Load video preview</a>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix" style="margin-top: 35px;">
                        <p class="sub-label big">Video Description</p>
                        <div class="clearfix section" style="border: 1px solid #ddd;">
                            <div class="grid_8 alpha omega">
                                <label class="div-label" for="video-title">Title (req):</label>
                                <div class="metro-box">
                                    <?php echo form_input('video_title', $video['title'], 'class="require"'); ?>
                                </div>
                            </div>

                            <div class="grid_8 alpha omega" style="margin-top: 15px;">
                                <label class="div-label">Detail:</label>
                                <?php echo form_textarea('video_desc', $video['desc']); ?>
                            </div>
                        </div>
                    </div>
                </div>

            <?php echo form_close(); ?>
        </div><!-- .content-body -->

    </div><!-- .content -->

</div><!-- #dsb_video_index -->

<script type="text/javascript">
    jQuery(function() {
        $('#youtube-link').change(function() {
            var videoLink = $(this).val().toString();
            videoLink = videoLink.replace('http:\/\/youtu.be\/', '');
            $('#youtube-preview').attr('src', 'http://www.youtube.com/embed/' + videoLink);
        });

        $('#video-submit').click(function() {
            $('form').submit();
        });

        $('#load-preview').click(function() {
            var videoLink = $('#youtube-link').val().toString();
            videoLink = videoLink.replace('http:\/\/youtu.be\/', '');
            $('#youtube-preview').attr('src', 'http://www.youtube.com/embed/' + videoLink);
        });
    });
</script>