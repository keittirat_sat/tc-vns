<div id="dsb_video_index">

    <div class="container_12 content clearfix">
        <div class="content-header grid_12">
            <p class="sub-label super-big">
                All Video
            </p>
        </div><!-- content-header -->

        <div class="grid_12 content-body">
            <?php if(!empty($video_data)): ?>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Preview</th>
                            <th>Title</th>
                            <th>Created</th>
                            <th>Modified</th>
                            <th>Edit</th>
                        </tr>
                    </thead>    
                    <tbody>
                        <?php foreach($video_data as $data): ?>
                            <tr>
                                <td>
                                    <div class="goog-checkbox list { video_id: <?php echo $data['id']; ?> }"></div>
                                </td>
                                <td>
                                    <iframe width="292" src="http://www.youtube.com/embed/<?php echo preg_replace('/http:\/\/youtu.be\//', '', $data['URL']); ?>" frameborder="0" allowfullscreen></iframe>
                                </td>
                                <td>
                                    <a href="<?php echo site_url('pages/video/view/' . $data['id']); ?>">
                                        <?php if(empty($data['title'])): ?>
                                            Untitled Video
                                        <?php else: ?>
                                            <?php echo $data['title']; ?>
                                        <?php endif; ?>
                                    </a>
                                </td>
                                <td><?php echo date('F d Y H:i:s', strtotime($data['created'])); ?></td>
                                <td><?php echo date('F d Y H:i:s', strtotime($data['modified'])); ?></td>
                                <td>
                                    <a href="<?php echo site_url("dashboard/video/edit/{$data['id']}"); ?>" target="_blank">
                                        Click
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div><!-- .content-body -->
        
        <div class="content-footer grid_12">
            <a id="post-delete" class="button">Delete Select</a>

            <div class="page right">
                <?php echo $pagination; ?>
            </div>
        </div><!-- .content-footer -->
    </div><!-- .content -->

</div><!-- #dsb_video_index -->

<!-- Hidden area -->
<div class="hidden">

    <!-- Confirm  dialog when delete this file -------------------------------->
    <div id="confirm-delete" class="confirm-dialog">
        <p class="sub-label">
            Are you sure you want to permanently delete this video?
        </p><!-- .sub-label -->

        <div class="confirm-panel clearfix">
            <div id="confirm-okay" class="confirm-button danger">Confirm</div>
            <div id="confirm-cancel" class="confirm-button" style="margin-left: 7px;">Cancel</div>
        </div><!-- confirm-button -->
    </div>
    <!-- #confirm -->

</div>
<!-- / -->

<script type="text/javascript">
$(function()
{  
    /* Global section ------------------------------------------------------- */
        
    var global = {};
    
    /* Checkbox ------------------------------------------------------------- */
    
    $('.goog-checkbox.all').click(function()
    {
        if($(this).is('.goog-checkbox-checked'))
        {
            $('.goog-checkbox.list').removeClass('goog-checkbox-checked');
            $(this).removeClass('goog-checkbox-checked');
        }
        else
        {
            $('.goog-checkbox.list').addClass('goog-checkbox-checked');
            $(this).addClass('goog-checkbox-checked');
        }
    });
        
    $('.goog-checkbox').click(function() 
    {
        $(this).toggleClass('goog-checkbox-checked');
    });
    
    /* Confirm dialog control ----------------------------------------------- */
    
    $('#confirm-okay').live('click', function()
    {       
        $.post('/dashboard/video/delete', { id: global.video_id }, function(response) { window.location.reload(); });
    });

    $('#confirm-cancel').live('click', function()
    {
        unblock_ui();
    });
    
    /* Action --------------------------------------------------------------- */
    
    $('#post-delete').click(function()
    {
        var video_id = [];
        
        $('.goog-checkbox.list.goog-checkbox-checked').each(function(index, element)
        {
            video_id.push($(element).metadata().video_id);
        });
        
        if(video_id.length > 0)
        {
            global.video_id = video_id;
            
            block_ui(null, $('#confirm-delete'));
        }
    });
});
</script>