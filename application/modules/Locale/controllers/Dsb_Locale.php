<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dsb_Locale
 * 
 * @package Locale
 * @subpackage Controllers
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */
class Dsb_Locale extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_enable_locale()
    {
        $enable_locale = $this->dashboard->get_option('locale');

        return json_decode($enable_locale, TRUE);
    }

    public function is_enable_locale($sign)
    {
        $enable_locale = $this->get_enable_locale();
        $is_enable_locale = FALSE;

        foreach ($enable_locale as $value)
        {
            if (!strcmp($sign, $value['sign']))
            {
                $is_enable_locale = TRUE;
                break;
            }
        }
        
        return $is_enable_locale;
    }

}

/* End of file Dsb_Locale.php */
/* Location: ./application/modules/Locale/controllers/Dsb_Locale.php */