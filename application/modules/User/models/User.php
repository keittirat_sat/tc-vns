<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * User
 * 
 * @package User
 * @subpackage Models
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */
class User extends MY_Model
{

    var $table = 'user';

    public function get_user_by_username($username)
    {
        $this->db->where('username', $username);

        $result = $this->db->get($this->table)->result_array();

        return (!empty($result) ? $result[0] : NULL);
    }

    public function get_metadata($uid, $name = NULL)
    {
        $this->db->where('uid', $uid);

        if (isset($name))
        {
            $this->db->where('name', $name);
        }

        $result = $this->db->get('user_metadata')->result_array();

        if (isset($name))
        {
            return (!empty($result) ? $result[0] : NULL);
        }

        /* Re-form metadata */
        foreach ($result as $index => $value)
        {
            $result[$value['name']] = $value;
            unset($result[$index]);
        }

        return $result;
    }

    public function set_metadata($uid, $name, $value)
    {
        $result = $this->get_metadata($uid, $name);

        $metadata = array(
            'uid' => $uid,
            'name' => $name,
            'value' => $value
        );

        if (!empty($result))
        {
            /* Let's update */

            $metadata['modified'] = date('Y-m-d H:i:s');

            $this->db->where('id', $result['id']);
            $this->db->update('user_metadata', $metadata);

            $metadata_id = $result['id'];
        }
        else
        {
            /* Let's insert */

            $metadata['created'] = date('Y-m-d H:i:s');
            $metadata['modified'] = date('Y-m-d H:i:s');

            $metadata_id = $this->db->insert('user_metadata', $metadata);
        }

        return $metadata_id;
    }

}

/* End of file User.php */
/* Location: ./application/modules/User/models/User.php */