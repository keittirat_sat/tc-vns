<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dsb_Auth
 * 
 * @package User
 * @subpackage Controllers
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
class Dsb_Auth extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        /* Auto load model */
        $this->load->model('User');
    }

    /**
     * Get login status
     * 
     * @param boolean $return_boolean
     * @return string | boolean 
     */
    public function get_login_status($return_boolean = FALSE)
    {
        $ss_token = $this->session->userdata('token');
        $ss_uid = $this->session->userdata('uid');

        $cc_token = get_cookie('token');
        $cc_uid = get_cookie('uid');
        $cc_forgetmenot = get_cookie('forgetmenot');

        /* User remember login status */
        if (!empty($cc_forgetmenot) AND !empty($cc_token) AND !empty($cc_uid))
        {
            $ss_token = $cc_token;
            $ss_uid = $cc_uid;
        }
        else
        {
            $response = array(
                'status' => 'failed',
                'message' => 'Authentication failure, please sign-in again.'
            );
        }

        /* Get user data */
        if (!empty($ss_token) AND !empty($ss_uid))
        {
            $db_user = $this->User->find_id($ss_uid);

            if (!empty($db_user))
            {
                if (!strcmp($ss_token, $db_user['password']))
                {
                    // accept
                    $this->session->set_userdata('token', $ss_token);
                    $this->session->set_userdata('uid', $ss_uid);

                    $response = array(
                        'status' => 'success',
                        'message' => 'Authentication complete.'
                    );
                }
                else
                {
                    // remove session
                    $this->session->unset_userdata('token');
                    $this->session->unset_userdata('uid');

                    // remove cookie
                    set_cookie('token', NULL, time());
                    set_cookie('uid', NULL, time());

                    $response = array(
                        'status' => 'failed',
                        'message' => 'Invalid username or password.'
                    );
                }
            }
            else
            {
                $response = array(
                    'status' => 'failed',
                    'message' => 'User not found.'
                );
            }
        }
        else
        {
            $response = array(
                'status' => 'failed',
                'message' => 'Authentication failure, please sign-in again.'
            );
        }

        if ($return_boolean)
        {
            return (!strcmp($response['status'], 'success') ? TRUE : FALSE);
        }
        else
        {
            $this->template->load('response', 'json', $response);
        }
    }

    public function login($return_boolean = FALSE)
    {
        $post_data = $this->input->post(NULL, TRUE);
        $db_user = $this->User->get_user_by_username($post_data['username']);

        if (!empty($db_user))
        {
            $db_salt = $db_user['salt'];
            $db_password = $db_user['password'];

            $token = md5($post_data['password'] . $db_salt);

            /* Compare password token */
            if (!strcmp($token, $db_password))
            {
                // accept

                /* Set user data to session */
                $this->session->set_userdata('uid', $db_user['id']);
                $this->session->set_userdata('token', $token);

                /* Set user data to cookie */
                if (!empty($post_data['forgetmenot']))
                {
                    // if user set remember
                    // time is 1 year
                    $time = time() + (3600 * 24 * 365);
                    set_cookie('forgetmenot', TRUE, $time);
                }
                else
                {
                    // if user don't set remember
                    // time is 1 hour
                    $time = time() + 3600;
                }

                set_cookie('uid', $db_user['id'], $time);
                set_cookie('token', $token, $time);

                $response = array(
                    'status' => 'success',
                    'message' => 'Login successfully.'
                );
            }
            else
            {
                // invalid password
                $response = array(
                    'status' => 'failed',
                    'message' => 'Invalid username or password.'
                );
            }
        }
        else
        {
            // user not found
            $response = array(
                'status' => 'failed',
                'message' => 'User not found.'
            );
        }

        if ($return_boolean)
        {
            return (!strcmp($response['status'], 'success') ? TRUE : FALSE);
        }
        else
        {
            $this->template->load('response', 'json', $response);
        }
    }

}

/* End of file Dsb_Auth.php */
/* Location: ./application/modules/User/controllers/Dsb_Auth.php */