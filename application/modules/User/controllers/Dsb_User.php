<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dsb_User
 * 
 * @package User
 * @subpackage Controllers
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */
class Dsb_User extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        /* Auto load model */
        $this->load->model('User');
    }

    public function get_user_data($uid)
    {
        $db_user = $this->User->get(NULL, $uid);

        if (!empty($db_user))
        {
            $db_user['metadata'] = $this->User->get_metadata($uid);
        }
        
        return $db_user;
    }

    public function save($data = NULL)
    {
        /* Allow set user by post */
        if (empty($data))
        {
            $data = $this->input->post(NULL, TRUE);
        }

        if (!empty($data))
        {
            /* Get existing user from email account */
            if (isset($data['email']))
            {
                $db_user = $this->User->find(NULL, array('email' => $data['email']), 0, 1);
            }

            $save = array(
                'email' => $data['email'],
                'group_id' => (isset($data['group_id']) ? $data['group_id'] : GROUP_SUBSCRIBER),
                'status' => (isset($data['status']) ? $data['status'] : USER_ACTIVE)
            );

            if (!empty($db_user))
            {
                $save['id'] = $db_user[0]['id'];
            }

            $uid = $this->User->save($save);

            foreach ($data as $key => $value)
            {
                $this->User->set_metadata($uid, $key, $value);
            }

            return $uid;
        }
    }

}

/* End of file Dsb_User.php */
/* Locaiotion: ./application/modules/User/controllers/Dsb_User.php */