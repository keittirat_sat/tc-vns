<?php
/**
 * View
 * 
 * @package Cart
 * @subpackage Dsb_Order
 * @since 2.0 
 * @author Nomkhonwaan ComputerScience
 */
?>

<div id="dsb_order_view">


    <div class="container_12 content clearfix">
        <div class="content-header grid_12">
            <p class="sub-label super-big">
                <?php echo "{$invoice['user']['metadata']['name']['value']} {$invoice['user']['metadata']['lastname']['value']}"; ?>
                -
                <?php $timestamp = strtotime($invoice['created']); ?>
                <?php echo date('F d Y H:i', $timestamp); ?>
            </p>
        </div><!-- content-header -->

        <div class="content-body grid_12">
            <div class="grid_4 alpha">
                <p class="sub-label big">Detail</p>
                <div class="invoice-detail">
                    <table>
                        <tbody>
                            <tr>
                                <th>Customer:</th>
                                <td><?php echo "{$invoice['user']['metadata']['name']['value']} {$invoice['user']['metadata']['lastname']['value']}"; ?></td>
                            </tr>
                            <tr>
                                <th>Address:</th>
                                <td>
                                    <?php if (!empty($invoice['user']['metadata']['address'])): ?>
                                        <?php echo $invoice['user']['metadata']['address']['value']; ?>
                                    <?php endif; ?>
                                    
                                    <?php if (!empty($invoice['user']['metadata']['state'])): ?>
                                        <?php echo $invoice['user']['metadata']['state']['value']; ?>
                                    <?php endif; ?>

                                    <?php if (!empty($invoice['user']['metadata']['country'])): ?>
                                        <?php echo $invoice['user']['metadata']['country']['value']; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Telephone:</th>
                                <td>
                                    <?php if (!empty($invoice['user']['metadata']['telephone'])): ?>
                                        <?php echo $invoice['user']['metadata']['telephone']['value']; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Email:</th>
                                <td>
                                    <?php if (!empty($invoice['user']['metadata']['email'])): ?>
                                        <?php echo $invoice['user']['metadata']['email']['value']; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>        
                                <th>Order Date:</th>
                                <td>
                                    <?php $timestamp = strtotime($invoice['created']); ?>
                                    <?php echo date('F d Y H:i', $timestamp); ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Total Price:</th>
                                <td><?php echo number_format($invoice['total']); ?> Baht</td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- .invoice-detail -->

                <p class="sub-label big" style="margin-top: 35px;">Shipping</p>
                <div id="shipping-type" class="clearfix">
                    <?php echo form_open('dashboard/order/change_shipping', NULL, array('invoice_id' => $invoice['id'])); ?>

                    <div class="left">
                        <div class="show">
                            <b><?php echo shipping_type($invoice['shipping']); ?></b>
                        </div>
                        <div class="hide">
                            <?php echo form_dropdown('shipping', $shipping_type, $invoice['shipping'], 'style="width: 197px;"'); ?>
                        </div>
                    </div>

                    <div class="right">
                        <div class="show">
                            <a id="change-shipping" style="text-decoration: none;">Change shipping</a>
                        </div>
                        <div class="hide">
                            <a class="submit button">Save</a>
                        </div>
                    </div>

                    <?php echo form_close(); ?>
                </div><!-- #shipping-type -->

                <p class="sub-label big" style="margin-top: 35px;">Status</p>
                <div id="invoice-status" class="clearfix">
                    <?php echo form_open('dashboard/order/update_status', NULL, array('invoice_id' => $invoice['id'])); ?>

                    <div class="left">
                        <div class="show">
                            <b><?php echo product_status($invoice['status']); ?></b>
                        </div>
                        <div class="hide">
                            <?php echo form_dropdown('status', $product_status, $invoice['status'], 'style="width: 197px;"'); ?>
                        </div>
                    </div>

                    <div class="right">
                        <div class="show">
                            <a id="change-status" style="text-decoration: none;">Change status</a>
                        </div>
                        <div class="hide">
                            <a class="submit button">Save</a>
                        </div>
                    </div>

                    <?php echo form_close(); ?>
                </div><!-- .invoice-status -->
            </div><!-- .grid_4 -->

            <div class="grid_8 omega">
                <p class="sub-label big">Item List</p>
                <div class="order-list">
                    <?php if (!empty($order_list)): ?>
                        <ul>
                            <?php foreach ($order_list as $order): ?>
                                <li class="clearfix">
                                    <div class="left">
                                        <?php if (!empty($order['post']['file'][0])): ?>
                                            <img src="/<?php echo $order['post']['file'][0]['path'], 'thumb-', $order['post']['file'][0]['name']; ?>" height="50" />
                                        <?php else: ?>
                                            <img src="<?php echo image_asset_url('blank.png', 'Dashboard'); ?>" alt="No image" height="50" />
                                        <?php endif; ?>
                                    </div>

                                    <div class="left" style="margin-left: 15px; padding-top: 8px;">
                                        <div><b><?php echo $order['post']['name']; ?></b></div>
                                        Quantity:
                                        <span class="message error"><?php echo $order['quantity']; ?></span>
                                        l
                                        Total Price:
                                        <span class="message error"><?php echo number_format($order['total']); ?></span>
                                        Baht
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

</div><!-- #dsb_order_view -->

<script type="text/javascript">
    $(function()
    {
        /* Shipping --------------------------------------------------------- */
        
        $('#change-shipping').click(function()
        {
            $('#shipping-type .show').hide();
            $('#shipping-type .hide').show();
        });
        
        $('#shipping-type .submit').click(function()
        {
            block_ui();
            
            $('#shipping-type form').submit();
        });
        
        $('#shipping-type form').submit(function()
        {
            var data = $(this).serialize();
            
            $.post($(this).attr('action'), data, function(response)
            {           
                unblock_ui();
                                
                $('#shipping-type .show').show();
                $('#shipping-type .hide').hide();
                    
                if(response.status == 'success')
                {  
                    $('#shipping-type b').text($('#shipping-type option:selected').text());
                }
                else
                {
                    if(confirm('An error encountered. Reload this page again?'))
                    {
                        window.location.reload();
                    }
                }
                
            }, 'json');
            
            return false;
        });
        
        /* Status ----------------------------------------------------------- */
        
        $('#change-status').click(function()
        {
            $('#invoice-status .show').hide();
            $('#invoice-status .hide').show();
        });
        
        $('#invoice-status .submit').click(function()
        {
            block_ui();
            
            $('#invoice-status form').submit();
        });
        
        $('#invoice-status form').submit(function()
        {
            var data = $(this).serialize();
            
            $.post($(this).attr('action'), data, function(response)
            {           
                unblock_ui();
                                
                $('#invoice-status .show').show();
                $('#invoice-status .hide').hide();
                    
                if(response.status == 'success')
                {  
                    $('#invoice-status b').text($('#invoice-status option:selected').text());
                }
                else
                {
                    if(confirm('An error encountered. Reload this page again?'))
                    {
                        window.location.reload();
                    }
                }
                
            }, 'json');
            
            return false;
        });
    });
</script>

<?php
/* End of file view.php */
/* Location: ./application/modules/Cart/views/Dsb_Order/view.php */
