<?php
/**
 * Index
 * 
 * @package Cart
 * @subpackage Dsb_Order
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
?>

<div id="dsb_order_index">

    <div class="container_12 content clearfix">
        <div class="content-header grid_12">
            <p class="sub-label super-big">
                Order
            </p>
        </div><!-- content-header -->

        <div class="grid_12 content-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Order Date</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Total (baht)</th>
                        <th>Shipment</th>
                        <th>Status</th>
                        <th>Detail</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($invoice_list)): ?>
                        <?php foreach ($invoice_list as $invoice): ?>
                            <tr class="<?php if($invoice['notify'] == UNNOTIFY): ?> new-order <?php endif; ?> <?php if($invoice['status'] == ORDER_SUCCESS): ?> order-success <?php endif; ?>">
                                <td>
                                    <?php $timestamp = strtotime($invoice['created']); ?>
                                    <?php if (!strcmp(date('Y-m-d', $timestamp), date('Y-m-d'))): ?>
                                        Today at <?php echo date('H:i:s', $timestamp); ?>
                                    <?php else: ?>
                                        <?php echo date('F d Y H:i', $timestamp); ?>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php echo $invoice['user']['metadata']['name']['value']; ?>
                                    <?php echo $invoice['user']['metadata']['lastname']['value']; ?>
                                </td>
                                <td><?php echo $invoice['user']['email']; ?></td>
                                <td><?php echo $invoice['total']; ?></td>
                                <td><?php echo shipping_type($invoice['shipping']); ?></td>
                                <td><?php echo product_status($invoice['status']); ?></td>
                                <td><a target="_blank" href="<?php echo site_url("dashboard/order/view/{$invoice['id']}"); ?>">Click</a></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div><!-- .content-body -->
        
        <div class="content-footer grid_12">
            <div class="page right">
                <?php echo $pagination; ?>
            </div>
        </div><!-- .content-footer -->
    </div><!-- .content -->

</div><!-- #dsb_order_index -->

<?php
/* End of file index.php */
/* Location: ./application/modules/Cart/views/Dsb_Order/index.php */