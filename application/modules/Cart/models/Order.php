<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Order
 * 
 * @package Cart
 * @subpackage Models
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
class Order extends MY_Model 
{
    var $table = 'order';
}

/* End of file Order.php */
/* Location: ./application/modules/Cart/models/Order.php */
