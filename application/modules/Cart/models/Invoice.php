<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Invoice
 * 
 * @package Cart
 * @subpackage Models
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */
class Invoice extends MY_Model
{

    var $table = 'invoice';

    public function notify($id)
    {
        $this->db->where('id', $id);
        $db_invoice = $this->db->get($this->table);

        if (!empty($db_invoice))
        {
            $this->db->where('id', $id);
            $this->db->update($this->table, array('notify' => NOTIFIED));
        }
    }

}

/* End of file Invoice.php */
/* Location: ./application/modeuls/cart/models/Invoice.php */
