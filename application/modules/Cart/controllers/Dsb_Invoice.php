<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dsb_Invoice
 * 
 * @package Cart
 * @subpackage Controllers
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
class Dsb_Invoice extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Invoice');
    }

    public function save($data)
    {
        if (empty($data['id']))
        {
            $data['name'] = time();
        }
        
        $data['notify'] = UNNOTIFY;

        return $this->Invoice->save($data);
    }

    public function get_notify()
    {
        /* This function will called at MY_Controller::__construct(); */
        $this->load->model('Invoice');

        return $this->Invoice->count(NULL, array('notify' => UNNOTIFY));
    }

}

/* End of file Dsb_Invoice.php */
/* Location: ./application/modules/Cart/controller/Dsb_Invoice.php */
