<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dsb_Order
 * 
 * @package Cart
 * @subpackage Controllers
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
class Dsb_Order extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Order');
        $this->load->model('Invoice');
    }

    public function index($page = 1)
    {
        if (!Modules::run('User/Dsb_Auth/get_login_status', TRUE))
        {
            redirect('dashboard/login');
        }

        $invoice_list = $this->Invoice->find(NULL, NULL, ($page - 1) * 25, 25, 'desc');
        $count_invoice = $this->Invoice->count();

        $pagination = get_pagination($page, $count_invoice, 25, 1, site_url('dashboard/order/index'));
        $this->template->set('pagination', $pagination);

        foreach ($invoice_list as &$invoice)
        {
            $user = Modules::run('User/Dsb_User/get_user_data', $invoice['uid']);
            $invoice['user'] = $user;
        }

        $this->template->set('invoice_list', $invoice_list);

        $this->template->set('title', 'Order');
        $this->template->load(NULL, 'dashboard');
    }

    public function view($invoice_id)
    {
        if (!Modules::run('User/Dsb_Auth/get_login_status', TRUE))
        {
            redirect('dashboard/login');
        }

        $invoice = $this->Invoice->get(NULL, $invoice_id);

        if (!empty($invoice))
        {
            $user = Modules::run('User/Dsb_User/get_user_data', $invoice['uid']);
            $invoice['user'] = $user;
            $this->template->set('invoice', $invoice);

            $order_list = $this->Order->find(NULL, array('invoice_id' => $invoice_id));

            if (!empty($order_list))
            {
                foreach ($order_list as &$order)
                {
                    $post = $this->dashboard->get_product($order['post_id']);

                    if (!empty($post))
                    {
                        $order['post'] = $post[0];
                    }
                }
            }

            $this->template->set('order_list', $order_list);

            $product_status = array(
                ORDER_ORDERING => product_status(ORDER_ORDERING),
                ORDER_CONFIRMED => product_status(ORDER_CONFIRMED),
                ORDER_PACKED => product_status(ORDER_PACKED),
                ORDER_SENDING => product_status(ORDER_SENDING),
                ORDER_SUCCESS => product_status(ORDER_SUCCESS),
                ORDER_FAILURE => product_status(ORDER_FAILURE)
            );

            $this->template->set('product_status', $product_status);

            $shipping_type = array(
                SHIP_BY_AIR => shipping_type(SHIP_BY_AIR),
                SHIP_BY_BUS => shipping_type(SHIP_BY_BUS),
                SHIP_BY_TRAIN => shipping_type(SHIP_BY_TRAIN),
                SHIP_BY_EMS => shipping_type(SHIP_BY_EMS)
            );

            $this->template->set('shipping_type', $shipping_type);

            /* Update notify status */
            $this->Invoice->notify($invoice_id);



            $timestamp = strtotime($invoice['created']);

            $this->template->set('title', date('F d Y H:i', $timestamp));
            $this->template->load(NULL, 'dashboard');
        }
        else
        {
            show_error('Order not found.', 404);
        }
    }

    public function save()
    {
        $data = $this->input->post(NULL, TRUE);
        $product = array();
        $user = array();

        /* Explode data */

        foreach ($data as $key => $value)
        {
            if (preg_match('/^product_/', $key))
            {
                $explode = explode('_', $key);
                $product[$explode['2']][$explode['1']] = $value;
            }
            elseif (preg_match('/^user_/', $key))
            {
                $explode = explode('_', $key);
                $user[$explode[1]] = $value;
            }
            elseif (preg_match('/^order_/', $key))
            {
                $shipping = $value;
            }
        }

        /* Check existing user */

        $uid = Modules::run('User/Dsb_User/save', $user);

        /* Calculate total price */

        $order = array();
        $total = 0;

        foreach ($product as $post)
        {
            $response = $this->buy($post['id'], $post['qty'], TRUE);

            if (!strcmp($response['status'], 'success'))
            {
                $response = $response['message'];

                /* Summary tatal price */
                $total += $response['total'];

                /* Data of order */
                array_push($order, array(
                    'uid' => $uid,
                    'post_id' => $response['product_id'],
                    'quantity' => $response['quantity'],
                    'total' => $response['total'])
                );

                /* Reduce item from stock */
                Modules::run('Post/Dsb_Post/reduce_stock', $response['product_id'], $response['quantity']);
            }
            else
            {
                $this->template->load('response', 'json', $response);
            }
        }

        /* Create invoicing  */

        $invoice = array('uid' => $uid, 'total' => $total, 'shipping' => $shipping, 'status' => ORDER_ORDERING);
        $invoice_id = Modules::run('Cart/Dsb_Invoice/save', $invoice);

        /* Create item order */

        foreach ($order as $o)
        {
            /* Set invoice ID */
            $o['invoice_id'] = $invoice_id;
            $return = $this->Order->save($o);
        }

        if (isset($data['return_url']))
        {
            redirect($data['return_url']);
        }

        redirect('pages/index');
    }

    public function buy($product_id, $quantity = NULL, $return = FALSE)
    {
        /* Quantity from session */

        if (empty($quantity))
        {
            $cart = $this->session->userdata('cart');

            /* Set default quantity is 1 */
            $quantity = (empty($cart[$product_id]) ? 1 : $cart[$product_id]['total'] + 1);
        }

        /* Get product */

        $post = $this->dashboard->get_product($product_id);

        if (!empty($post))
        {
            $post = $post[0];

            $price = $post['metadata']['price']['value'];
            $stock = $post['metadata']['stock']['value'];

            if ($stock != -1 AND $stock < $quantity)
            {
                $response = array(
                    'status' => 'failed',
                    'message' => 'This item is not enough.'
                );
            }
            else
            {
                $response = array(
                    'status' => 'success',
                    'message' => array(
                        'product_id' => $product_id,
                        'quantity' => $quantity,
                        'price' => $price,
                        'stock' => $stock,
                        'total' => $price * $quantity
                    )
                );
            }
        }
        else
        {
            $response = array(
                'status' => 'failed',
                'message' => 'Product not found.'
            );
        }

        if ($return) return $response;

        $this->template->load('response', 'json', $response);
    }

    public function update_status()
    {
        if (!Modules::run('User/Dsb_Auth/get_login_status', TRUE))
        {
            redirect('dashboard/login');
        }

        $invoice_id = $this->input->post('invoice_id', TRUE);
        $status = $this->input->post('status', TRUE);

        $this->Invoice->get(NULL, $invoice_id);

        if (!empty($invoice_id))
        {
            $this->Invoice->update($invoice_id, array('status' => $status));

            $response = array(
                'status' => 'success',
                'message' => 'Successfully update status.'
            );
        }
        else
        {
            $response = array(
                'status' => 'failed',
                'message' => 'Order not found.'
            );
        }

        $this->template->load('response', 'json', $response);
    }

    public function change_shipping()
    {
        if (!Modules::run('User/Dsb_Auth/get_login_status', TRUE))
        {
            redirect('dashboard/login');
        }

        $invoice_id = $this->input->post('invoice_id', TRUE);
        $shipping = $this->input->post('shipping', TRUE);

        $this->Invoice->get(NULL, $invoice_id);

        if (!empty($invoice_id))
        {
            $this->Invoice->update($invoice_id, array('shipping' => $shipping));

            $response = array(
                'status' => 'success',
                'message' => 'Successfully change shipping.'
            );
        }
        else
        {
            $response = array(
                'status' => 'failed',
                'message' => 'Order not found.'
            );
        }

        $this->template->load('response', 'json', $response);
    }

}

/* End of file Dsb_Order.php */
/* Location: ./application/modules/Cart/controllers/Dsb_Order.php */