<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Bootstrap Hook
 * 
 * @package Hooks
 * @subpackage Hooks
 * @since 1.0
 * @author Nomkhonwaan ComputerScience
 */
class Bootstrap
{

    /**
     * To define constant here
     * 
     * this function will initialize before system controller load
     * 
     * @since 2.0
     * @author Nomkhonwaan ComputerScience
     */
    public function constant()
    {
        /* Default */
        define('DRAFT', 1);
        define('PUBLISH', 2);

        define('CHILDREN', 99);

        define('UNNOTIFY', 1);
        define('NOTIFIED', 2);

        /* File type */
        define('FILE_DOCUMENT', 1);
        define('FILE_MULTIMEDIA', 2);
        define('FILE_IMAGE', 3);

        /* Post type */
        define('POST_PRODUCT', 1);
        define('POST_PROMOTION', 2);
        define('POST_TECHNOLOGY', 3);
        define('POST_RECIPE', 4);

        /* User group */
        define('GROUP_ADMINISTRATOR', 1);
        define('GROUP_MODERATOR', 2);
        define('GROUP_WRITER', 3);
        define('GROUP_SUBSCRIBER', 4);

        /* User status */
        define('USER_ACTIVE', 1);
        define('USER_BANNED', 2);

        /* Order status */
        define('ORDER_ORDERING', 1);
        define('ORDER_CONFIRMED', 2);
        define('ORDER_PACKED', 3);
        define('ORDER_SENDING', 4);
        define('ORDER_SUCCESS', 5);
        define('ORDER_FAILURE', 6);

        /* Shipping method */
        define('SHIP_BY_AIR', 1);
        define('SHIP_BY_BUS', 2);
        define('SHIP_BY_TRAIN', 3);
        define('SHIP_BY_EMS', 4);

        /* Privacy protected */
        define('GOOGLE_ACCOUNT', 'gdidevelop@gmail.com');
        define('GOOGLE_PASSWORD', '500510xxx');
        define('ANALYTIC_SITE_ID', '60647897');
    }
}

/* End of file bootstrap.php */
/* Location: ./application/hooks/bootstrap.php */