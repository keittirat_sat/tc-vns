<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Pages
 * 
 * @package Vanusnun
 * @subpackage Controllers 
 * @since 2.0
 * @author GDI Development
 */
class Pages extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $enable_locale = $this->dashboard->get_enable_locale();
        $this->template->set('locale', $this->get_locale());
        $this->template->set('all_locale', $enable_locale);
        $this->template->set('cart_display', $this->retrieve_cart());
    }

    private function retrieve_cart() {
        return $this->load->view('pages/get_cart', '', TRUE);
    }

    public function get_cart() {
        echo $this->retrieve_cart();
    }

    public function add_cart() {
        $product_id = $this->input->get_post('product_id');
        $data = $this->session->userdata('cart');
        if (empty($data[$product_id])) {
            $data[$product_id] = array(
                'product_id' => $product_id,
                'total' => $this->input->get_post('total')
            );
        } else {
            $data[$product_id]['total'] += $this->input->get_post('total');
        }
        $this->session->set_userdata('cart', $data);
        echo "1";
    }

    public function update_cart() {
        $product_id = $this->input->get_post('product_id');
        $data = $this->session->userdata('cart');
        $data[$product_id]['total'] = $this->input->get_post('total');
        $this->session->set_userdata('cart', $data);
        echo "1";
    }

    public function delete_cart() {
        $product_id = $this->input->get_post('product_id');
        $data = $this->session->userdata('cart');
        unset($data[$product_id]);
        $this->session->set_userdata('cart', $data);
        echo "1";
    }

    public function clear_cart() {
        $this->session->set_userdata('cart', array());
    }

    private function get_locale() {
        if ($this->input->cookie('default_locale', TRUE)) {
            return $this->input->cookie('default_locale');
        } else {
            return 'en_US';
        }
    }

    public function cover_page() {
        $promotion_cover = $this->dashboard->get_promotion_cover();
        $this->template->set('slide', $promotion_cover);
        $this->template->load(null,'landing_page');
    }

    public function add_subscribe() {
        $data = json_decode($this->dashboard->get_option('subscribe'));
        $data = (array) $data;
        $exist = true;
        if (!empty($data)) {
            foreach ($data as $email) {
                if ($email == $this->input->get_post('email')) {
                    $exist = false;
                }
            }
        } else {
            $data = array();
        }

        if ($exist) {
            if (array_push($data, $this->input->get_post('email'))) {
                $json_data = json_encode($data);
                $this->dashboard->set_option('subscribe', $json_data);
                echo json_encode(array("status" => "success"));
            } else {
                echo json_encode(array("status" => "Push Error"));
            }
        } else {
            echo json_encode(array("status" => "Email on exist"));
        }
        //print_r($data);
    }

    public function index() {

        $promotion_index = $this->dashboard->get_promotion_index();
        $this->template->set('promotion_index', $promotion_index);

        $advertisement_list = $this->dashboard->get_advertisement();
        $this->template->set('advertisement_list', $advertisement_list);

        $technology_list = $this->dashboard->get_technology();
        $this->template->set('technology_list', $technology_list);

        $this->template->set('slide_promotion', $this->load->view('pages/slide_promotion', '', true));
        $this->template->set('title', 'Homepage');
        $this->template->load();
    }

    public function promotion_index() {
        $promotion_index = $this->dashboard->get_promotion_index();
        $this->template->set('promotion_index', $promotion_index);
        $this->template->load('pages/promotion_index', 'ajax');
    }

    public function techno() {
        $id = $this->input->get('id');
        $technology_list = $this->dashboard->get_technology();
        foreach ($technology_list as $tech) {
            if ($tech['parent_id'] != 0) {
                $temp_id = $tech['parent_id'];
            } else {
                $temp_id = $tech['id'];
            }
            if ($temp_id == $id) {
                $hold = $tech;
            }
        }
        $this->template->set('technology', $hold);
        $this->template->load('pages/techno', 'ajax');
    }

    public function about() {
        $lang = $this->get_locale();
        $this->template->set('title', 'About us');
        $this->template->set('lang', $lang);
        $this->template->load();
    }

    public function contact() {
        $coordinate = array(
            array(
                'lat' => 18.807533,
                'lon' => 99.014636,
                'branch' => "บริษัท วนัสนันท์ สาขาใหญ่",
                'address' => "เลขที่ 398 ถ.เชียงใหม่-ลำปาง</br>ต.ฟ้าฮ่าม อ.เมือง จ.เชียงใหม่ 50000</br>โทร. 053-240829, 243010</br>Fax. 053-249518</br>Email: sales.vanusnun@gmail.com
"
            ),
            array(
                'lat' => 18.769337,
                'lon' => 98.968114,
                'branch' => "การท่าอากาศยานนานาชาติ เชียงใหม่",
                'address' => "ชั้น2 ฝั่งขาออกภายในประเทศ<br/>โทร.053-922246, 081-9930781"
            ),
            array(
                'lat' => 18.801739,
                'lon' => 99.017318,
                'branch' => "นครชัยแอร์ อาเขต เชียงใหม่",
                'address' => "จุดจอดรถนครชัยแอร์ อาเขต<br/>โทร.081-7243240"
            ),
            array(
                'lat' => 18.791661,
                'lon' => 98.999913,
                'branch' => "ตลาดวโรรส เชียงใหม่",
                'address' => "ตลาดวโรรส เชียงใหม่<br/>โทร.053-234866"
            ),
            array(
                'lat' => 18.670641,
                'lon' => 99.052683,
                'branch' => "ปตท. สารภี เชียงใหม่",
                'address' => "ในปั๊ม ปตท. สารภี ฝั่งขาออก<br/>โทร.053-964188"
            ),
            array(
                'lat' => 13.8281,
                'lon' => 100.552185,
                'branch' => "ศูนย์บริการนครชัยแอร์ กรุงเทพฯ",
                'address' => "ถนนกำแพงเพชร 2 ปากซอยวิภาวดีรังสิต 19<br/>โทร.02-9360971, 089-4011206"
            )
        );
        $this->template->set('coordinate', $coordinate);
        $this->template->set('title', 'Contact us');
        $this->template->load();
    }

    public function export() {
        $all_category = array('all_category' => $this->dashboard->get_category());

        $product = $this->dashboard->get_product(NULL, -1);

        $this->template->set('all_product', $product);
        $this->template->set('product_category', $this->load->view('pages/product_category', $all_category, true));
        $this->template->set('slide_promotion', $this->load->view('pages/slide_promotion', '', true));
        $this->template->set('title', 'Export Product');
        $this->template->load();
    }

    public function product_each_cate() {
        if ($this->input->get_post('cate_id', true)) {
            $product = $this->dashboard->get_product(NULL, $this->input->get_post('cate_id'));
        } else {
            $product = $this->dashboard->get_product();
        }

        $this->template->set('all_product', $product);
        $this->template->load('pages/product_each_cate', 'ajax');
    }

    public function recipe() {
        $ingredient = array(
            array(
                'image' => 'rec01.png',
                'name' => 'Chillies',
                'detail' => 'Both Spur and hot chillies are used Dried hot chillies are normally roasted and ground , and used as the seasoning. While dried spur chilies are used mostly in curry paste. The benefit of chilles is to boost the appetite, and the spur chili also acts as a tonic for the system. It dispels gas, phlegm, urine and can also be used to treat indigestion.'
            ),
            array(
                'image' => 'rec02.png',
                'name' => 'Onions',
                'detail' => 'Good quality onions are heavy with dry and smooth skin. Keep them in a ventilated area or in a basket for longer use. Do not refrigerate'
            ),
            array(
                'image' => 'rec03.png',
                'name' => 'Shallots',
                'detail' => 'Shallots The zesty small red onion. The ones with glossy purplish red skin give strong smell, while those with yellowish orange skin are sweeter in taste. The benefit of shallot is todispel gas, helps ease urinary passing, regulates mentrual cycles and is an effectiveb treatment for the common cold.'
            ),
            array(
                'image' => 'rec04.png',
                'name' => 'Garlic',
                'detail' => 'Thai garic has small cloves with rather soft skin but strong aroma. Hanging the garlic in a ventilated area helps extend its shelf life. The benefit of garlic is to tower blood pressure, high cholesterol and blood-sugar. It can also boost the immuns system of the body.'
            ),
            array(
                'image' => 'rec05.png',
                'name' => 'Ginger',
                'detail' => 'Two forms are used in the Thai cooking. Young ginger is usually sliced and sprinkled over steamed fish. The mature one with stronger flavor is best added to the sauces. The benefit of ginger is to dispel gas and regulates the functioning of the gall bladder. It also lessens intestinal contractions, nauseas and vomiting, It can relieve headaches, stomachaches and can alleviate inflammation.'
            ),
            array(
                'image' => 'rec06.png',
                'name' => 'Coriander',
                'detail' => 'A unique tealure of Thai Cookking is the use of coriander root to add aroma to various dishes'
            ),
            array(
                'image' => 'rec07.png',
                'name' => 'Sweet Basil (ho-ra-pha)',
                'detail' => 'The dark green teaves with red stems. Its Teaves are slightly thicker then holy basil, and have own distinctive flavors. The benefit of sweet basil is to help with digestion and relieves abdominal pain,colic and indigestion.'
            ),
        );
        $this->template->set('title', 'Recipe');
        $this->template->set('all_ingredient', $ingredient);
        $this->template->set('all_recipe', $this->dashboard->get_recipe());
        $this->template->load();
    }

    public function detail_recipe($recipe_id) {
        $recipe = $this->dashboard->get_recipe($recipe_id);
        $recipe = $recipe[0];
        $ingredient = $recipe['content'];
        $instruction = $recipe['metadata']['how_to']['value'];
        $recipe_image = "/" . $recipe['file'][0]['path'] . $recipe['file'][0]['name'];
        $this->template->set('ingredient', nl2br($ingredient));
        $this->template->set('instruction', nl2br($instruction));
        $this->template->set('recipe_image', $recipe_image);
        $this->template->set('recipe_name', $recipe['name']);
        $this->template->load('pages/detail_recipe', 'ajax');
    }

    public function product() {
        $this->template->set('title', 'Product');
        $all_category = array('all_category' => $this->dashboard->get_category());
        $this->template->set('product_category', $this->load->view('pages/product_category', $all_category, true));
        $this->template->set('product_filter', $this->load->view('pages/product_filter', '', true));
        $this->template->load();
    }

    public function detail($id) {
        $product = $this->dashboard->get_product($id);
        $product = $product[0];
        $this->template->set('title', $product['name']);
        $this->template->set('fb', $this->load->view('pages/fb_for_product', array('product' => $product), true));
        $all_category = array('all_category' => $this->dashboard->get_category());
        $this->template->set('product', $product);
        $this->template->set('product_category', $this->load->view('pages/product_category', $all_category, true));
        $this->template->set('product_filter', $this->load->view('pages/product_filter', '', true));
        $this->template->load();
    }

    public function promotion() {
        $all_category = array('all_category' => $this->dashboard->get_category());

        $product = $this->dashboard->get_promotion();

        $this->template->set('all_product', $product);
        $this->template->set('product_category', $this->load->view('pages/product_category', $all_category, true));
        $this->template->set('slide_promotion', $this->load->view('pages/slide_promotion', '', true));
        $this->template->set('title', 'Promotion');
        $this->template->load();
    }

    public function checkout() {
        $cart = $this->session->userdata('cart');
        if (count($cart)) {
            $this->template->set('title', 'Checkout');
            $this->template->load();
        } else {
            header("Location: " . site_url('pages/index'));
        }
    }

    public function order_complete() {
        $this->template->set('title', 'Done!!');
        $this->template->load();
        $this->clear_cart();
    }

    public function confirm_checkout() {
        $cart = $this->session->userdata('cart');
        if (count($cart)) {
            $shipping_method = array(
                array(
                    'method' => 'Air',
                    'value' => SHIP_BY_AIR,
                    'description' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident. '
                ),
                array(
                    'method' => 'Bus',
                    'value' => SHIP_BY_BUS,
                    'description' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident. '
                ),
                array(
                    'method' => 'Train',
                    'value' => SHIP_BY_TRAIN,
                    'description' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident. '
                ),
                array(
                    'method' => 'EMS',
                    'value' => SHIP_BY_EMS,
                    'description' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident. '
                )
            );
            $this->template->set('title', 'Shipping method');
            $this->template->set('shipping_method', $shipping_method);
            $this->template->load();
        } else {
            header("Location: " . site_url('pages/index'));
        }
    }

    public function shipping_address() {
        $cart = $this->session->userdata('cart');
        if (count($cart)) {
            $this->template->set('title', 'Shipping Address');
            $this->template->load();
        } else {
            header("Location: " . site_url('pages/index'));
        }
    }

    public function map_control() {
        $address = "<span class='info_box'>398 Chiang Mai - Lampang Rd. T.Fah-Hamm Muang, </br>Chiang Mai , Thailand 50000  </br>Tel. 66 +53 240829, 243010 Fax. 66 +53 249518 </br>Email: Info@Vanusanun.Com</span>";
        $this->template->set('lat', $this->input->get_post('lat'));
        $this->template->set('lon', $this->input->get_post('lon'));
        $this->template->set('address', $address);
        $this->template->load('pages/map_control', 'ajax');
    }

    public function set_locale() {
        set_cookie('default_locale', $this->input->get_post('locale'), (3600 * 24 * 365));
        echo "1";
    }

    public function contact_form() {
        $this->template->load('pages/contact_form', 'ajax');
    }

    public function sendmail() {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_port' => 465,
            'smtp_user' => 'trycatch.service@gmail.com',
            'smtp_pass' => 'gdiupo500510xxx',
            'mailtype' => 'html',
            'charset' => 'utf-8'
        );
        
        $this->load->library('email', $config);
        $this->email->clear();
        $this->email->set_newline("\r\n");
        $this->email->from($this->input->get_post('email'), $this->input->get_post('from'));
        $recv = array('sales.vanusnun@gmail.com');
        $bcc = array('service@trycatch.in.th');
        $this->email->to($recv);
        $this->email->subject("Vanusnun contact form : " . $this->input->get_post('from'));
        $msg = "Name : " . $this->input->get_post('from') . "<br/>Reply to : " . $this->input->get_post('email') . "<br/><br/>" . $this->input->get_post('msg');

        $this->email->message($msg);
        if ($this->email->send()) {
            echo "1";
        } else {
            $this->session->set_userdata('Email_debugger', $this->email->print_debugger());
            echo "0";
        }
    }

    public function set_shipping_method() {
        $this->session->set_userdata('shipping_method', $this->input->post('method'));
        echo $this->input->post('method');
    }

    public function example() {
        /* Locale */

        if (isset($_GET['locale'])) {
            // set locale to cookie
            set_cookie('default_locale', $_GET['locale'], time() + 3600);
        }

        $enable_locale = $this->dashboard->get_enable_locale();
        $this->template->set('enable_locale', $enable_locale);

        $default_locale = $this->dashboard->get_default_locale();
        $this->template->set('default_locale', $default_locale);

        /* Category */

        $all_category = $this->dashboard->get_category();
        $this->template->set('all_category', $all_category);

        $category = $this->dashboard->get_category(1 /* $cat_id */);
        $this->template->set('category', $category);

        $count_category = $this->dashboard->count_category();
        $this->template->set('count_category', $count_category);

        /* Product */

        $all_product = $this->dashboard->get_product();
        $this->template->set('all_product', $all_product);

        $product = $this->dashboard->get_product(1 /* $product_id */);
        $this->template->set('product', $product);

        $cat_product = $this->dashboard->get_product(NULL, 5 /* $cat_id */);
        $this->template->set('cat_product', $cat_product);

        $count_product = $this->dashboard->count_product();
        $this->template->set('count_product', $count_product);

        $count_cat_product = array();

        foreach ($all_category as $cat) {
            $cat_id = (!empty($cat['parent_id']) ? $cat['parent_id'] : $cat['id']);
            $count_cat_product[$cat['name']] = $this->dashboard->count_product($cat_id);
        }

        $this->template->set('count_cat_product', $count_cat_product);

        $this->template->set('title', 'Library Example');
        $this->template->load(NULL, 'ajax');
    }

    public function prepare_stock() {
        $product = $this->dashboard->get_product($this->input->get_post('product_id'));
        $this->template->set('product', $product[0]);
        $this->template->load('pages/prepare_stock', 'ajax');
    }

    public function edit_cart() {
        $product = $this->dashboard->get_product($this->input->get_post('product_id'));
        $this->template->set('product', $product[0]);
        $this->template->load('pages/edit_cart', 'ajax');
    }

    public function get_product_by_cate() {
        $this->template->set('product', $this->dashboard->get_product(NULL, $this->input->get_post('cate_id')));
        $this->template->load('pages/product_from_cate', 'ajax');
    }

    public function video($page = 1) {

        $limit = 10;
        $offset = ($page - 1) * $limit;

        $count_video = $this->db->count_all('video');
        $pagination = get_pagination($page, $count_video, $limit, 1, site_url('pages/video'));

        if ($count_video > $limit) {
            $this->template->set('pagination', $pagination);
        }

        $video_data = $this->db->limit($limit)->offset($offset)->order_by('id', 'desc')->get('video')->result_array();
        $this->template->set('video_data', $video_data);

        $this->template->load('pages/video');
    }

    public function video_view($id) {

        $video = $this->db->where('id', $id)->get('video')->result_array();

        if (!empty($video)) {
            $video = $video[0];
        } else {
            redirect('pages');
        }

        $this->template->set('video', $video);
        $this->template->load('pages/video_view');
    }

}

/* End of file pages.php */
/* Location: ./application/controllers/pages.php */
