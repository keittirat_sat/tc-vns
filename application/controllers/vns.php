<?php

class Vns extends MY_Controller {

    private $total_locale;

    public function __construct() {
        parent::__construct();
        $enable_locale = $this->dashboard->get_enable_locale();
        $this->total_locale = count($enable_locale);
        $this->template->set('locale', $this->get_locale());
        $this->template->set('all_locale', $enable_locale);
//        $this->template->set('css', css_asset('other_page_style.css'));
    }

    private function get_locale() {
        if ($this->input->cookie('default_locale', TRUE)) {
            return $this->input->cookie('default_locale');
        } else {
            return 'en_US';
        }
    }

    public function add_cart() {
        $product_id = $this->input->get_post('product_id');
        $total = $this->input->get_post('total');
        $data = $this->session->userdata('cart');

        $info = $this->dashboard->get_product($product_id);
        $info = $info[0];

        if (!is_array($data)) {
            $data = array();
        }

        if (array_key_exists($product_id, $data)) {
            if ($data[$product_id] + $total <= $info['metadata']['stock']['value']) {
                $data[$product_id] += $total;
            } else {
                $data[$product_id] = $info['metadata']['stock']['value'];
            }
        } else {
            $data[$product_id] = $total;
        }

        $this->session->set_userdata('cart', $data);
        echo json_encode(array('status' => 'success', 'id' => $product_id));
    }

    public function get_cart($echo = true) {
        $data = $this->session->userdata('cart');
        if (!is_array($data)) {
            $data = array();
            $this->session->set_userdata('cart', $data);
        }

        $cart = array();
        foreach ($data as $product_id => $total) {
            $temp = array();
            $info = $this->dashboard->get_product($product_id);
            $info = $info[0];

            $temp['id'] = $product_id;
            $temp['name'] = $info['name'];
            $temp['price'] = $info['metadata']['price']['value'];
            $temp['total'] = $total;
            $temp['stock'] = $info['metadata']['stock']['value'];

            array_push($cart, $temp);
        }

        if ($echo) {
            echo json_encode($cart);
        } else {
            return $cart;
        }
    }

    public function del_cart() {
        $data = $this->session->userdata('cart');
        $product_id = $this->input->get_post('product_id');

        if (!is_array($data)) {
            $data = array();
            $this->session->set_userdata('cart', $data);
        }

        foreach ($data as $p_id => &$total) {
            if ($p_id == $product_id) {
                unset($data[$p_id]);
            }
        }

        $this->session->set_userdata('cart', $data);
        echo json_encode(array('status' => 'success', 'id' => $product_id));
    }

    public function clear_cart() {
        $data = array();
        $this->session->set_userdata('cart', $data);
        echo json_encode(array('status' => 'success'));
    }

    public function index() {
        $this->template->set('menu', 'menu_1');
        $slide = $this->dashboard->get_slide();
        $this->template->set('slide', $slide);
        $this->template->set('css', null);
        $this->template->set('promotion_index', $this->dashboard->get_promotion_index());
        $this->template->set('advertisement_list', $this->dashboard->get_advertisement());
        $this->template->set('technology_list', $this->dashboard->get_technology());
        $this->template->set('lates_product', get_lates_product(4));
        $this->template->set('vdo', $this->dashboard->get_video(1));

        $this->template->set('title', 'Homepage');
        $this->template->load();
    }

    public function aboutus() {
        $this->template->set('menu', 'menu_2');
        $this->template->set('advertisement_list', $this->dashboard->get_advertisement());
        $this->template->set('title', 'Aboutus');
        $this->template->load();
    }

    public function product() {
        $cate = $this->input->get('cate_id');
        $cate = is_numeric($cate) ? $cate : NULL;

        $page = $this->input->get('page');
        $page = is_numeric($page) ? $page : 0;

        $post_per_page = 18;
        $limit = $post_per_page;
        $offset = $post_per_page * $page;

        $all_product = $this->dashboard->get_product(NULL, $cate);
        $total_product = count($all_product);
        $total_page = ceil($total_product / 9);

        $this->template->set('menu', 'menu_3');
        $this->template->set('title', 'All Product');

        $this->template->set('total_page', $total_page);
        $this->template->set('page', $page);
        $this->template->set('cate_id', $cate);

        $this->template->set('all_category', $this->dashboard->get_category());

        $product = $this->dashboard->get_product(NULL, $cate, $offset, $limit);
        foreach ($product as &$each_product) {
            $category = $this->dashboard->get_category($each_product['cat_id']);
            $each_product['cate_info'] = $category;
        }

        $this->template->set('product', $product);
        $this->template->load();
    }

    public function video() {
        $page = $this->input->get('page');
        $page = is_numeric($page) ? $page : 0;

        $post_per_page = 8;
        $offset = $post_per_page * $page;

        $all_vdo = $this->dashboard->get_video();
        $total_page = ceil(count($all_vdo) / $post_per_page);

        $this->template->set('menu', 'menu_4');
        $this->template->set('title', 'Video');
        $this->template->set('vdo', $this->dashboard->get_video($post_per_page, 'desc', $offset));
        $this->template->set('all_vdo', $all_vdo);
        $this->template->set('page', $page);
        $this->template->set('total_page', $total_page);
        $this->template->load();
    }

    public function contactus() {
        $coordinate = array(
            array(
                'lat' => 18.807533,
                'lon' => 99.014636,
                'branch' => "บริษัท วนัสนันท์ สาขาใหญ่",
                'address' => "เลขที่ 398 ถ.เชียงใหม่-ลำปาง</br>ต.ฟ้าฮ่าม อ.เมือง จ.เชียงใหม่ 50000</br>โทร. 053-240829, 243010</br>Fax. 053-249518</br>Email: sales.vanusnun@gmail.com
"
            ),
            array(
                'lat' => 18.769337,
                'lon' => 98.968114,
                'branch' => "การท่าอากาศยานนานาชาติ เชียงใหม่",
                'address' => "ชั้น2 ฝั่งขาออกภายในประเทศ<br/>โทร.053-922246, 081-9930781"
            ),
            array(
                'lat' => 18.801739,
                'lon' => 99.017318,
                'branch' => "นครชัยแอร์ อาเขต เชียงใหม่",
                'address' => "จุดจอดรถนครชัยแอร์ อาเขต<br/>โทร.081-7243240"
            ),
            array(
                'lat' => 18.791661,
                'lon' => 98.999913,
                'branch' => "ตลาดวโรรส เชียงใหม่",
                'address' => "ตลาดวโรรส เชียงใหม่<br/>โทร.053-234866"
            ),
            array(
                'lat' => 18.670641,
                'lon' => 99.052683,
                'branch' => "ปตท. สารภี เชียงใหม่",
                'address' => "ในปั๊ม ปตท. สารภี ฝั่งขาออก<br/>โทร.053-964188"
            ),
            array(
                'lat' => 13.8281,
                'lon' => 100.552185,
                'branch' => "ศูนย์บริการนครชัยแอร์ กรุงเทพฯ",
                'address' => "ถนนกำแพงเพชร 2 ปากซอยวิภาวดีรังสิต 19<br/>โทร.02-9360971, 089-4011206"
            )
        );
        
        $coordinate_partner = array(
            array(
                'lat' => 18.840133,
                'lon' => 99.024745,
                'branch' => "สวนผักโอ้กะจู๋",
                'address' => "ตำบล หนองจ๊อม อำเภอ สันทราย จังหวัด เชียงใหม่"
            ),
            array(
                'lat' => 18.798870,
                'lon' => 99.033216,
                'branch' => "V COMMUNITY | ร้านอาหารลำดีตี้ขัวแดง",
                'address' => "345 หมู่ 3 ต.สันพระเนตร อ.สันทราย เชียงใหม่ 50210"
            ),
            array(
                'lat' => 18.742889,
                'lon' => 98.949683,
                'branch' => "วีดารา รีสอร์ท แอนด์ สปา",
                'address' => "ต.แม่เหียะ อ.เมือง จ.เชียงใหม่"
            ),
            array(
                'lat' => 18.791661,
                'lon' => 98.999913,
                'branch' => "รอยัลปิง การเด้น แอนด์ รีสอร์ท",
                'address' => "ต.บ้านเป้า อ.แม่แตง จ.เชียงใหม่"
            )
        );
        
        $this->template->set('coordinate', $coordinate);
        $this->template->set('coordinate_partner', $coordinate_partner);
        $this->template->set('menu', 'menu_5');
        $this->template->set('advertisement_list', $this->dashboard->get_advertisement());
        $this->template->set('title', 'Contact us');
        $this->template->load();
    }

    public function player($id) {
        $video = $this->dashboard->get_video_id($id);
        $all_vdo = $this->dashboard->get_video();

        $this->template->set("title", $video->title);
        $this->template->set('menu', 'menu_4');
        $this->template->set('post', $video);
        $this->template->set('all_vdo', $all_vdo);
        $this->template->load();
    }

    /* Other Operation */

    public function add_subscribe() {
        $data = json_decode($this->dashboard->get_option('subscribe'));
        $data = (array) $data;
        $exist = true;
        if (!empty($data)) {
            foreach ($data as $email) {
                if ($email == $this->input->get_post('email')) {
                    $exist = false;
                }
            }
        } else {
            $data = array();
        }

        if ($exist) {
            if (array_push($data, $this->input->get_post('email'))) {
                $json_data = json_encode($data);
                $this->dashboard->set_option('subscribe', $json_data);
                echo json_encode(array("status" => "success"));
            } else {
                echo json_encode(array("status" => "Push Error"));
            }
        } else {
            echo json_encode(array("status" => "Email on exist"));
        }
        //print_r($data);
    }

    public function set_locale() {
        set_cookie('default_locale', $this->input->get_post('locale'), (3600 * 24 * 365));
        echo json_encode(array("status" => "success"));
    }

    public function checkout() {
        $this->template->set("title", "Checkout");
        $this->template->set('menu', 'menu_3');
        $this->template->set('post', $this->get_cart(false));
        $this->template->load();
    }

    public function process() {
        $this->template->set("title", "Checkout");
        $this->template->set('menu', 'menu_3');
        $this->template->load();
    }

    public function shipping_address() {
        $shipping_method = $this->input->post('shipping');
        if (!is_numeric($shipping_method)) {
            redirect(site_url('vns'));
        } else {
            $this->template->set("title", "Checkout");
            $this->template->set('menu', 'menu_3');
            $this->template->set('shipping_method', $shipping_method);
            $this->template->set('post', $this->get_cart(false));
            $this->template->load();
        }
    }

    public function order_complete() {
        $this->template->set("title", "Finish");
        $this->template->set('menu', 'menu_3');
        $this->template->set('post', $this->get_cart(false));
        $this->template->load();
    }

    public function sendmail() {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_port' => 465,
            'smtp_user' => 'trycatch.service@gmail.com',
            'smtp_pass' => 'gdiupo500510xxx',
            'mailtype' => 'html',
            'charset' => 'utf-8'
        );

        $name = $this->input->post('user_name');
        $email = $this->input->post('user_email');
        $detail = $this->input->post('detail');


        $name = empty($name) ? "Anonymous" : $name;

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from("sales.vanusnun@gmail.com", "Vanusnun Website");
        if (!empty($email)) {
            $this->email->reply_to($email, $name);
        }

        $this->email->to(array('sales.vanusnun@gmail.com'));
        $this->email->bcc(array('service@trycatch.in.th', 'trycatch.service@gmail.com'));

        $this->email->subject("Vanusnun Website: {$name}");
        $this->email->message(auto_typography($detail));

        if ($this->email->send()) {
            echo json_encode(array("status" => "success"));
        } else {
            echo json_encode(array("status" => "fail"));
        }
    }

}
