<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH . "third_party/MX/Loader.php";

/**
 * MY_Loader 
 * 
 * Modified model loading for sake useing case sensitive.
 * 
 * @package Core
 * @subpackage Core
 */
class MY_Loader extends MX_Loader
{

    /**
     * Overriding model loader
     * 
     * @param string $model
     * @param string $object_name
     * @param string $connect
     * @return mixed
     */
    public function model($model, $object_name = NULL, $connect = FALSE)
    {
        if (is_array($model)) return $this->models($model);

        ($_alias = $object_name) OR $_alias = basename($model);

        if (in_array($_alias, $this->_ci_models, TRUE)) return CI::$APP->$_alias;

        /* check module */
        /* Modified case sensitive file name */
        list($path, $_model) = Modules::find($model, $this->_module, 'models/');

        if ($path == FALSE)
        {
            /* check application & packages */
            parent::model($model, $object_name);
        }
        else
        {
            class_exists('CI_Model', FALSE) OR load_class('Model', 'core');

            if ($connect !== FALSE AND !class_exists('CI_DB', FALSE))
            {
                if ($connect === TRUE) $connect = '';
                $this->database($connect, FALSE, TRUE);
            }

            Modules::load_file($_model, $path);

            $model = ucfirst($_model);
            CI::$APP->$_alias = new $model();

            $this->_ci_models[] = $_alias;
        }

        return CI::$APP->$_alias;
    }

}

/* End of file MY_Loader.php */
/* Location: ./application/core/MY_Loader.php */