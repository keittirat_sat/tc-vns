<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MY_Model
 * 
 * @package Core
 * @subpackage Models
 * @since 1.0
 * @author Nomkhonwaan ComputerScience 
 */
class MY_Model extends CI_Model
{

    protected $table = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function insert($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function find_id($id)
    {
        if ($id == NULL)
        {
            return NULL;
        }

        $this->db->where('id', $id);
        $query = $this->db->get($this->table);

        $result = $query->result_array();
        return (count($result) > 0 ? $result[0] : NULL);
    }

    public function find_all($sort = 'id', $order = 'asc')
    {
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
    }

    public function delete($id)
    {
        if ($id != NULL)
        {
            $this->db->where('id', $id);
            $this->db->delete($this->table);
        }
    }

    /* Custom model --------------------------------------------------------- */

    public function save($data = array())
    {
        /* Created filed is exist */
        if ($this->db->field_exists('created', $this->table) AND empty($data['id']))
        {
            $data['created'] = date('Y-m-d H:i:s');
        }

        $data['modified'] = date('Y-m-d H:i:s');

        if (!empty($data['id']))
        {
            /* Prevent duplicate key content */

            $data_id = $data['id'];
            unset($data['id']);

            $this->update($data_id, $data);

            return $data_id;
        }
        else
        {
            return $this->insert($data);
        }
    }

    public function get($uid = NULL, $id = NULL, $offset = NULL, $limit = NULL, $order = 'asc')
    {
        /* Set condition  */

        if (isset($uid))
        {
            $this->db->where('uid', $uid);
        }

        if (isset($id))
        {
            $this->db->where('id', $id);
        }

        if (isset($offset) AND isset($limit))
        {
            $this->db->limit($limit, $offset);
        }

        /* Set order  */

        $this->db->order_by('id', $order);

        /* Query  */

        $result = $this->db->get($this->table)->result_array();

        /* Return result  */

        if (isset($id) AND !empty($result))
        {
            return $result[0];
        }

        return $result;
    }

    public function find($uid = NULL, $cond = NULL, $offset = NULL, $limit = NULL, $order = 'asc')
    {
        if (isset($cond))
        {
            $this->db->where($cond);
        }

        return $this->get($uid, NULL, $offset, $limit, $order);
    }

    public function count($uid = NULL, $cond = NULL)
    {
        if (isset($uid))
        {
            $this->db->where('uid', $uid);
        }

        if (isset($cond))
        {
            $this->db->where($cond);
        }

        return $this->db->count_all_results($this->table);
    }

}

/* End of file MY_Model.php */
/* Location: ./system/application/libraries/MY_Model.php */