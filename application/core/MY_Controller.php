<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MY_Controller
 * 
 * @package Core
 * @subpackage Controllers
 * @since 1.0
 * @author Nomkhonwaan ComputerScience
 */
class MY_Controller extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();

        $default_locale = $this->dashboard->get_default_locale();
        
        if(!defined('DEFAULT_LOCALE'))
        {
            define('DEFAULT_LOCALE', $default_locale);
        }
        
        /* Initialize */
        $this->dashboard();
    }

    public function dashboard()
    {
        $segment = $this->uri->segment(1);

        if (strcasecmp($segment, 'dashboard'))
        {
            // not dashboard
            return FALSE;
        }

        /* Router variable */
        $class = $this->router->fetch_class();
        $this->template->set('class', $class);

        $method = $this->router->fetch_method();
        $this->template->set('method', $method);

        /* Create and set menu array to template */
        $this->menu();
    }

    private function menu()
    {
        /* Special menu with notification */
        $notify = Modules::run('Cart/Dsb_Invoice/get_notify');
        $order_text = 'Order' . ($notify > 0 ? ' <span class="notification">[' . $notify . ']</span>' : NULL);

        /* Array of menu */

        $menu = array(
            // array(
            //     'text' => 'Overview',
            //     'URL' => site_url('dashboard'),
            //     'cond' => array(
            //         'class' => 'Dsb_Dashboard'
            //     )
            // ),
            array(
                'text' => 'Decoration',
                'URL' => NULL, //site_url('dashboard/page'),
                'cond' => array(
                    'class' => 'Dsb_Page'
                ),
                'sub' => array(
                    array(
                        'text' => 'Slide',
                        'URL' => site_url('dashboard/page/slide'),
                        'cond' => array(
                            'class' => 'Dsb_Page',
                            'method' => 'slide'
                        )
                    ),    
                    array(
                        'text' => 'Promotion',
                        'URL' => site_url('dashboard/page/promotion'),
                        'cond' => array(
                            'class' => 'Dsb_Page',
                            'method' => 'promotion'
                        )
                    ),
                    array(
                        'text' => 'Technology',
                        'URL' => site_url('dashboard/page/technology'),
                        'cond' => array(
                            'class'=> 'Dsb_Page',
                            'method' => 'technology'
                        )
                    ),
                    array(
                        'text' => 'Advertisement',
                        'URL' => site_url('dashboard/page/advertisement'),
                        'cond' => array(
                            'class' => 'Dsb_Page',
                            'method' => 'advertisement'
                        )
                    )
                )
            ),
            array(
                'text' => 'Product',
                'URL' => site_url('dashboard/post'),
                'cond' => array(
                    'class' => 'Dsb_Post'
                ),
                'sub' => array(
                    array(
                        'text' => 'All Product',
                        'URL' => site_url('dashboard/post'),
                        'cond' => array(
                            'class' => 'Dsb_Post'
                        )
                    ),
                    array(
                        'text' => 'New Product',
                        'URL' => site_url('dashboard/post/add'),
                        'cond' => array(
                            'class' => 'Dsb_Post',
                            'method' => 'add'
                        )
                    )
                )
            ),
            array(
                'text' => 'Recipe',
                'URL' => site_url('dashboard/recipe'),
                'cond' => array(
                    'class' => 'Dsb_Recipe'
                ),
                'sub' => array(
                    array(
                        'text' => 'All Recipe',
                        'URL' => site_url('dashboard/recipe'),
                        'cond' => array(
                            'class' => 'Dsb_Recipe',
                        )
                    ),
                    array(
                        'text' => 'New Recipe',
                        'URL' => site_url('dashboard/recipe/add'),
                        'cond' => array(
                            'class' => 'Dsb_Recipe',
                            'method' => 'add'
                        )
                    )
                )
            ),
            array(
                'text' => $order_text,
                'URL' => site_url('dashboard/order'),
                'cond' => array(
                    'class' => 'Dsb_Order'
                )
            ),
            array(
                'text' => 'Category',
                'URL' => site_url('dashboard/category'),
                'cond' => array(
                    'class' => 'Dsb_Category'
                )
            ),
            array(
                'text' => 'Video',
                'URL' => site_url('dashboard/video'),
                'cond' => array(
                    'class' => 'Dsb_Video'
                ),
                'sub' => array(
                    array(
                        'text' => 'All Video',
                        'URL' => site_url('dashboard/video'),
                        'cond' => array(
                            'class' => 'Dsb_Video',
                        )
                    ),
                    array(
                        'text' => 'New Video',
                        'URL' => site_url('dashboard/video/add'),
                        'cond' => array(
                            'class' => 'Dsb_Video',
                            'method' => 'add'
                        )
                    )
                )
            )
        );

        $this->template->set('menu', $menu);

        $navigation = $this->template->element('navigation');
        $hotlinks = $this->template->element('hotlinks');

        $this->template->set('navigation', $navigation);
        $this->template->set('hotlinks', $hotlinks);
    }

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */