<?php

/**
 * Dashboard Library
 * 
 * @package Dashboard
 * @subpackage Library
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
class Dashboard {

    private $CI;

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->database();
    }

    public function get_option($name, $pure = FALSE) {
        $result = $this->CI->db->get_where('option', array('name' => $name))->result_array();

        if ($pure)
            return (!empty($result) ? $result[0] : NULL);
        else
            return (!empty($result) ? $result[0]['value'] : NULL);
    }

    public function set_option($name, $value) {
        /* Check existing option */
        $option = $this->get_option($name, TRUE);

        if (!empty($option)) {
            /* Do update option */
            $data['value'] = $value;
            $data['modified'] = date('Y-m-d H:i:s');

            $this->CI->db->where('id', $option['id']);
            $this->CI->db->update('option', $data);
        } else {
            /* Do insert new option */

            $data = array(
                'name' => $name,
                'value' => $value,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            );

            $this->CI->db->insert('option', $data);
        }
    }

    /* Localization --------------------------------------------------------- */

    public function get_enable_locale() {
        $enable_locale = $this->get_option('locale');

        return json_decode($enable_locale, TRUE);
    }

    public function get_default_locale() {
        $default_locale = get_cookie('default_locale');

        return (!empty($default_locale) ? $default_locale : 'en_US');
    }

    private function filter_locale($result_array) {
        $default_locale = $this->get_default_locale();
        $return_result = array();

        foreach ($result_array as $index => $data) {
            if (!strcmp($data['locale'], $default_locale)) {
                array_push($return_result, $data);
            }
        }

        return $return_result;
    }

    /* Category ------------------------------------------------------------- */

    public function get_category($cat_id = NULL, $offset = NULL, $limit = NULL, $order = 'asc') {
        if (isset($cat_id)) {
            $this->CI->db->or_where('id', $cat_id);
            $this->CI->db->or_where('parent_id', $cat_id);
        }

        if (isset($offset) AND isset($limit)) {
            $this->CI->db->limit($limit, $offset);
        }

        $this->CI->db->where('status', PUBLISH);

        $this->CI->db->order_by('id', $order);

        $result = $this->CI->db->get('category')->result_array();

        return $this->filter_locale($result);
    }

    public function count_category() {
        $this->CI->db->where('status', PUBLISH);
        $this->CI->db->where('parent_id', 0);

        return $this->CI->db->count_all_results('category');
    }

    /* File ----------------------------------------------------------------- */

    private function get_file($id = NULL, $offset = NULL, $limit = NULL, $order = 'asc') {
        if (isset($id)) {
            $this->CI->db->where('id', $id);
        }

        if (isset($offset) AND isset($limit)) {
            $this->CI->db->limit($limit, $offset);
        }

        $this->CI->db->where('status', PUBLISH);

        $this->CI->db->order_by('id', $order);

        $result = $this->CI->db->get('file')->result_array();

        if (isset($id)) {
            return (!empty($result) ? $result[0] : NULL);
        }

        return $result;
    }

    private function get_file_post($post_id, $file_id = NULL, $offset = NULL, $limit = NULL) {
        $this->CI->db->where('post_id', $post_id);

        if (isset($file_id)) {
            $this->CI->db->where('file_id', $file_id);
        }

        if (isset($offset) AND isset($limit)) {
            $this->CI->db->limit($limit, $offset);
        }

        $file_post = $this->CI->db->get('file_post')->result_array();
        $file = array();

        if (!empty($file_post)) {
            foreach ($file_post as &$value) {
                $db_file = $this->get_file($value['file_id']);
                array_push($file, $db_file);
            }
        }

        return $file;
    }

    /* Post ----------------------------------------------------------------- */

    private function get_post_metadata($post_id) {
        $this->CI->db->where('post_id', $post_id);
        $result = $this->CI->db->get('post_metadata')->result_array();

        if (!empty($result)) {
            foreach ($result as $index => $value) {
                $result[$value['name']] = $value;
                unset($result[$index]);
            }
        }

        return $result;
    }

    public function get_product($post_id = NULL, $cat_id = NULL, $offset = NULL, $limit = NULL, $order = 'asc') {
        if (is_numeric($post_id)) {
            $this->CI->db->or_where('id', $post_id);
            $this->CI->db->or_where('parent_id', $post_id);
        }

        if (is_numeric($cat_id)) {
            $this->CI->db->where('cat_id', $cat_id);
        }

        if (is_numeric($offset) AND is_numeric($limit)) {
            $this->CI->db->limit($limit, $offset);
        } else if (is_numeric($limit)) {
            $this->CI->db->limit($limit, 0);
        }

        $this->CI->db->where('type', POST_PRODUCT);
        $this->CI->db->where('status', PUBLISH);

        $this->CI->db->order_by('id', $order);

        $result = $this->CI->db->get('post')->result_array();

        $filter_result = $this->filter_locale($result);

        /* Get product image */
        foreach ($filter_result as &$product) {
            /* File */
            $file = $this->get_file_post((!empty($product['parent_id']) ? $product['parent_id'] : $product['id']));
            $product['file'] = $file;

            /* Metadata */
            $metadata = $this->get_post_metadata((!empty($product['parent_id']) ? $product['parent_id'] : $product['id']));
            $product['metadata'] = $metadata;
        }

        return $filter_result;
    }

    public function get_recipe($post_id = NULL, $cat_id = NULL, $offset = NULL, $limit = NULL, $order = 'asc') {
        if (isset($post_id)) {
            $this->CI->db->or_where('id', $post_id);
            $this->CI->db->or_where('parent_id', $post_id);
        }

        if (isset($cat_id)) {
            $this->CI->db->where('cat_id', $cat_id);
        }

        if (isset($offset) AND isset($limit)) {
            $this->CI->db->limit($limit, $offset);
        }

        $this->CI->db->where('type', POST_RECIPE);
        $this->CI->db->where('status', PUBLISH);

        $this->CI->db->order_by('id', $order);

        $result = $this->CI->db->get('post')->result_array();

        $filter_result = $this->filter_locale($result);

        /* Get product image */
        foreach ($filter_result as &$product) {
            /* File */
            $file = $this->get_file_post((!empty($product['parent_id']) ? $product['parent_id'] : $product['id']));
            $product['file'] = $file;

            /* Metadata */
            $metadata = $this->get_post_metadata((!empty($product['parent_id']) ? $product['parent_id'] : $product['id']));
            $product['metadata'] = $metadata;
        }

        return $filter_result;
    }

    public function get_promotion($post_id = NULL, $cat_id = NULL, $offset = NULL, $limit = NULL, $order = 'asc') {
        $this->CI->db->where('promotion', 1);

        return $this->get_product($post_id, $cat_id, $offset, $limit, $order);
    }

    public function count_product($cat_id = NULL) {
        if (isset($cat_id)) {
            $this->CI->db->where('cat_id', $cat_id);
        }

        $this->CI->db->where('status', PUBLISH);
        $this->CI->db->where('parent_id', 0);

        return $this->CI->db->count_all_results('post');
    }

    /* Page ----------------------------------------------------------------- */

    public function get_slide() {
        $slide = $this->get_option('slide');
        $slide = json_decode($slide, TRUE);

        if (!empty($slide)) {
            foreach ($slide as $index => $value) {
                $file = $this->get_file($value['file_id']);
                $slide[$index]['file'] = $file;
            }
        }

        return $slide;
    }

    public function get_promotion_cover() {
        $promotion_cover = $this->get_option('promotion_cover');

        if (!empty($promotion_cover)) {
            return $this->get_file($promotion_cover);
        }

        return NULL;
    }

    public function get_promotion_index() {
        $promotion_index = json_decode($this->get_option('promotion_index'), TRUE);

        $default_locale = $this->get_default_locale();

        $pattern = array('/&gt;/', '/&lt;/');
        $replace = array('>', '<');

        return preg_replace($pattern, $replace, $promotion_index[$default_locale]);
    }

    public function get_technology() {
        $this->CI->db->where('parent_id', 0);
        $this->CI->db->where('type', POST_TECHNOLOGY);
        $this->CI->db->order_by('id', 'desc');

        $result = $this->CI->db->get('post')->result_array();

        foreach ($result as $value) {
            $this->CI->db->where('parent_id', $value['id']);
            $children = $this->CI->db->get('post')->result_array();

            array_push($result, $children[0]);
        }

        $filter_result = $this->filter_locale($result);

        /* Get product image */
        foreach ($filter_result as &$technology) {
            /* File */
            $file = $this->get_file_post((!empty($technology['parent_id']) ? $technology['parent_id'] : $technology['id']));
            $technology['file'] = $file;

            /* Metadata */
            $metadata = $this->get_post_metadata((!empty($technology['parent_id']) ? $technology['parent_id'] : $technology['id']));
            $technology['metadata'] = $metadata;
        }

        return $filter_result;
    }

    public function get_advertisement() {
        $advertisement_list = $this->get_option('advertisement');
        $advertisement_list = json_decode($advertisement_list, TRUE);

        if (!empty($advertisement_list)) {
            foreach ($advertisement_list as &$advertisement) {
                $file = $this->get_file($advertisement['file_id']);
                $advertisement['file'] = $file;
            }
        }

        return $advertisement_list;
    }

    public function get_video($limit = null, $order = 'desc', $offset = null) {
        $this->CI->db->from('video')->order_by('id', $order);

        if (is_numeric($limit)) {
            if (is_numeric($offset)) {
                $this->CI->db->limit($limit, $offset);
            } else {
                $this->CI->db->limit($limit);
            }
        }

        return $this->CI->db->get()->result();
    }

    public function get_video_id($id) {
        $temp = $this->CI->db->from('video')->where('id', $id)->get()->result();
        return $temp[0];
    }

}

/* End of file Dashboard.php */
/* Location: ./application/libraries/Dashboard.php */
