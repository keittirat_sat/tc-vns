<div class="container padding_content_other">
    <div class="row">
        <span class="col-xs-1"></span><h1 class="green col-xs-11 col-xs-offset-1 quark txt_shadow uppercase">การสั่งซื้อสำเร็จ&nbsp;<b>Transaction successful</b></h1>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <?php echo image_asset("progress_4.png", NULL, array('style' => 'margin:0px auto;', 'class' => 'img-responsive')) ?>
        </div>
    </div>
    <div class="row margin_top_20">
        <div class="col-xs-12">
            <p class="bg-success txt_center quark" style="font-size: 24px; padding: 15px 0px 10px;"><?php __('Thank you, Your Transaction has already saved, please wait our call, we will contact you back.'); ?></p>
        </div>
    </div>
</div>