<div class="container padding_content_other">
    <div class="row">
        <span class="col-xs-1"></span><h1 class="green col-xs-11 col-xs-offset-1 quark txt_shadow uppercase">วีดีโอ&nbsp;<b>Video</b></h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('vns/video'); ?>">Video</a></li>
        <li class="active"><?php echo $post->title; ?></li>
    </ol>
    <div class="row">
        <div class="col-xs-8">
            <?php $str_source = explode('/', $post->URL); ?>
            <iframe width="617" height="462" src="//www.youtube.com/embed/<?php echo $str_source[3]; ?>" frameborder="0" allowfullscreen></iframe>
            <h3 class="green quark txt_shadow_thin uppercase"><?php echo $post->title ?></h3>
            <?php if ($post->desc != ""): ?>
                <p><?php echo $post->desc ?></p>
            <?php endif; ?>
            <p class="txt_right">
                <button class=" btn btn-default" id="back_btn"><i class="glyphicon glyphicon-circle-arrow-left"></i>&nbsp;Go Back</button>
            </p>
        </div>
        <div class="col-xs-4">
            <h1 class='quark green block_title'><b>All</b>Video</h1>
            <ul class="sidebar_vdo_list margin_top_20">
                <?php foreach ($all_vdo as $each_vdo): ?>
                    <li <?php echo $post->id == $each_vdo->id ? "class='active'" : ""; ?>>
                        <i class="glyphicon glyphicon-chevron-right green"></i>
                        <a class="link_inherite" href="<?php echo site_url("vns/player/{$each_vdo->id}/" . slug($each_vdo->title)); ?>"><?php echo $each_vdo->title; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <h1 class="quark green block_title non_underline margin_bottom_20"><b>Feature</b>Video</h1>
    <div class="row">
        <?php $f = 0; ?>
        <?php $total_video = count($all_vdo) - 1 ?>
        <?php $now = array(); ?>
        <?php array_push($now, $post->id); ?>
        <?php while ($f < 3): ?>
            <?php $index = rand(0, $total_video); ?>
            <?php $each_vdo = $all_vdo[$index]; ?>
            <?php if (!in_array($each_vdo->id, $now)): ?>
                <div class="col-xs-4">
                    <?php $str_source = explode('/', $each_vdo->URL); ?>
                    <div class="thumbnail margin_bottom_10">
                        <img class="img-responsive" src="<?php echo "http://img.youtube.com/vi/" . $str_source[3] . "/hqdefault.jpg"; ?>">
                    </div>
                    <p class="green txt_right txt_shadow_thin margin_top_0 margin_bottom_0">
                        <a href="<?php echo site_url("vns/player/{$each_vdo->id}/" . slug($each_vdo->title)); ?>" class="link_inherite"><?php echo $each_vdo->title; ?></a>
                    </p>
                    <p class="txt_right">
                        <a href="<?php echo site_url("vns/player/{$each_vdo->id}/" . slug($each_vdo->title)); ?>" class="link_inherite" style="font-weight: bold; font-size: 17px;">
                            Play <i class="glyphicon glyphicon-play"></i>
                        </a>
                    </p>
                </div>
                <?php array_push($now, $each_vdo->id); ?>
                <?php $f++; ?>
            <?php endif; ?>
        <?php endwhile; ?>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('#back_btn').click(function() {
            history.back();
        });
    });
</script>