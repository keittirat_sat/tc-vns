<div class="container padding_content_other">
    <div class="row">
        <span class="col-xs-1"></span><h1 class="green col-xs-11 col-xs-offset-1 quark txt_shadow uppercase">ช่องทางการจัดส่ง&nbsp;<b>Shipping Method</b></h1>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <?php echo image_asset("progress_2.png", NULL, array('style' => 'margin:0px auto;', 'class' => 'img-responsive')) ?>
        </div>
    </div>
    <form role="form" method="post" action="<?php echo site_url('vns/shipping_address'); ?>">
        <div class="row margin_top_20">
            <div class="col-xs-1"></div>
            <label class="col-xs-5 shipping_method method_1 active">
                <input type="radio" name="shipping" value="1" checked="checked">
            </label>
            <label class="col-xs-5 shipping_method method_2">
                <input type="radio" name="shipping" value="2">
            </label>
        </div>
        <div class="row">
            <div class="col-xs-1"></div>
            <label class="col-xs-5 shipping_method method_3">
                <input type="radio" name="shipping" value="3">
            </label>
            <label class="col-xs-5 shipping_method method_4">
                <input type="radio" name="shipping" value="4">
            </label>
        </div>
        <div class="row">
            <div class="col-xs-11 txt_right">
                <button type="submit" class="btn btn-default">Continue&nbsp;<i class="glyphicon glyphicon-chevron-right"></i></button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(function() {
        $('[name=shipping]').change(function() {
            var current = $('[name=shipping]:checked').val();
            $('.shipping_method.active').removeClass('active');
            $('.method_' + current).addClass('active');
        });
    });
</script>