<div class='container padding_content_other'>
    <h1 class="green quark txt_shadow txt_center uppercase">วีดีโอ&nbsp;<b>Video</b></h1>
    <div class="row">
        <div class="col-xs-12 txt_center">
            <ul class="pagination pagination-sm">
                <?php for ($i = 0; $i < $total_page; $i++): ?>
                    <li <?php echo $i == $page ? "class='active'" : ""; ?>>
                        <a href="<?php echo "?page={$i}"; ?>"><?php echo $i + 1; ?></a>
                    </li>
                <?php endfor; ?>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-8">
            <?php foreach ($vdo as $each_vdo): ?>
                <div class="col-xs-6">
                    <?php $str_source = explode('/', $each_vdo->URL); ?>
                    <div class="thumbnail margin_bottom_10">
                        <img class="img-responsive" src="<?php echo "http://img.youtube.com/vi/" . $str_source[3] . "/hqdefault.jpg"; ?>">
                    </div>
                    <p class="green txt_right font_bold font_size_16 margin_top_0 margin_bottom_0 cufon">
                        <a href="<?php echo site_url("vns/player/{$each_vdo->id}/" . slug($each_vdo->title)); ?>" class="link_inherite"><?php echo $each_vdo->title; ?></a>
                    </p>
                    <p class="txt_right">
                        <a href="<?php echo site_url("vns/player/{$each_vdo->id}/" . slug($each_vdo->title)); ?>" class="link_inherite" style="font-weight: bold; font-size: 17px;">
                            Play <i class="glyphicon glyphicon-play"></i>
                        </a>
                    </p>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="col-xs-4">
            <h1 class='quark green block_title'><b>All</b>Video</h1>
            <ul class="sidebar_vdo_list margin_top_20">
                <?php foreach ($all_vdo as $each_vdo): ?>
                    <li>
                        <i class="glyphicon glyphicon-chevron-right green"></i>
                        <a class="link_inherite" href="<?php echo site_url("vns/player/{$each_vdo->id}/" . slug($each_vdo->title)); ?>"><?php echo $each_vdo->title; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 txt_center">
            <ul class="pagination pagination-sm">
                <?php for ($i = 0; $i < $total_page; $i++): ?>
                    <li <?php echo $i == $page ? "class='active'" : ""; ?>>
                        <a href="<?php echo "?page={$i}"; ?>"><?php echo $i + 1; ?></a>
                    </li>
                <?php endfor; ?>
            </ul>
        </div>
    </div>
</div>