<div class="container padding_content_other">
    <h1 class="green quark txt_shadow txt_center uppercase">Contact<b>Us</b></h1>
    <div class="row">
        <div class="col-xs-8">
            <div id="map_canvas" class="margin_bottom_10"></div>
            <h1 class="green quark"><b>Chiang Mai Vanusnun</b></h1>
            <p>
                <?php if ($locale == "en_US"): ?>
                    398 Chiang Mai - Lampang Rd. T.Fah-Hamm Muang Chiang Mai , Thailand 50000
                <?php else: ?>
                    เลขที่ 398 ถ.เชียงใหม่-ลำปาง ต.ฟ้าฮ่าม อ.เมือง จ.เชียงใหม่ 50000
                <?php endif; ?>
            </p>
            <p><b class="green">Tel.</b> +66 53 240 829 <b class="green">Fax.</b> +66 53 249 518</p>
            <p><b class="green">Email. </b> sales.vanusnun@gmail.com</p>

            <form class="margin_top_30" role="form" method="post" id="contact_form" action="<?php echo site_url('vns/sendmails'); ?>">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input type="text" class="form-control" placeholder="Name" name="user_name" required="required">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                        <input type="email" class="form-control" placeholder="Email" name="user_email" required="required">
                    </div>
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="detail" rows="12" required="required"></textarea>
                </div>
                <div class="form-group txt_right">
                    <button class="btn btn-default" id="contact_sent" data-loadin-text="Sending..."><i class="glyphicon glyphicon-ok"></i>&nbsp;SEND</button>
                </div>
            </form>
        </div>
        <div class="col-xs-4">
            <!--Map Coor-->
            <h1 class='quark green block_title'><b>Location</b></h1>
            <?php foreach ($coordinate as $index => $coor): ?>
                <div class="row">
                    <div class="col-xs-2 pin_list <?php echo $index == 0 ? "active" : "" ?>" data-lat="<?php echo $coor['lat']; ?>" data-lnt="<?php echo $coor['lon']; ?>" id="map_<?php echo $index; ?>" data-map-id="<?php echo $index; ?>"></div>
                    <div class="col-xs-10">
                        <p class="green"><?php echo $coor['branch']; ?></p>
                        <p class="font_size_12"><?php echo $coor['address']; ?></p>
                    </div>
                </div>
            <?php endforeach; ?>
            <!--/Map Coor-->

            <?php $con = $index + 1; ?>

            <!--Our Partner-->
            <h1 class='quark blue block_title'><b>Our</b>Partner</h1>
            <?php foreach ($coordinate_partner as $index => $coor): ?>
                <div class="row partner_location">
                    <div class="col-xs-2 pin_list" data-lat="<?php echo $coor['lat']; ?>" data-lnt="<?php echo $coor['lon']; ?>" id="map_<?php echo $index + $con; ?>" data-map-id="<?php echo $index + $con; ?>"></div>
                    <div class="col-xs-10">
                        <p class="blue"><?php echo $coor['branch']; ?></p>
                        <p class="font_size_12"><?php echo $coor['address']; ?></p>
                    </div>
                </div>
            <?php endforeach; ?>
            <!--/Our Partner-->

            <!--Subscribe-->
            <h1 class='quark green block_title margin_top_30'><b>NewsLetter</b></h1>
            <div class="row">
                <div class="col-xs-12">
                    <p>E-News Letter Keep following us Our news and Product</p>
                    <form role="form" action="<?php echo site_url("vns/add_subscribe"); ?>" class="subscribe_form">
                        <div class="input-group">
                            <input type="email" placeholder="Email" name="email" required="required" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn btn-success scb_btn" type="submit" data-loading-text="Adding...">SUBMIT</button>
                            </span>
                        </div><!-- /input-group -->
                    </form>
                </div>
            </div>
            <!--/Subscribe-->

            <!--Facebook-->
<!--            <h1 class='quark green block_title margin_top_30'><b>Facebook</b></h1>
            <div class="fb-like-box" data-href="https://www.facebook.com/pages/%E0%B9%80%E0%B8%8A%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B9%83%E0%B8%AB%E0%B8%A1%E0%B9%88%E0%B8%A7%E0%B8%99%E0%B8%B1%E0%B8%AA%E0%B8%99%E0%B8%B1%E0%B8%99%E0%B8%97%E0%B9%8C/108391455897916" data-width="293" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="true" data-show-border="true"></div>-->
            <!--/Facebook-->
        </div>
    </div>
</div>

<style>
    .pin_list{
        cursor: pointer;
    }
</style>

<script type="text/javascript">
    $(function() {
        $('#map_canvas').gmap({
            'mapTypeId': google.maps.MapTypeId.TERRAIN,
        }).bind('init', function(ev, map) {
<?php foreach ($coordinate as $index => $coor): ?>
                $('#map_canvas').gmap('addMarker', {id:<?php echo $index; ?>, 'position': '<?php echo $coor['lat'] . "," . $coor['lon'] ?>', 'bounds': true, 'icon': '<?php echo image_asset_url("contactus_mappin_active.png") ?>'}).click(function() {
                    var info = "<h2 class='quark'><?php echo $coor['branch']; ?></h4>";
                    $('.pin_list.active').removeClass('active');
                    $('#map_<?php echo $index; ?>').addClass('active');
                    $('#map_canvas').gmap('openInfoWindow', {'content': info}, this);
                });
<?php endforeach; ?>
<?php $con = $index + 1 ?>
<?php foreach ($coordinate_partner as $index => $coor): ?>
                $('#map_canvas').gmap('addMarker', {id:<?php echo $index + $con; ?>, 'position': '<?php echo $coor['lat'] . "," . $coor['lon'] ?>', 'bounds': true, 'icon': '<?php echo image_asset_url("contactus_mappin_blue_active.png") ?>'}).click(function() {
                    var info = "<h2 class='quark'><?php echo $coor['branch']; ?></h4>";
                    $('.pin_list.active').removeClass('active');
                    $('#map_<?php echo $index + $con; ?>').addClass('active');
                    $('#map_canvas').gmap('openInfoWindow', {'content': info}, this);
                });
<?php endforeach; ?>
        });

        $('.pin_list').click(function() {
            var id = $(this).attr('data-map-id');
            $('.pin_list.active').removeClass('active');
            $('#map_' + id).addClass('active');

            var lat = $(this).attr('data-lat');
            var lnt = $(this).attr('data-lnt');
            var center = new google.maps.LatLng(lat, lnt);
            $("#map_canvas").gmap("option", "center", center);
            $("#map_canvas").gmap("option", "zoom", 16);
        });

        $('#contact_form').attr('action', '<?php echo site_url('vns/sendmail'); ?>');
        $('#contact_form').ajaxForm({
            beforSend: function() {
                $('#contact_sent').attr('dissabled', 'disabled').text('Sending...');
            },
            complete: function(xhr) {
                var json = $.parseJSON(xhr.responseText);
                if (json.status === "success") {
                    $('#contact_sent').text('Sent!!!');
                    alert("Message already sent !!");
                } else {
                    $('#contact_sent').text('Error');
                    alert("Connection Error");
                }
            }
        });
    });
</script>