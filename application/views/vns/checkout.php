<div class="container padding_content_other">
    <div class="row">
        <span class="col-xs-1"></span><h1 class="green col-xs-11 col-xs-offset-1 quark txt_shadow uppercase">ตรวจสอบรายการสั่งซื้อสินค้า&nbsp;<b>Order check</b></h1>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <?php echo image_asset("progress_1.png", NULL, array('style' => 'margin:0px auto;', 'class' => 'img-responsive')) ?>
        </div>
    </div>
    <div class="row margin_top_20">
        <div class="col-xs-12 order_check">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="txt_center" width="45">No.</th>
                        <th class="txt_center">Name</th>
                        <th class="txt_center" width="75">Price</th>
                        <th class="txt_center" width="75">Amount</th>
                        <th class="txt_center" width="90">Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $net_price = 0; ?>
                    <?php foreach ($post as $index => $each_product): ?>
                        <?php $total_price = $each_product['price'] * $each_product['total']; ?>                    
                        <?php $net_price += $total_price; ?>
                        <tr>
                            <td align="center"><?php echo $index + 1 ?></td>
                            <td><?php echo $each_product['name'] ?></td>
                            <td align="right"><?php echo number_format($each_product['price']) ?>฿</td>
                            <td align="center"><?php echo number_format($each_product['total']) ?></td>
                            <td align="right"><?php echo number_format($total_price) ?>฿</td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot id="foot_cart">
                    <tr>
                        <th colspan="4" class="green txt_right">total price</th>
                        <th class="txt_right" id="total_cart_price" style="width:90px;"><?php echo number_format($net_price); ?>฿</th>
                    </tr>
                </tfoot>
            </table>
            <p class="txt_right" style="padding: 10px">
                <a href="<?php echo site_url("vns/process"); ?>" class="btn btn-default">Continue&nbsp;<i class="glyphicon glyphicon-chevron-right"></i></a>
            </p>
        </div>
    </div>
</div>