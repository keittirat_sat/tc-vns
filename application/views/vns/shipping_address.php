<div class="container padding_content_other">
    <div class="row">
        <span class="col-xs-1"></span><h1 class="green col-xs-11 col-xs-offset-1 quark txt_shadow uppercase">รายละเอียดการจัดส่ง&nbsp;<b>Shipping Address</b></h1>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <?php echo image_asset("progress_3.png", NULL, array('style' => 'margin:0px auto;', 'class' => 'img-responsive')) ?>
        </div>
    </div>
    <div class="row margin_top_20">
        <div class="col-xs-7 order_check">
            <h1 class='quark green block_title'><b>Order</b>Information</h1>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="txt_center" width="45">No.</th>
                        <th class="txt_center">Name</th>
                        <th class="txt_center" width="75">Price</th>
                        <th class="txt_center" width="75">Amount</th>
                        <th class="txt_center" width="90">Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $net_price = 0; ?>
                    <?php foreach ($post as $index => $each_product): ?>
                        <?php $total_price = $each_product['price'] * $each_product['total']; ?>                    
                        <?php $net_price += $total_price; ?>
                        <tr>
                            <td align="center"><?php echo $index + 1 ?></td>
                            <td><?php echo $each_product['name'] ?></td>
                            <td align="right"><?php echo number_format($each_product['price']) ?>฿</td>
                            <td align="center"><?php echo number_format($each_product['total']) ?></td>
                            <td align="right"><?php echo number_format($total_price) ?>฿</td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot id="foot_cart">
                    <tr>
                        <th colspan="4" class="green txt_right">total price</th>
                        <th class="txt_right" id="total_cart_price" style="width:90px;"><?php echo number_format($net_price); ?>฿</th>
                    </tr>
                </tfoot>
            </table>
        </div>

        <div class="col-xs-5">
            <h1 class='quark green block_title'><b>Shipping</b>Information</h1>
            <form action="<?php echo site_url("dashboard/order/save"); ?>" method="post" accept-charset="utf-8" role="form"> 
                <label for="user_name">Name</label>
                <div class="form-group">
                    <input type="text" id="user_name" name="user_name" class="form-control" required="required">
                </div>

                <label for="user_lastname">Last name</label>
                <div class="form-group">
                    <input type="text" id="user_lastname" name="user_lastname" class="form-control" required="required">
                </div>

                <label for="user_email">Email</label>
                <div class="form-group">
                    <input type="email" id="user_email" name="user_email" class="form-control" required="required">
                </div>

                <label for="user_telephone">Telephone</label>
                <div class="form-group">
                    <input type="text" id="user_telephone" name="user_telephone" class="form-control" required="required">
                </div>

                <label for="user_address">Address</label>
                <div class="form-group">
                    <input type="text" id="user_address" name="user_address" class="form-control" required="required">
                </div>

                <label for="user_state">State / Province</label>
                <div class="form-group">
                    <input type="text" id="user_state" name="user_state" class="form-control" required="required">
                </div>

                <label for="user_address">Country</label>
                <div class="form-group">
                    <input type="text" id="user_country" name="user_country" class="form-control" value="Thailand" required="required">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i>&nbsp;Checkout</button>
                    <button type="reset" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i>&nbsp;Reset</button>
                </div>

                <input type="hidden" name="order_shipping" value="<?php echo $shipping_method; ?>">
                <?php foreach ($post as $index => $each_product): ?>
                    <?php $i = $index + 1; ?>
                    <input type="hidden" id="<?php echo "product_qty_{$i}"; ?>" name="<?php echo "product_qty_{$i}"; ?>" value="<?php echo $each_product['total']; ?>" />
                    <input type="hidden" id="<?php echo "product_id_{$i}"; ?>" name="<?php echo "product_id_{$i}"; ?>" value="<?php echo $each_product['id']; ?>" />
                <?php endforeach; ?>
                <input type="hidden" name="return_url" value="<?php echo site_url('vns/order_complete'); ?>" />
            </form>
        </div>

    </div>
</div>