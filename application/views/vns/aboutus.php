<div class="container padding_content_other">
    <h1 class="green quark txt_shadow"><b>เกี่ยวกับเรา</b> About us</h1>
    <div class="row">
        <div class="col-xs-4"><?php echo image_asset("aboutus_side.png"); ?></div>
        <div class="col-xs-8" style='text-align: justify;'>
            <?php if ($locale == 'en_US') : ?>
                <p style="text-indent:30px; margin-top: 30px; margin-bottom: 30px;">Chiang Mai Vanussanun Co.,Ltd. Was Originated From A Domestic Business By Mr. Chok-Lee , The Father And Mrs. Lib-Eye Sae-Tang, The Mother. We Primarily Produced Pickled Vegetables And Fruits. Later It Has Been Proceeded By Mr. Chadchan Ekchaipattanakul And Legitimated For A Company With The Capital Investment Of 2,000,000 Baht.</p>
                <p>The Headquarters Is Located At 388 Chiang Mai - Lumpang Road, Tumbon Fah - Ham, Amphur Muang, Chiang Mai. Since Mr. Chadchan Ekchaipattanakul Proceeded The Business, He Has Expanded It To The Sales Of The Local Products From Both Within Chiang Mai And The Adjacent Provinces. These Products Have Become Widely Known. On The Part Of Pickled Vegetable And Fruit Manufacture It Has Been Developed Into Moderness And High Qualities For The Purpose Of Consumers Safety. The Products Under Vanussanun's Brand Name, Therefore, Are Needed By Their Market Places Because Their Sanitary Factors And Over - 20 - Year - Business Are Recommended.</p>
            <?php else: ?>
                <p style="text-indent:30px; margin-top: 30px; margin-bottom: 30px;">บริษัท เชียงใหม่ วนัสนันท์ จำกัด ได้ก่อกำเนิดขึ้นมาจากกิจการภายในครอบครัว โดย คุณพ่อชกลี และ คุณแม่ลิบอ้าย แซ่ตั้ง ในเบื้องต้นผลิตและจำหน่ายสินค้าประเภทผักและผลไม้ ดอง ต่อมาผู้สืบทอดกิจการคือ คุณ ชัดชาญ เอกชัยพัฒนกุล ได้ดำเนินการจดทะเบียนจัดตั้ง บริษัท ด้วยทุนจดทะเบียน 2 ล้านบาท เมื่อวันที่ 7 ธันวาคม 2532</p>
                <p>โดยมีสำนักงาน ตั้งอยู่เลขที่ 398 ถ.เชียงใหม่-ลำปาง ต.ฟ้าฮ่าม อ.เมือง จ.เชียงใหม่ หลังจากที่เข้ามาดำเนินกิจการ คุณชัดชาญ เอกชัยพัฒนกุล ได้ขยายกิจการสู่การจำหน่ายสินค้าของฝากที่เป็นผลิตภัณฑ์ พื้นบ้าน ทั้งของจังหวัด เชียงใหม่ และจังหวัดใกล้เคียงจนมีชื่อเสียงเป็นที่รู้จักโดยทั่วไป ด้านการผลิตและจำหน่ายสินค้าผัก และ ผลไม้ดอง ได้มีการพัฒนาผลิตภัณฑ์ให้มีความทันสมัย และมีคุณภาพโดยคำนึงถึงความปลอดภัยของผู้บริโภคเป็นสำคัญ สินค้าภาย ใต้ชื่อ วนัสนันท์ จึงเป็นที่ต้องการของตลาด เพราะ มีความมั่นใจในความสะอาดถูกอนามัย ในการบริโภค และเป็นที่เชื่อถือมากกว่า 20 ปี</p>
            <?php endif; ?>
        </div>
    </div>

    <h1 class='quark green block_title non_underline margin_bottom_20'><b>Our</b>Partner</h1>
    <div class='row'>
        <?php foreach ($advertisement_list as $ads): ?>
            <div class='col-xs-4'>
                <a href='<?php echo prep_url($ads['URL']); ?>'>
                    <img class='img-responsive' src='<?php echo site_url($ads['file']['path'] . $ads['file']['name']); ?>'>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>