<div class="container padding_content_other">
    <div class="row">
        <span class="col-xs-1"></span><h1 class="green col-xs-11 col-xs-offset-1 quark txt_shadow uppercase">ผลิตภัณฑ์&nbsp;<b>Product</b></h1>
    </div>
    <div class="row">
        <div class="col-xs-3">
            <h1 class='quark green block_title'><b>Category</b></h1>
            <ul class="sidebar_vdo_list margin_top_20">
                <li <?php echo $cate_id == null ? "class='active'" : ""; ?>>
                    <i class="glyphicon glyphicon-chevron-right green"></i>
                    <a class="link_inherite" href="<?php echo current_url(); ?>"><?php echo $locale == "en_US" ? "All Product" : "ทุกหมวดหมู่" ?></a>
                </li>
                <?php foreach ($all_category as $each_category): ?>
                    <li <?php echo $cate_id == $each_category['id'] || $cate_id == $each_category['parent_id'] ? "class='active'" : ""; ?>>
                        <i class="glyphicon glyphicon-chevron-right green"></i>
                        <?php $now_cate = $each_category['parent_id'] != 0 ? $each_category['parent_id'] : $each_category['id']; ?>
                        <a class="link_inherite" href="<?php echo "?cate_id={$now_cate}" ?>"><?php echo $each_category['name']; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="col-xs-9">
            <div class="row" style="padding-top: 45px;">
                <?php if (count($product)): ?>
                    <?php $product_per_rows = set_product_rows($product, 3); ?>
                    <?php foreach ($product_per_rows as $rows): ?>
                        <div class="row">
                            <?php foreach ($rows as $each_product): ?>
                                <div class='col-xs-4 popover_vns' data-toggle="popover" data-placement="top" data-content="<?php echo $each_product["content"]; ?>">
                                    <?php if (count($each_product['file'])): ?>
                                        <?php if (count($each_product['file'][0])): ?>
                                            <?php $path = getimagesize("./" . $each_product['file'][0]['path'] . $each_product['file'][0]['name']); ?>
                                            <?php $img_url = site_url($each_product['file'][0]['path'] . $each_product['file'][0]['name']); ?>
                                            <?php $dim = $path[0] > $path[1] ? "landscape" : "portrait"; ?>
                                        <?php elseif (array_key_exists(1, $each_product['file'])): ?>
                                            <?php $path = getimagesize("./" . $each_product['file'][1]['path'] . $each_product['file'][1]['name']); ?>
                                            <?php $img_url = site_url($each_product['file'][1]['path'] . $each_product['file'][1]['name']); ?>
                                            <?php $dim = $path[0] > $path[1] ? "landscape" : "portrait"; ?>
                                        <?php else: ?>
                                            <?php $dim = "landscape" ?>
                                            <?php $img_url = image_asset_url("nophoto_img.png") ?>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <?php $dim = "landscape" ?>
                                        <?php $img_url = image_asset_url("nophoto_img.png") ?>
                                    <?php endif; ?>
                                    <div class='product_thumb <?php echo $dim; ?>' id="<?php echo "product_{$each_product['id']}" ?>" style='background-image:url("<?php echo $img_url ?>");'>
                                        <div class="mask_product txt_center">
                                            <button class="btn btn-xs btn-default swap_btn" data-enable="0" data-target="<?php echo "product_{$each_product['id']}" ?>" type="button"><i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;Add to cart</button>
                                            <form role="form" method="post" action="<?php echo site_url('vns/add_cart'); ?>" class="add_order">
                                                <label>Quantity</label>
                                                <div class="form-group margin_bottom_10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-shopping-cart"></i></span>
                                                        <select class="form-control" name="total">
                                                            <?php for ($i = 1; $i <= $each_product['metadata']['stock']['value']; $i++): ?>
                                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                            <?php endfor; ?>
                                                        </select>
                                                        <input type="hidden" name="product_id" value="<?php echo $each_product['id']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group txt_center margin_bottom_0">
                                                    <button class="btn btn-xs btn-success add_cart" data-target="<?php echo "product_{$each_product['id']}" ?>" data-loading-text="Adding..." type="submit"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;Add to cart</button>
                                                    <button class="btn btn-xs btn-danger cancel_cart" data-target="<?php echo "product_{$each_product['id']}" ?>" type="button"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="label_product_vns txt_center white">
                                            <p class="margin_bottom_0"><?php echo number_format($each_product['metadata']['price']['value'], 2); ?></p>
                                            <p class="margin_bottom_0">THB</p>
                                        </div>
                                    </div>
                                    <p class="txt_center quark font_bold green font_size_18 margin_bottom_0 cufon"><?php echo $each_product['name']; ?></p>
                                    <p class="txt_center quark txt_italic margin_bottom_0 cufon"><?php echo $each_product['cate_info'][0]['name']; ?></p>
                                    <?php if (array_key_exists('net_weight', $each_product['metadata'])): ?>
                                        <p class='txt_center quark font_size_15'><?php echo __('Net Weight', false) . ": " . $each_product['metadata']['net_weight']['value']; ?></p>
                                    <?php endif; ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="col-xs-12">
                        <h2 class="quark txt_center txt_shadow">
                            <?php echo $locale == "en_US" ? "Not found any product" : "ไม่พบสินค้า" ?>
                        </h2>
                    </div>
                <?php endif; ?>
            </div>

            <div class="row">
                <div class="col-xs-12 txt_center">
                    <ul class="pagination pagination-sm">
                        <?php for ($i = 0; $i < $total_page; $i++): ?>
                            <li <?php echo $i == $page ? "class='active'" : ""; ?>>
                                <a href="<?php echo "?cate_id={$cate_id}&page={$i}"; ?>"><?php echo $i + 1; ?></a>
                            </li>
                        <?php endfor; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>