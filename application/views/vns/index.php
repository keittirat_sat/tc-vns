<!--.padding_content-->
<section class='padding_content'>
    <div class='content_area'>
        <div class="container">
            <div class='row margin_bottom_20' style='margin-top: -230px;'>
                <div class='col-xs-12 relative'>
                    <div class='vns_nav_direction'>
                        <a href='#prev' class='prev_nav'>prev</a>
                        <a href='#next' class='next_nav'>next</a>
                    </div>
                    <div class="nivoSlider" id='slider'>
                        <?php foreach ($slide as $each_slide): ?>
                            <img src='<?php echo site_url($each_slide['file']['path'] . $each_slide['file']['name']); ?>' alt='<?php echo $each_slide['file']['real_name'] ?>'>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>

            <div class='row'>
                <div class="col-xs-6">
                    <h1 class='quark green block_title'><b>News</b></h1>
                    <?php echo $promotion_index; ?>
                </div>
                <div class="col-xs-6">
                    <h1 class='quark green block_title'><b>HowTo</b>Order</h1>
                    <?php if ($locale == "en_US"): ?>
                        <span class='uppercase green'><b>How to order.</b></span>
                        <ol style='padding-left: 30px;'>
                            <li>Ordering page by clicking View &raquo; Add to Cart &raquo; Checkout.</li>
                            <li>Confirm Order &raquo; Shipping Options.</li>
                            <li>Complete name &dash; last name, address, phone number. For delivery profile.</li>
                        </ol>
                        <span class='uppercase green'><b>Or  Order by E-Mail</b></span>
                        <ol style='padding-left: 30px;'>
                            <li>Specific product type, size, quantity that you want.</li>
                            <li>Complete name &dash; last name, address, phone number. For delivery profile.</li>
                            <li>Sent to e-mail  <b>sales.vanusnun@gmail.com</b></li>
                        </ol>
                        <p><i>***After ordering  We will respond to E-mail your order to (non-automated mail). To confirm that the product. And specific orders. And account number for payment.</i></p>
                    <?php else: ?>
                        <span class='uppercase green'><b>วิธีการสั่งซื้อสินค้า</b></span>
                        <ol style='padding-left: 30px;'>
                            <li>สั่งซื้อทางหน้าเว็บ โดยคลิก ดูข้อมูล &raquo; เพิ่มลงตะกร้า &raquo; ชำระเงิน</li>
                            <li>ยืนยันการสั่งซื้อ &raquo; เลือกการจัดส่ง</li>
                            <li>กรอกรายละเอียด ชื่อ &dash; นามสกุล ,ที่อยู่,เบอร์โทร เพื่อการจัดส่งอย่างละเอียด</li>
                        </ol>
                        <span class='uppercase green'><b>หรือ สั่งซื้อทาง E-mail</b></span>
                        <ol style='padding-left: 30px;'>
                            <li>ระบุชนิดสินค้า, ขนาด, จำนวนที่ต้องการ</li>
                            <li>กรอกรายละเอียด ชื่อ &dash; นามสกุล ,ที่อยู่,เบอร์โทร เพื่อการจัดส่งอย่างละเอียด</li>
                            <li>ส่งมาที่  <b>sales.vanusnun@gmail.com</b></li>
                        </ol>
                        <p><i>***หลังจากที่สั่งซื้อแล้ว ทางร้านจะ E-mail ตอบรับการสั่งซื้อไปให้ (ไม่ใช่ mail อัตโนมัติ) เพื่อยืนยันว่ามีสินค้าพร้อมส่ง และแจ้งยอดสั่งซื้อ พร้อมเลขบัญชีสำหรับโอนเงิน</i></p>
                    <?php endif; ?>
                </div>
            </div>

            <h1 class='quark green block_title non_underline margin_bottom_20'><b>Latest</b>Product</h1>
            <?php $product_per_rows = set_product_rows($lates_product, 4); ?>
            <?php foreach ($product_per_rows as $rows): ?>
                <div class='row'>
                    <?php foreach ($rows as $each_product): ?>
                        <div class='col-xs-3 popover_vns' data-toggle="popover" data-placement="top" data-content="<?php echo $each_product["content"]; ?>">
                            <?php if (count($each_product['file'])): ?>
                                <?php if (count($each_product['file'][0])): ?>
                                    <?php $path = getimagesize("./" . $each_product['file'][0]['path'] . $each_product['file'][0]['name']); ?>
                                    <?php $img_url = site_url($each_product['file'][0]['path'] . $each_product['file'][0]['name']); ?>
                                    <?php $dim = $path[0] > $path[1] ? "landscape" : "portrait"; ?>
                                <?php elseif (array_key_exists(1, $each_product['file'])): ?>
                                    <?php $path = getimagesize("./" . $each_product['file'][1]['path'] . $each_product['file'][1]['name']); ?>
                                    <?php $img_url = site_url($each_product['file'][1]['path'] . $each_product['file'][1]['name']); ?>
                                    <?php $dim = $path[0] > $path[1] ? "landscape" : "portrait"; ?>
                                <?php else: ?>
                                    <?php $dim = "landscape" ?>
                                    <?php $img_url = image_asset_url("nophoto_img.png") ?>
                                <?php endif; ?>
                            <?php else: ?>
                                <?php $dim = "landscape" ?>
                                <?php $img_url = image_asset_url("nophoto_img.png") ?>
                            <?php endif; ?>
                            <div class='product_thumb <?php echo $dim; ?>' id="<?php echo "product_{$each_product['id']}" ?>" style='background-image:url("<?php echo $img_url ?>");'>
                                <div class="mask_product txt_center">
                                    <button class="btn btn-xs btn-default swap_btn" data-enable="0" data-target="<?php echo "product_{$each_product['id']}" ?>" type="button"><i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;Add to cart</button>
                                    <form role="form" method="post" action="<?php echo site_url('vns/add_cart'); ?>" class="add_order">
                                        <label>Quantity</label>
                                        <div class="form-group margin_bottom_10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-shopping-cart"></i></span>
                                                <select class="form-control" name="total">
                                                    <?php for ($i = 1; $i <= $each_product['metadata']['stock']['value']; $i++): ?>
                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                    <?php endfor; ?>
                                                </select>
                                                <input type="hidden" name="product_id" value="<?php echo $each_product['id']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group txt_center margin_bottom_0">
                                            <button class="btn btn-xs btn-success add_cart" data-target="<?php echo "product_{$each_product['id']}" ?>" data-loading-text="Adding..." type="submit"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;Add to cart</button>
                                            <button class="btn btn-xs btn-danger cancel_cart" data-target="<?php echo "product_{$each_product['id']}" ?>" type="button"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;Cancel</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="label_product_vns txt_center white">
                                    <p class="margin_bottom_0"><?php echo number_format($each_product['metadata']['price']['value'], 2); ?></p>
                                    <p class="margin_bottom_0">THB</p>
                                </div>
                            </div>
                            <p class="txt_center quark font_bold green font_size_18 margin_bottom_0 cufon"><?php echo $each_product['name']; ?></p>
                            <p class="txt_center quark txt_italic margin_bottom_0 cufon"><?php echo $each_product['cate_info'][0]['name']; ?></p>
                            <?php if (array_key_exists('net_weight', $each_product['metadata'])): ?>
                                <p class='txt_center quark font_size_15'><?php echo __('Net Weight', false) . ": " . $each_product['metadata']['net_weight']['value']; ?></p>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
        </div>

        <div class='green_ribbon txt_center white quark'>
            <a href="<?php echo site_url('vns/product') ?>" class="link_inherite">
                <?php echo image_asset("shoping_icon.png"); ?>
                <h2 class="margin_bottom_10 margin_top_10"><b>View all products</b></h2>
                <h2 class="margin_bottom_0 margin_top_0">&raquo;</h2>
            </a>
        </div>

        <div class="container ">
            <div class="row vdo_section">                
                <?php $vdo = $vdo[0]; ?>
                <?php $str_source = explode('/', $vdo->URL); ?>
                <div class="col-xs-8">
                    <iframe width="640" height="480" src="//www.youtube.com/embed/<?php echo $str_source[3]; ?>" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="col-xs-4">
                    <h1 class="quark txt_center green margin_bottom_0" style="margin-top: 100px;"><b>Feature</b>Video</h1>
                    <h3 class="quark txt_center margin_top_0 margin_bottom_20"><?php echo $vdo->title; ?></h3>
                    <p class="txt_center"><a href="<?php echo site_url('vns/video') ?>"><?php echo image_asset("Video_btn_viewmore.png") ?></a></p>
                </div>
            </div>

            <div class="row news_update">
                <h1 class="txt_center quark white label_news"><b>Up</b>date</h1>
                <?php foreach ($technology_list as $each_techno): ?>
                    <div class="col-xs-3">
                        <?php $path = getimagesize("./" . $each_techno['file'][0]['path'] . $each_techno['file'][0]['name']); ?>
                        <?php $dim = $path[0] > $path[1] ? "landscape" : "portrait"; ?>
                        <div class="product_thumb margin_bottom_10 <?php echo $dim; ?>" style="background-image: url('<?php echo site_url($each_techno['file'][0]['path'] . $each_techno['file'][0]['name']); ?>'); "></div>
                        <p class="txt_center quark font_bold green font_size_18 margin_bottom_10 cufon"><?php echo $each_techno['name']; ?></p>
                        <p><?php echo $each_techno['content']; ?></p>
                    </div>
                <?php endforeach; ?>
            </div>

            <h1 class='quark green block_title non_underline margin_bottom_20'><b>Our</b>Partner</h1>

            <div class='row'>
                <?php foreach ($advertisement_list as $ads): ?>
                    <div class='col-xs-4'>
                        <a href='<?php echo prep_url($ads['URL']); ?>'>
                            <img class='img-responsive' src='<?php echo site_url($ads['file']['path'] . $ads['file']['name']); ?>'>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section><!--/.padding_content-->

<style>
    body{
        background-image: url('<?php echo image_asset_url('bg_main_pallarax.jpg'); ?>');
    }
</style>
<script type='text/javascript'>
    $(function() {
        $('#slider').nivoSlider();
        $('.vns_nav_direction a.prev_nav').click(function() {
            $('.nivo-prevNav').trigger('click');
        });

        $('.vns_nav_direction a.next_nav').click(function() {
            $('.nivo-nextNav').trigger('click');
        });
    });
</script>