<script>
    $(function(){
        $('.cart_sub').remove();
    });
</script>
<div class="checkout_method proc4">
    <span id="_proc1"><?php __('Checkout'); ?></span>
    <span id="_proc2"><?php __('Shipping Method'); ?></span>
    <span id="_proc3"><?php __('Finalizing'); ?></span>
    <span id="_proc4"><?php __('Done'); ?>!</span>
</div>
<div class="assesory_promotion">
    <div class="_title"><?php __('Transaction complete'); ?></div>
</div>

<div class="shipping clearfix" style="text-align: center; font-size: 16px; font-weight: 700;">
    <span><?php __('Thank you, Your Transaction has already saved, please wait our call, we will contact you back.'); ?></span>
</div>