<div style="width: 500px; background-color: #87d237; padding: 30px;">
    <div style="text-align: center;">
        <img src="/<?php echo $technology['file'][0]['path'], $technology['file'][0]['name']; ?>" style="width: 229px; height: 229px; box-shadow: 0 0 5px #888;" />
    </div>
    <div style="text-align: center; margin: 10px 0px; font-size: 22px; color: #fff; text-shadow: 1px 1px #333;"><?php echo $technology['name']; ?></div>
    <div style="width: 391px; margin: 10px auto; font-size: 15px; color: #fff; text-indent: 30px; text-shadow: 1px 1px #333; line-height: 22px;"><?php echo $technology['content'] ?></div>
</div>