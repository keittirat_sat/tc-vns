<script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js" type="text/javascript"></script>
<?php echo js_asset("jquery.ui.map.js"); ?>

<script type="text/javascript">    
    $(function(){
        var new_position = $('#my_map').offset();
        $('body').animate({scrollTop : (new_position.top - 20)},'slow');
        
        $('#map').gmap({ 'center': new google.maps.LatLng(18.807533,99.014636),
            'zoom':16, 'callback':function() {
<?php foreach ($coordinate as $coor): ?>
                    $('#map').gmap('addMarker',{'position':new google.maps.LatLng(<?php echo "{$coor['lat']},{$coor['lon']}" ?>)});    
<?php endforeach; ?>            
            } 
        });
            
        $('a.frans').click(function(){
            var lat = $(this).metadata().lat;
            var lon = $(this).metadata().lon;
            $('#map').gmap({ 'center': new google.maps.LatLng(lat, lon)});
            var new_position = $('#my_map').offset();
            $('body').animate({scrollTop : (new_position.top - 20)},'slow');
            t = $(this).find('.address_other').html();
            $('.vanusnun_address').html(t);
            console.log(t);
            return false;
        });
        
        $('a.pin_on_google_map').click(function(){
            $('a.frans:first').trigger('click');
            return false;
        });
        
        $('a.frans:first').trigger('click');
    });
</script>
<style>
    #cboxLoadedContent {
        border: 5px solid #000;
        background: url('<?php echo image_asset_url('ctf.png'); ?>') repeat-y #000;
    }
    #map{
        width: 922px;
        height: 294px;
        display: block;
        margin: 0px;
    }
</style>
<a name="my_map" id="my_map"></a>
<div class="vanusnun_map">
    <div id="map"></div>
</div>
<div class="vanusnun_map_option">
    <div class="clearfix address_zone">
        <div class="beginning">Address</div>
        <div class="vanusnun_address">
            398 Chiang Mai - Lampang Rd. T.Fah-Hamm Muang, <br/>
            Chiang Mai , Thailand 50000 <br/>
            Tel. 66 +53 240829, 243010 Fax. 66 +53 249518 <br/>
            Email: sales.vanusnun@gmail.com
        </div>
    </div>
    <a class="map_picture" href="#map_picture"></a>
    <?php $coor = $coordinate[0]; ?>
    <a class="pin_on_google_map {<?php echo "lat: {$coor['lat']}, lon: {$coor['lon']}"; ?>}" href="#pin_on_google_map"></a>
</div>
<div class="vanusnun_branch clearfix">
    <?php $i = 0; ?>
    <?php foreach ($coordinate as $branch): ?>
        <?php if ($i % 2 != 0): ?>
            <?php $class = "mod {lat: {$branch['lat']}, lon: {$branch['lon']}}"; ?>
        <?php else: ?>
            <?php $class = "{lat: {$branch['lat']}, lon: {$branch['lon']}}"; ?>
        <?php endif; ?>
        <?php $i++; ?>
        <a class="frans <?php echo $class; ?>" href="<?php echo "#my_map"; ?>">
            <div class="address_title"><?php echo $branch['branch']; ?></div>
            <div class="clearfix">
                <div class="pin_on_google_map"></div>
                <div class="address_other"><?php echo $branch['address']; ?></div>
            </div>
        </a>
    <?php endforeach; ?>
</div>
