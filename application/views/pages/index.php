<script>
    $(function(){        
        $('a.technology').hover(
        function()
        {
            $(this).find('.technology_detail').stop().animate({ height: 'toggle' }, 'fast');
        },
        function()
        {
            $(this).find('.technology_detail').stop().animate({  height: 'toggle' }, 'fast');
        }
    ).colorbox();
        $('.read_more_promotion').colorbox();
    });
</script>

<!--slide promotion-->
<?php echo $slide_promotion; ?>
<!--end slide promotion-->

<div class="clearfix home_section">
    
    <div class="promotion_txt">
        <div class="title_tag"><!-- Promotion Event --></div>
        <div class="home-promotion-top"></div>
        <div class="home-promotion-mid">
            <?php echo $promotion_index; ?>
        </div>
        <div class="home-promotion-bot"></div>
    </div><!-- .promotion_txt -->
    
    <div class="vanusnun_sponsor" style="padding-top: 25px;">
        <?php if (!empty($advertisement_list)): ?>
            <ul>
                <?php foreach ($advertisement_list as $advertisement): ?>
                    <li style="text-align: right;">
                        <?php if (!empty($advertisement['URL'])): ?>
                            <a href="http://<?php echo $advertisement['URL']; ?>">
                            <?php endif; ?>

                            <?php if (!empty($advertisement['file'])): ?>
                                <img src="/<?php echo $advertisement['file']['path'], $advertisement['file']['name']; ?>" style="width: 250px; height: 80px; margin-top: 10px;" />
                            <?php else: ?>
                                <img src="<?php echo image_asset_url('blank.png', 'Dashboard'); ?>" style="width: 250px; height: 80px; margin-top: 10px;" />
                            <?php endif; ?>

                            <?php if (!empty($advertisement['URL'])): ?>
                            </a>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>   
        <?php endif; ?>
    </div>
</div>
<div class="vanusnun_technology clearfix">
    <?php if (!empty($technology_list)): ?>
        <?php foreach ($technology_list as $technology): ?>
            <?php if ($technology['parent_id'] != 0): ?>
                <?php $id = $technology['parent_id']; ?>
            <?php else: ?>
                <?php $id = $technology['id']; ?>
            <?php endif; ?>
            <a class="technology" href="<?php echo site_url('pages/techno') . "?id={$id}"; ?>">
                <?php // var_dump($technology); ?>
                <?php if (!empty($technology['file'][0])): ?>
                    <img src="/<?php echo $technology['file'][0]['path'], $technology['file'][0]['name']; ?>" style="width: 229px; height: 229px;" />
                <?php else: ?>
                    <img src="<?php echo image_asset_url('no_img.gif', 'Dashboard') ?>" style="width: 229px; height: 229px;" />
                <?php endif; ?>
                <div class="technology_detail" style="background-color: rgba(0, 0, 0, 0.55);">
                    <div class="_dimp_center"></div>
                    <ul>
                        <li class="tech_title" style="width:219px; height: 22px; line-height: 22px; padding: 0 5px;"><?php echo $technology['name']; ?></li>
                        <li class="tech_detail" style="width: 219px; height: 19px; line-height: 17px; padding: 0 5px;"><?php echo ellipsize($technology['content'], 30); ?></li>
                    </ul>
                </div>
            </a>
        <?php endforeach; ?>
    <?php endif; ?>
</div>