<?php $product_id = (empty($product['parent_id']) ? $product['id'] : $product['parent_id']); ?>
<?php $metadata = $product['metadata'] ?>

<script type="text/javascript">
    $(function(){
        $("a.each_product_frame").live({
            mouseenter: function(){
                $(this).css({'z-index':'1'}).find('.product_hover').css({'z-index':'2'}).show();
            },
            mouseleave: function(){
                var cur = $(this);
                cur.find('.product_hover').hide(0,function(){
                    $(this).css({'z-index':'-1'});
                    cur.css({'z-index':'0'})
                });
            }
        });
        
        $('a.add_cart_btn').colorbox();
        
        //        $('a.product_image').colorbox({maxWidth:'80%', maxHeight:'80%'});
    });
</script>

<!--filter start here-->
<?php echo $product_filter; ?>
<!--filter end here-->

<div class="showroom_section clearfix">
    <!--    product menu start here-->
    <?php echo $product_category; ?>
    <!--    product menu end here-->

    <div class="product_detail clearfix">
        <img class="product-image" src="<?php echo (empty($product['file'][0]) ? image_asset_url('noimage.jpg') : '/' . $product['file'][0]['path'] . $product['file'][0]['name']); ?>" alt="alt"/>
        <div class="product_info">
            <?php if (!empty($metadata['stock']['value']) && !empty($metadata['price']['value']) && ($metadata['stock']['value'] != 0)): ?>
                <a href="<?php echo site_url('pages/prepare_stock?product_id=' . $product_id); ?>" class="add_cart_btn {product_id: <?php echo $product_id; ?>}"></a>
            <?php endif; ?>
            <ul class="info_section">
                <li class="product_title"><?php echo $product['name']; ?></li>

                <?php if (!empty($metadata['net_weight']['value'])): ?>
                    <li>
                        <?php __('Net Weight'); ?>:
                        <span><?php echo $metadata['net_weight']['value']; ?></span>
                    </li>
                <?php endif; ?>

                <?php if (!empty($metadata['size_of_box']['value'])): ?>
                    <li>
                        <?php __('Size of Box') ?>:
                        <span><?php echo $metadata['size_of_box']['value']; ?></span>
                    </li>
                <?php endif; ?>

                <?php if (!empty($metadata['weight']['value'])): ?> 
                    <li>
                        <?php __('Weight W/ Bottle'); ?>:
                        <span><?php echo $metadata['weight']['value']; ?></span>
                    </li>
                <?php endif; ?>

                <?php if (!empty($metadata['size_of_product']['value'])): ?>
                    <li>
                        <?php __('Size of Product'); ?>: 
                        <span><?php echo $metadata['size_of_product']['value']; ?></span>
                    </li>
                <?php endif; ?>

                <?php if (!empty($metadata['amount_of_product']['value'])): ?>
                    <li>
                        <?php __('Amount of Products/Box'); ?>:
                        <span><?php echo $metadata['amount_of_product']['value']; ?></span>
                    </li>
                <?php endif; ?>

                <?php if (!empty($metadata['price']['value'])): ?>
                    <li>
                        <?php __('Price'); ?>:
                        <span>&#3647;<?php echo number_format($metadata['price']['value']); ?></span>
                    </li>
                <?php endif; ?>

                <?php if (!empty($product['content'])): ?>
                    <li>
                        <?php __('Detail'); ?>:
                        <span><?php echo $product['content']; ?></span>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
        <a href="<?php echo site_url('pages/product'); ?>" class="product_back"></a>
    </div>
</div>