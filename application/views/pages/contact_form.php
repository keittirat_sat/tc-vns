<style>
    .frame_cotact{
        padding: 25px 0px;
        margin: 0px auto;
        width: 262px;
    } 

    li.cft_title{
        font-size: 12px;
        color: #fff;
        text-shadow: 1px 1px 0.5px #000;
        line-height: 12px;
    }

    li.cft_input{
        margin-bottom: 10px;
    }

    li.cft_input input[type=text]{
        width: 250px;
        height: 20px;
        line-height: 25px;
        border: 1px solid #222;
        padding: 5px;
        margin: 5px 0px 0px;
    }

    li.cft_input textarea{
        width: 250px;
        padding: 5px;
        margin: 0px;
        height: 120px;
        border: 1px solid #222;
        margin: 5px 0px 0px;
        resize: none;
    }

    li.cft_input input[type=button]{
        padding: 5px 10px;
        margin: 0px 0px 0px 15px;
        border: 1px solid #ccc;
        background-color: #efefef;
        color: #222;
        text-shadow: 1px 1px 0.5px #ccc;
        border-radius: 3px;
        cursor: pointer;
    }
    li.cft_input input[type=button]:hover{
        background-color: #cacaca;
    }
</style>
<script>
    $(function(){
        $('#_cancel').live({
            click: function(){
                $.colorbox.close();
            }
        });     
        
        $('#_send').live({
            click: function(){
                _name = $('#_name');
                _email = $('#_email');
                _msg = $('#_msg');
                
                if(!_name.val()){
                    alert("Please input contact's name");
                    return;
                }else if(!_msg.val()){                    
                    alert("Message cannot leave blank");
                    return;
                }else if(!_email.val()){                    
                    alert("Please fill your email");
                    return;
                }else{
                    str = _email.val();
                    addSign = false;
                    fullStop = false;
                    for(i = 0; i < str.length; i++){
                        temp = str[i];
                        if(addSign){
                            if(temp == '.'){
                                fullStop = true;
                            }else if(temp == '@'){       
                                alert("Email is not corect");
                                return;
                            }
                        }else{
                            if(temp == '@'){
                                addSign = true;
                            }
                        }
                    }
                    
                    if(addSign && fullStop){
                        $.post('<?php echo site_url('pages/sendmail'); ?>',{from: _name.val(), email: _email.val(), msg: _msg.val()},function(res){
                            if($.trim(res) == '1'){
                                $.colorbox.close();
                            }else{
                                console.log(res);
                                alert('Sorry, System on maintanace');
                            }
                        });
                    }else{
                        alert("Email is not corect");
                    }
                }
            }
        });
    });
</script>
<div class="frame_cotact">
    <ul>
        <li class="cft_title">Name</li>
        <li class="cft_input">
            <input type="text" id="_name">
        </li>
        <li class="cft_title">Email</li>
        <li class="cft_input">
            <input type="text" id="_email">
        </li>
        <li class="cft_title">Message</li>
        <li class="cft_input">
            <textarea id="_msg"></textarea>
        </li>
        <li class="cft_input" style="text-align: right;">
            <input type="button" value="Send" id="_send">
            <input type="button" value="Cancel" id="_cancel">
        </li>
    </ul>
</div>