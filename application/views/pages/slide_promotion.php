<style>
    .vanusnun_language_facebook {
        margin-bottom: 0px;
    }
</style>

<?php $slide = $this->dashboard->get_slide(); ?>

<div class="slide_news_dim">
    <div class="group_dim">  
        <?php if (!empty($slide)): ?>      
            <?php foreach ($slide as $index => $value): ?>
                <div class="slide-pin dim <?php if (!$index) : ?> selected <?php endif; ?>"></div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>

<div class="cc_slide">    
    <div id="slide-left"></div>
    <div id="slide" class="slide_news">
        <ul>
            <?php if (!empty($slide)): ?>
                <?php foreach ($slide as $index => $value): ?>
                    <li class="slide" style="background-image: url('/<?php echo $value['file']['path'], $value['file']['name']; ?>'); background-size: 918px 313px;">
                        <?php if (!empty($value['content'])): ?>
                            <div class="slide_info <?php if ($index > 0) : ?> hide <?php endif; ?>">
                                <?php echo $value['content']; ?>
                            </div>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
    <div id="slide-right"></div>
</div>

<script type="text/javascript">
            
    var max = $('.slide').length;
    var slideTimer = null;    
    var index = 0;
    
    function slideRight(current, next)
    {         
        if($(current).children('.slide_info').length > 0)
        {
            /* Animate slide info */
            var slideInfo = $(current).children('.slide_info');
                    
            $(slideInfo).stop().animate({ height: 'toggle' }, 'slow', function()
            {
                /* Move to next slide */
                $('#slide ul').stop().animate({ marginLeft: $(next).index() * 918 * -1 }, 'slow', function()
                {
                    if($(next).children('.slide_info').length > 0)
                    {
                        var childInfo = $(next).children('.slide_info');
                        
                        $(childInfo).hide().stop().animate({ height: 'toggle' }, 'slow');
                    }
        
                    /* Set slide pin selected */
                    $('.slide-pin').removeClass('selected');
                    $('.slide-pin').eq($(next).index()).addClass('selected');
                });
            });
        }
        else
        {
            /* Move to next slide */
            $('#slide ul').stop().animate({ marginLeft: $(next).index() * 918 * -1 }, 'slow', function()
            {
                if($(next).children('.slide_info').length > 0)
                {
                    var childInfo = $(next).children('.slide_info');
            
                    $(childInfo).hide().stop().animate({ height: 'toggle' }, 'slow');
                }
        
                /* Set slide pin selected */
                $('.slide-pin').removeClass('selected');
                $('.slide-pin').eq($(next).index()).addClass('selected');
            });
        }
    }
    
    $(document).ready(function()
    {      
        //        $('#s0').css({left: '0px'});
        //        $('#n0').addClass('selected');
        //        var index = 0;
        //        var limit = $('.slide').length;
        //        
        //        var slideTimer = window.setInterval(function(){
        //            var cur = $('#s'+ index);
        //            $('#n'+index).removeClass('selected');
        //            index = (index + 1) % limit;
        //            next = $('#s'+index);
        //            $('#n'+index).addClass('selected');
        //            
        //            next.css({left: '920px'});
        //            cur.animate({left: '-920px'},'slow');
        //            next.animate({left: '0px'},'slow');
        //        },5000);
        
        /* Initial slide frame (ul) */
        $('#slide ul').width(max * 918);
        
        if($('.slide').length > 1)
        {        
            slideTimer = window.setInterval(function()
            {
                /* First initial index zero */
                var current = $('.slide').eq(index);
                var next = null;
                
                index = (index + 1 < max ? index + 1 : 0);
                    
                next = $('.slide').eq(index);
                slideRight(current, next);
                
            }, 5000);
        }
        
        /* Binding slide navigation */
        $('#slide-left').click(function()
        {
            window.clearInterval(slideTimer);
            
            var current = $('.slide').eq(index);
            var previous = null;
                
            index = (index - 1 < 0 ? max - 1 : index - 1);
                    
            previous = $('.slide').eq(index);
            slideRight(current, previous);            
        });
        
        $('#slide-right').click(function()
        {
            window.clearInterval(slideTimer);
            
            var current = $('.slide').eq(index);
            var next = null;
                
            index = (index + 1 < max ? index + 1 : 0);
                    
            next = $('.slide').eq(index);
            slideRight(current, next);
        });
    });
</script>