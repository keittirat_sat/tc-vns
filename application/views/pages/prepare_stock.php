<?php if ($product['parent_id'] != 0): ?>
    <?php $product_id = $product['parent_id']; ?>
<?php else: ?>
    <?php $product_id = $product['id']; ?>
<?php endif; ?>
<?php $metadata = $product['metadata']; ?>
<style type="text/css">
    .update_cart_live{
        text-align: center;
        font-size: 13px;
        line-height: 25px;
        padding: 25px;
    }
    /*    #item_amount{
            width: 225px;
            height: 20px;
            border: 1px solid #000;
            padding: 0px 0px 0px 5px;
        }*/

    input[type="button"] {
        padding: 5px 10px;
    }

    input[type="button"]:hover {
        cursor: pointer;
    }
</style>

<script type="text/javascript">
    $(function(){
        $('#live_cancel').live({
            click: function(){
                $.colorbox.close();
            }
        });
        
        $('#live_cart').live({
            click: function(){
                $.post("/dashboard/order/buy/<?php echo $product_id; ?>/"+$('#item_amount').val(), function(res) {
                    if(res.status == 'success'){
                        $.post('/pages/add_cart',{product_id: <?php echo $product_id; ?>, total: $('#item_amount').val()},function(crt){
                            if($.trim(crt) == '1'){
                                $.get('/pages/get_cart',function(cart){
                                    $('.cart_display').html(cart);
                                    $('.cart_sub a.logo').trigger('click');
                                    $.colorbox.close();
                                });
                            }else{
                                alert('Connection lost ...');
                            }
                        });
                    }else{
                        alert(res.message);
                    }
                }, 'json');
            
            }
        });
    });
</script>
<div class="update_cart_live">        
    <p style="font-weight: 700; font-size: 15px;">        
        <?php printf(__('Add amount of "%s" to cart.', FALSE), $product['name']); ?>
    </p>

    <div style="margin-top: 15px;">
        <?php __('Amount'); ?>
        <?php if ($metadata['stock']['value'] != -1): ?>
            <select id="item_amount" name="item_amount" style="margin-left: 10px; border: 1px solid #ccc; padding: 3px 5px; width: 225px;">
                <?php $cart = $this->session->userdata('cart'); ?>
                <?php $previous = (!empty($cart[$product_id]) ? $cart[$product_id]['total'] : 0); ?>

                <?php for ($i = 1; $i <= $metadata['stock']['value']; $i++): ?>
                    <option value="<?php echo $previous + $i; ?>"><?php echo $i; ?></option>
                <?php endfor; ?>
            </select>
        <?php else: ?>
            <input type="text" value="1"  id="item_amount" name="item_amount" />
        <?php endif; ?>
    </div>

    <div style="margin-top: 15px;">
        <input type="button" value="<?php __('Add to cart'); ?>" id="live_cart" />
        <input type="button" value="<?php __('Cancel'); ?>" id="live_cancel" />
    </div>
</div>