<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script>
    function addMarker(location) {
        var text = "<?php echo $address; ?>";
        var tt = '<?php echo image_asset_url('vanasnunfav.png'); ?>';

        infowindow = new google.maps.InfoWindow({
            content: text
        })	

        marker = new google.maps.Marker({
            position: location,
            map: map,
            icon: tt,
            //title: '15',
            //id: 15,
            html:  text

        });

        infowindow.open(map,marker);
        google.maps.event.addListener(marker, 'click', function() {

            infowindow.setContent(this.html);
            infowindow.open(map,this);
            map.setCenter(this.getPosition());
            //alert("click : ");
        });

        //markersArray.push(marker);
    }    

    $(function(){
        var coordinate = new google.maps.LatLng('<?php echo $lat; ?>','<?php echo $lon; ?>');

        var mapOptions = {
            zoom : 14,
            center : coordinate,
            mapTypeId : google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        addMarker(coordinate);
    });
</script>
<style>
    #map{
        width: 922px;
        height: 294px;
        display: block;
        margin: 0px;
    }
</style>
<div id="map"></div>