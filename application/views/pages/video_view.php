<div style="float: left;">
	<iframe id="youtube-preview" width="450" height="253" src="http://www.youtube.com/embed/<?php echo preg_replace('/http:\/\/youtu.be\//', '', $video['URL']); ?>" frameborder="0" allowfullscreen></iframe>
</div>
<div style="float: left; margin-left: 20px;">
	<p style="font-size: 18px; font-weight: bolder;">
		<?php echo (empty($video['title']) ? 'Untitled Video' : $video['title']); ?>
	</p>
	<hr style="border-top: 1px dashed #444; display: block; width: 430px; margin-top: 9px;">
	<div style="width: 430px; margin-top: 20px;">
		<?php echo nl2br($video['desc']); ?>
	</div>
</div>