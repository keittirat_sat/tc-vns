<?php foreach ($all_product as $product): ?>

    <?php $product_id = ($product['parent_id'] != 0 ? $product['parent_id'] : $product['id']); ?>
    <?php $image = (empty($product['file'][0]) ? image_asset_url('noimage.jpg') : '/' . $product['file'][0]['path'] . $product['file'][0]['name']); ?>

    <a class="each_product_frame" href="<?php echo site_url("detail/" . $product_id . "/" . slug($product['name'])); ?>" style="background-image: url('<?php echo $image; ?>'); background-size: auto 212px;">
        <?php if (empty($product['metadata']['stock']['value']) || $product['metadata']['stock']['value'] == 0): ?>
            <div class="out_of_stock"></div>
        <?php endif; ?>
        <div class="detail clearfix">
            <div class="name_title">
                <?php echo $product['name']; ?>
            </div>
            <div class="price_tag">
                <?php if (empty($product['metadata']['price']['value'])): ?>
                    <span>Call</span>
                <?php else: ?>
                    <?php echo "&#3647;" . number_format($product['metadata']['price']['value']); ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="product_hover">
            <ul>
                <li><b><?php echo $product['name']; ?></b></li> 

                <?php if (!empty($product['metadata']['net_weight']['value'])): ?>
                    <li>
                        <?php __('Net Weight') ?>:
                        <?php echo $product['metadata']['net_weight']['value']; ?>
                    </li> 
                <?php endif; ?>

                <?php if (!empty($product['metadata']['amount_of_product']['value'])): ?>
                    <li>
                        <?php __('Amount of Products/Box'); ?>
                        <?php echo $product['metadata']['amount_of_product']['value']; ?>
                    </li> 
                <?php endif; ?>
            </ul>
            <div class="cover_btn_vfd">
                <div class="view_full_detail"></div>
            </div>
        </div>
    </a>

<?php endforeach; ?>