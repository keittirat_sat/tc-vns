<script>
    var shipping_method = 0;
    $(function(){
        $('.shipping_method').click(function(){
            temp = $(this);
            $('.shipping_method').removeClass('selected');
            $.post('<?php echo site_url('pages/set_shipping_method'); ?>',{token: '<?php echo md5(date('d:Y:m')); ?>', method: temp.metadata().method},function(res){
                if($.trim(res) == temp.metadata().method){
                    temp.addClass('selected');
                    shipping_method = parseInt(temp.metadata().method);
                    $('.confirm_checkout').attr('href','<?php echo site_url('pages/shipping_address'); ?>');
                }else{
                    shipping_method = 0;
                    $('.confirm_checkout').attr('href','#finalize');
                }
            });
        });
        
        $('.confirm_checkout').click(function(){
            if(shipping_method == 0){
                alert('Please select shipping method');
            }
        });
        
        $('.shipping_method.selected').trigger('click');
        
        $('.cart_sub').remove();
    });
</script>
<div class="checkout_method proc2">
    <a id="_proc1" href="<?php echo site_url('pages/checkout'); ?>"><?php __('Checkout'); ?></a>
    <a id="_proc2" href="<?php echo site_url('pages/confirm_checkout'); ?>"><?php __('Shipping Method'); ?></a>
    <span id="_proc3"><?php __('Finalizing'); ?></span>
    <span id="_proc4"><?php __('Done'); ?>!</span>
</div>
<div class="assesory_promotion">
    <div class="_title"><?php __('Shipping Method'); ?></div>    
</div>
<?php $session_cart = $this->session->userdata('shipping_method'); ?>
<?php foreach ($shipping_method as $temp): ?>
    <?php if (!empty($session_cart)): ?>
        <?php if ($session_cart == $temp['value']): ?>
            <?php $class = "selected"; ?>
        <?php else: ?>
            <?php $class = ""; ?>
        <?php endif; ?>
    <?php else: ?>
        <?php $class = ""; ?>
    <?php endif; ?>
    <div class="shipping_method <?php echo $class; ?> {method: <?php echo $temp['value']; ?>}">   
        <div class="shipping_title"><?php __($temp['method']); ?></div>
        <div class="shipping_detail"><?php echo $temp['description']; ?></div>
        <div class="logo_shipping"></div>
    </div>
<?php endforeach; ?>
<a class="confirm_checkout" href="#finalize" style="margin: 0px 40px 0px 0px;"><?php __('Next Step'); ?></a>