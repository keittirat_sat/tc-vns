<script type="text/javascript">
    $(function(){
        $('.cart_sub').remove();
    });
</script>

<style type="text/css" media="screen">
    th {
        font-weight: 700;
        font-size: 15px;
    }
</style>

<?php $cart = $this->session->userdata('cart'); ?>

<div class="checkout_method proc1">
    <a id="_proc1" href="<?php echo site_url('pages/checkout'); ?>"><?php __('Checkout'); ?></a>
    <span id="_proc2"><?php __('Shipping Method'); ?></span>
    <span id="_proc3"><?php __('Finalizing'); ?></span>
    <span id="_proc4"><?php __('Done'); ?>!</span>
</div>
<div class="assesory_promotion">
    <div class="_title"><?php __('Confirm Order'); ?></div>
</div>
<table class="checkout_table">
    <tr>
        <th width="50px"><?php __('No.'); ?></th>
        <th width="462px"><?php __('Product name'); ?></th>
        <th width="100px"><?php __('Price for each'); ?></th>
        <th width="100px"><?php __('Amount'); ?></th>
        <th width="200px"><?php __('Price'); ?></th>
    </tr>
    <?php $i = 1; ?>
    <?php $price = 0; ?>
    <?php foreach ($cart as $item): ?>
        <?php $product = $this->dashboard->get_product($item['product_id']); ?>
        <?php $product = $product[0]; ?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $product['name']; ?></td>
            <td><?php if (empty($product['metadata']['price']['value'])) echo " N/A "; else echo "&#3647;" . number_format($product['metadata']['price']['value']); ?></td>
            <td><?php echo $item['total']; ?></td>
            <td><?php if (empty($product['metadata']['price']['value'])) echo "&#3647;0"; else echo "&#3647;" . number_format($product['metadata']['price']['value'] * $item['total']); ?></td>
        </tr>
        <?php if (empty($product['metadata']['price']['value'])) $price += 0; else $price += $product['metadata']['price']['value'] * $item['total']; ?>
        <?php $i++; ?>
    <?php endforeach; ?>

    <tr class="checkout_total">
        <td colspan="4"><b><?php __('Total'); ?></b></td>
        <td style="border: 1px solid white;"><?php echo "&#3647;" . number_format($price); ?></td>
    </tr>
</table>
<a class="confirm_checkout" href="<?php echo site_url('pages/confirm_checkout'); ?>"><?php __('Confirm'); ?></a>