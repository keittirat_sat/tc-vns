<script type="text/javascript">
    $(function(){       
        var cate_id = -1;
        $('.category').click(function(){
            if($(this).metadata().cate_id != cate_id){
                cate_id = $(this).metadata().cate_id;
                $('.product_categorize').hide();
                $('.product_categorize.cate_'+cate_id).show();
            }
        });
    });
</script>

<div class="product_category">
    <?php $m = 0; ?>
    <?php foreach ($all_category as $cate): ?>
        <?php $cate_id = ($cate['parent_id'] != 0 ? $cate['parent_id'] : $cate['id']); ?>
        <?php if ($cate_id != -1): ?>
            <div class="category <?php echo (($m == 0) ? 'first' : $m); ?> { cate_id: <?php echo $cate_id; ?> }">
                <?php echo $cate['name']; ?>
            </div>

            <?php $product_by_cate = $this->dashboard->get_product(NULL, $cate_id); ?>
            <?php $j = 0; ?>

            <?php foreach ($product_by_cate as $product): ?>

                <?php $product_id = ($product['parent_id'] != 0 ? $product['parent_id'] : $product['id']); ?>
                <?php $class = ($j % 2 ? NULL : 'light'); ?>

                <div class="product_categorize <?php echo "{$class} cate_{$cate_id}"; ?>">
                    <a  href="<?php echo site_url("detail/{$product_id}/" . slug($product['name'])); ?>">
                        <?php echo $product['name']; ?>
                    </a>
                </div>

                <?php $j++; ?>
            <?php endforeach; ?>
            <?php $m++; ?>
        <?php endif; ?>
    <?php endforeach; ?>
</div>