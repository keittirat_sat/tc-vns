<script>
    $(function(){
        $("a.each_product_frame").live({
            mouseenter: function(){
                $(this).css({'z-index':'1'}).find('.product_hover').css({'z-index':'2'}).show();
            },
            mouseleave: function(){
                cur = $(this);
                cur.find('.product_hover').hide(0,function(){
                    $(this).css({'z-index':'-1'});
                    cur.css({'z-index':'0'})
                });
            }
        });
        
        $('.lastest_product').click(function(){
            $.get('/pages/product_each_cate',function(html){
                $('.product_all').html(html);
            });
        });
        
        $.get('/pages/product_each_cate',{cate_id: $('.category.first').metadata().cate_id},function(html){
            $('.product_all').html(html);
        });
        
        $('.category').live({
            click: function(){
                $.get('/pages/product_each_cate',{cate_id: $(this).metadata().cate_id},function(html){
                    $('.product_all').html(html);
                });
            }
        });
    });
</script>
<!--filter start here-->
<?php echo $product_filter; ?>
<!--filter end here-->

<div class="showroom_section clearfix">
    
    <!--    product menu start here-->
    <?php echo $product_category; ?>
    <!--    product menu end here-->

    <div class="product_all clearfix"></div>
</div>