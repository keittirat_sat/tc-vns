<?php $cart = $this->session->userdata('cart'); ?>

<?php if (!empty($cart)): ?>

    <table>
        <tr id="cart-title">
            <th width="32px" class="first"><?php __('No.'); ?></th>
            <th width="91px"><?php __('Product'); ?></th>
            <th width="48px"><?php __('Price') ?></th>
            <th width="48px" class="last"><?php __('Amount') ?></th>
        </tr>

        <?php $i = 1; ?>
        <?php $price = 0; ?>

        <?php foreach ($cart as $item): ?>

            <?php $product = $this->dashboard->get_product($item['product_id']); ?>   
            <?php $product = $product[0]; ?>

            <tr class="item_in_cart {product_id: <?php echo $item['product_id']; ?>}">
                <td><?php echo $i; ?></td>
                <td><?php echo ellipsize($product['name'], 60); ?></td>
                <td><?php echo "&#3647;" . number_format($product['metadata']['price']['value']); ?></td>
                <td><?php echo $item['total']; ?></td>
            </tr>

            <?php $i++; ?>
            <?php if (empty($product['metadata']['price']['value'])): ?>
                <?php $price += 0; ?>
            <?php else: ?>
                <?php $price += $product['metadata']['price']['value'] * $item['total']; ?>
            <?php endif; ?>
            
        <?php endforeach; ?>

    </table>

    <a class="checkout" href="<?php echo site_url('pages/checkout'); ?>"></a>

    <div class="sub_total">
        <span>&#3647;<?php echo number_format($price); ?></span>
    </div>
    
<?php endif; ?>