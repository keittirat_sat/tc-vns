<?php if(!empty($video_data)): ?>
	<?php foreach($video_data as $data): ?>

		<span style="margin-top: 15px; margin-right: 5px; display: inline-block;">
			<p style="margin-bottom: 5px;">
				<a href="<?php echo site_url('pages/video/view/' . $data['id']); ?>" style="font-weight: bolder; font-size: 16px;">
					<?php if(empty($data['title'])): ?>
						Untitled Video
					<?php else: ?>
						<?php echo $data['title']; ?>
					<?php endif; ?>
				</a>
			</p>
			<iframe id="youtube-preview" width="450" height="253" src="http://www.youtube.com/embed/<?php echo preg_replace('/http:\/\/youtu.be\//', '', $data['URL']); ?>" frameborder="0" allowfullscreen></iframe>
		</span>

	<?php endforeach; ?>
<?php endif; ?>

<?php if(!empty($pagination)): ?>
	<style type="text/css">
		#my-pagination {
			margin-top: 25px;
			display: block;
		}

		#my-pagination li a {
			display: inline-block;
			border: 1px solid #222;
			padding: 2px 5px;
			float: left;
			margin-right: 3px;
		}
	</style>

	<ul id="my-pagination"> <?php echo $pagination; ?> </ul>
<?php endif; ?>