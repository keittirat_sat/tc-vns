<script>
    $(function(){
        $('a.list_recipe').colorbox();
    });
</script>
<div class="ingrediet">
    <?php $i = 0; ?>
    <?php foreach ($all_ingredient as $ingredient): ?>
        <?php if ($i % 2): ?>
            <div class="each_ing clearfix">
                <div class="image_ingredient" style="background-image: url('<?php echo image_asset_url($ingredient['image']); ?>');"></div>
                <div class="detail_of_ingredient" style="margin-left: 20px;">
                    <ul>
                        <li class="ingredient_name"><?php echo $ingredient['name']; ?></li>
                        <li class="ingredient_detail"><?php echo $ingredient['detail']; ?></li>
                    </ul>
                </div>
            </div>
        <?php else: ?>
            <div class="each_ing clearfix">
                <div class="detail_of_ingredient" style="margin-right: 20px;">
                    <ul>
                        <li class="ingredient_name"><?php echo $ingredient['name']; ?></li>
                        <li class="ingredient_detail"><?php echo $ingredient['detail']; ?></li>
                    </ul>
                </div>
                <div class="image_ingredient" style="background-image: url('<?php echo image_asset_url($ingredient['image']); ?>');"></div>
            </div>
        <?php endif; ?>
        <?php $i++; ?>
    <?php endforeach; ?>
</div>

<div class="assesory_promotion">
    <div class="_title">Recipes</div>
</div>
<div class="recipes clearfix">
    <?php foreach ($all_recipe as $recipe): ?>
    <?php if(empty($recipe['parent_id'])): ?>
        <?php $recipe_id = $recipe['id']; ?>
    <?php else: ?>
        <?php $recipe_id = $recipe['parent_id']; ?>
    <?php endif; ?>
        <a class="list_recipe" style="background-image: url('<?php echo "/" . $recipe['file'][0]['path'] . $recipe['file'][0]['name']; ?>')" href="<?php echo site_url('recipe/' . $recipe_id . '/' . slug($recipe['name'])); ?>">
            <span><?php echo $recipe['name']; ?></span>
        </a>
    <?php endforeach; ?>
</div>