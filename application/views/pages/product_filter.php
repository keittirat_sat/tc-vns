<script type="text/javascript">
$(function(){
    $('#category_filter').change(function(){
        $.get('/pages/get_product_by_cate',{cate_id: $(this).val()},function(product){
            $('#product_filter').html(product);
        });
    });
    
    $('#product_filter').change(function(){
        var url = "<?php echo site_url('detail'); ?>";
        window.location.href = url+"/"+$(this).val()+"/filter";
    });
});
</script>
<div class="product_filter">
    <a href="<?php echo site_url('pages/promotion'); ?>" class="promotion_btn"></a>
    <a style="cursor: pointer;" class="lastest_product"></a>
    <table>
        <tr>
            <td style="padding-bottom: 3px;"><span><?php __('SELECT OUR PRODUCT BY CATEGORIES'); ?>:</span></td>
            <td>
                <select id="category_filter">
                    <option value="0">-- <?php __('Category') ?> --</option>
                    <?php $cate = $this->dashboard->get_category(); ?>
                    <?php foreach ($cate as $category): ?>
                        <?php if ($category['parent_id'] != 0): ?>
                            <?php $category_id = $category['parent_id']; ?>
                        <?php else: ?>
                            <?php $category_id = $category['id']; ?>
                        <?php endif; ?>
                        <option value="<?php echo $category_id; ?>"><?php echo $category['name']; ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td><span><?php __('SELECT OUR PRODUCT ITEMS'); ?>:</span></td>
            <td>                
                <select id="product_filter">
                    <option value="0">-- <?php __('Product') ?> --</option>
                    <?php $product = $this->dashboard->get_product(); ?>
                    <?php foreach ($product as $item): ?>
                        <?php if ($item['parent_id'] != 0): ?>
                            <?php $product_id = $item['parent_id']; ?>
                        <?php else: ?>
                            <?php $product_id = $item['id']; ?>
                        <?php endif; ?>
                    <option value="<?php echo $product_id; ?>"><?php echo$item['name']; ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
    </table>
</div>