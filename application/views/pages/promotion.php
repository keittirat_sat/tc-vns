<script>
    $(function(){
        $("a.each_product_frame").live({
            mouseenter: function(){
                $(this).css({'z-index':'1'}).find('.product_hover').css({'z-index':'2'}).show();
            },
            mouseleave: function(){
                cur = $(this);
                cur.find('.product_hover').hide(0,function(){
                    $(this).css({'z-index':'-1'});
                    cur.css({'z-index':'0'})
                });
            }
        });
    });
</script>
<!--slide promotion-->
<?php echo $slide_promotion; ?>
<!--end slide promotion-->
<div class="showroom_section clearfix">
    <div class="product_all clearfix" style="width: 100%;">
        <?php foreach ($all_product as $product): ?>
            <?php
            if (empty($product['file'][0]['id'])) {
                $image = image_asset_url("noimage.jpg");
            } else {
                $img = $product['file'][0];
                $image = "/" . $img['path'] . $img['name'];
                if (!is_file($this->input->server('DOCUMENT_ROOT') . $image)) {
                    $image = image_asset_url("noimage.jpg");
                }
            }

            if ($product['parent_id'] != 0)
                $product_id = $product['parent_id'];
            else
                $product_id = $product['id'];
            ?>

            <a class="each_product_frame" href="<?php echo site_url("detail/" . $product_id . "/" . slug($product['name'])); ?>" style="background-image: url('<?php echo $image; ?>'); background-size: auto 212px;" target="_blank">
                <?php if (empty($product['metadata']['stock']['value']) || $product['metadata']['stock']['value'] == 0): ?>
                    <div class="out_of_stock"></div>
                <?php endif; ?>
                <div class="detail clearfix">
                    <div class="name_title">
                        <?php echo $product['name']; ?>
                    </div>
                    <div class="price_tag">
                        <?php if (empty($product['metadata']['price']['value'])): ?>
                            <span>Call</span>
                        <?php else: ?>
                            <?php echo "&#3647;" . number_format($product['metadata']['price']['value']); ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="product_hover">
                    <ul>
                        <li><b><?php echo $product['name']; ?></b></li> 
                        <li>Net Weight : <?php if (!empty($product['metadata']['net_weight']['value'])) echo $product['metadata']['net_weight']['value']; else echo "Not defined"; ?> G.</li> 
                        <li>Amount Of Products/Box : <?php if (!empty($product['metadata']['amount_of_product']['value'])) echo $product['metadata']['amount_of_product']['value']; else echo "Not defined"; ?></li> 
                    </ul>
                    <div class="cover_btn_vfd">
                        <div class="view_full_detail"></div>
                    </div>
                </div>
            </a>
        <?php endforeach; ?>
    </div>
</div>