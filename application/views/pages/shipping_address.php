<script type="text/javascript">
    $(function(){
        $('.cart_sub').remove();
        $('input[type=text]').focus(function(){
            $(this).css({'border':'1px solid #000'});
        });
        $('a.confirm_checkout').click(function(){
            var flag = true;

            if(!$('#user_name').val()){
                flag = false;
                $('#user_name').css({'border':'1px solid #ff0000'});
            }

            if(!$('#user_lastname').val()){
                flag = false;
                $('#user_lastname').css({'border':'1px solid #ff0000'});
            }

            if(!$('#user_email').val()){
                flag = false;
                $('#user_email').css({'border':'1px solid #ff0000'});
            }

            if(!$('#user_telephone').val()){
                flag = false;
                $('#user_telephone').css({'border':'1px solid #ff0000'});
            }

            if(!$('#user_address').val()){
                flag = false;
                $('#user_address').css({'border':'1px solid #ff0000'});
            }

            if(!$('#user_state').val()){
                flag = false;
                $('#user_state').css({'border':'1px solid #ff0000'});
            }

            if(!$('#user_country').val()){
                flag = false;
                $('#user_country').css({'border':'1px solid #ff0000'});
            }

            if(flag){
                $('.shipping form').submit();
            }
        });
    });
</script>

<style type="text/css" media="screen">
    input[type="text"] {
        border: 1px solid #ccc;
        padding: 5px 10px;
    }

    th {
        font-weight: 700;
        font-size: 15px;
    }

    ._input {
        margin-top: 3px;
    }

    ._title {
        padding-left: 1px;
    }
</style>
<div class="checkout_method proc3">
    <a id="_proc1" href="<?php echo site_url('pages/checkout'); ?>"><?php __('Checkout'); ?></a>
    <a id="_proc2" href="<?php echo site_url('pages/confirm_checkout'); ?>"><?php __('Shipping Method'); ?></a>
    <a id="_proc3" href="<?php echo site_url('pages/shipping_address'); ?>"><?php __('Finalizing'); ?></a>
    <span id="_proc4"><?php __('Done'); ?>!</span>
</div>
<div class="assesory_promotion">
    <div class="_title"><?php __('Shipping Address'); ?></div>
</div>

<div class="shipping clearfix">
    <?php echo form_open('/dashboard/order/save'); ?>
    <div style="float: left;">
        <ul>
            <li class="_title"><?php __('Name'); ?></li>
            <li class="_input"><input type="text" id="user_name" name="user_name" /></li>

            <li class="_title"><?php __('Last name'); ?></li>
            <li class="_input"><input type="text" id="user_lastname" name="user_lastname" /></li>

            <li class="_title"><?php __('Email'); ?></li>
            <li class="_input"><input type="text" id="user_email" name="user_email" /></li>

            <li class="_title"><?php __('Telephone'); ?></li>
            <li class="_input"><input type="text" id="user_telephone" name="user_telephone" /></li>

            <li class="_title"><?php __('Address'); ?></li>
            <li class="_input"><input type="text" id="user_address" name="user_address" /></li>

            <li class="_title"><?php __('State / Province'); ?></li>
            <li class="_input"><input type="text" id="user_state" name="user_state" /></li>

            <li class="_title"><?php __('Country'); ?></li>
            <li class="_input"><input type="text" id="user_country" name="user_country" value="Thailand" /></li>
        </ul>
    </div>
    <div style="float: right; position: relative;">
        <table class="checkout_table" style="margin-top: 16px; width: 470px;">
            <tr>
                <th width="50px"><?php __('No.'); ?></th>
                <th width="140px"><?php __('Product name'); ?></th>
                <th width="100px"><?php __('Price for each'); ?></th>
                <th width="80px"><?php __('Amount'); ?></th>
                <th width="100px"><?php __('Price'); ?></th>
            </tr>
            <?php $cart = $this->session->userdata('cart'); ?>
            <?php $i = 1; ?>
            <?php $price = 0; ?>
            <?php foreach ($cart as $item): ?>
                <?php $product = $this->dashboard->get_product($item['product_id']); ?>
                <?php $product = $product[0]; ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $product['name']; ?></td>
                    <td><?php if (empty($product['metadata']['price']['value'])) echo " N/A "; else echo "&#3647;" . number_format($product['metadata']['price']['value']); ?></td>
                    <td><?php echo $item['total']; ?></td>
                    <td><?php if (empty($product['metadata']['price']['value'])) echo "&#3647;0"; else echo "&#3647;" . number_format($product['metadata']['price']['value'] * $item['total']); ?></td>
                </tr>
                <?php if (empty($product['metadata']['price']['value'])) $price += 0; else $price += $product['metadata']['price']['value'] * $item['total']; ?>
                <?php $i++; ?>
            <?php endforeach; ?>
            <tr class="checkout_total">
                <td colspan="4"><b><?php __('Total'); ?></b></td>
                <td style="border: 1px solid white;"><?php echo "&#3647;" . number_format($price); ?></td>
            </tr>
        </table>
        <span class="shipping_method_final">
            <p style="color: #535353; font-size: 12px;">
                (<b><?php __('Transportation'); ?>:</b>
                <?php $shipping = $this->session->userdata('shipping_method'); ?>
                <?php __('Shipping via ' . shipping_type($shipping)); ?>)
            </p>
        </span>
        <input id="order_shipping" type="hidden" name="order_shipping" value="<?php echo $this->session->userdata('shipping_method'); ?>">
        <a class="confirm_checkout" href="#done"><?php __('Processed your order'); ?></a>
    </div>
    <?php $i = 1; ?>
    <?php foreach ($cart as $item): ?>
        <input type="hidden" id="<?php echo "product_qty_{$i}"; ?>" name="<?php echo "product_qty_{$i}"; ?>" value="<?php echo $item['total']; ?>" />
        <input type="hidden" id="<?php echo "product_id_{$i}"; ?>" name="<?php echo "product_id_{$i}"; ?>" value="<?php echo $item['product_id']; ?>" />
        <input type="hidden" name="return_url" value="<?php echo site_url('pages/order_complete'); ?>" />
        <?php $i++; ?>
    <?php endforeach; ?>
    <?php echo form_close(); ?>
</div>