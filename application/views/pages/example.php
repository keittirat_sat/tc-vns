<?php
/**
 * Example
 * 
 * @package Pages
 */
?>

<h1>Locale enable</h1>
<pre><?php print_r($enable_locale); ?></pre>

<h1>Now system locale is</h1>
<pre><?php print_r($default_locale); ?></pre>

<h1>Change system locale</h1>
<?php foreach ($enable_locale as $value): ?>
    <div>
        <a href="<?php echo site_url('pages/example') ?>?locale=<?php echo $value['sign']; ?>">
            <?php echo $value['name']; ?>
        </a>
    </div>
<?php endforeach; ?>

<hr />

<h1>Get all category</h1>
<pre><?php print_r($all_category); ?></pre>

<h1>Get category by specific ID</h1>
<pre><?php print_r($category); ?></pre>

<h1>Count category number</h1>
<pre><?php print_r($count_category); ?></pre>

<hr />

<h1>Get all product</h1>
<pre><?php print_r($all_product); ?></pre>

<h1>Get product by specific ID</h1>
<pre><?php print_r($product); ?></pre>

<h1>Get product by category ID</h1>
<pre><?php print_r($cat_product); ?></pre>

<h1>Count all product in any category</h1>
<pre><?php print_r($count_product); ?></pre>

<h1>Count product in specific category</h1>
<pre><?php print_r($count_cat_product); ?></pre>

<h1>Cart session</h1>
<pre><?php print_r($this->session->userdata('cart')); ?></pre>