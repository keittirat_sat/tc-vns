<?php if (!empty($product)): ?>
    <?php
    if (empty($product['file'][0]['id'])) {
        $image = image_asset_url("noimage.jpg");
        $fb_image = image_asset_url("noimage.jpg");
    } else {
        $img = $product['file'][0];
        $image = "/" . $img['path'] . $img['name'];
        $fb_image = base_url().$img['path'] . $img['name'];
        if (!is_file($this->input->server('DOCUMENT_ROOT') . $image)) {
            $image = image_asset_url("noimage.jpg");
            $fb_image = image_asset_url("noimage.jpg");
        }
    }
    ?>

    <?php if ($product['parent_id'] != 0): ?>
        <?php $product_id = $product['parent_id']; ?>
    <?php else: ?>
        <?php $product_id = $product['id']; ?>
    <?php endif; ?>
    <?php $metadata = $product['metadata'] ?>
    <meta property="og:title" content="Chiang Mai Vanussanun Co.,Ltd. : <?php echo $product['name']; ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo current_url(); ?>" />
    <meta property="og:image" content="<?php echo $fb_image; ?>" />
    <meta property="og:site_name" content="<?php echo $product['name']; ?>" />
    <meta property="og:description" content="<?php echo $product['content']; ?>" />
    <meta property="fb:admins" content="1304175534" />
<?php else: ?>
    <meta property="og:title" content="Chiang Mai Vanussanun Co.,Ltd." />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo current_url(); ?>" />
    <meta property="og:image" content="<?php echo image_asset_url('header_mainlogo.png'); ?>" />
    <meta property="og:site_name" content="Chiang Mai Vanussanun Co.,Ltd." />
    <meta property="og:description" content="The Sales Of The Local Products From Both Within Chiang Mai And The Adjacent Provinces. These Products Have Become Widely Known. On The Part Of Pickled Vegetable And Fruit Manufacture It Has Been Developed Into Moderness And High Qualities For The Purpose Of Consumers Safety." />
    <meta property="fb:admins" content="1304175534" />
<?php endif; ?>