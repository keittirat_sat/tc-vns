<style>
    .recipe_detail_bg{
        width: 590px;
        min-height: 600px;
        background: url('<?php echo image_asset_url('bg_pattern.jpg'); ?>');
        padding-bottom: 55px;
    }

    .recipe_ribbon{
        background: url('<?php echo image_asset_url('lightbox_ribbon.png'); ?>') repeat-x bottom;
        height: 80px;
        display: block;
        padding-top: 30px;
        position: relative;
    }
    .recipe_ribbon span{
        line-height: 43px;
        padding: 19px 15px;
        font-size: 30px;
        font-weight: bold;
        font-style: italic;
        text-shadow: 1px 1px 1px #CCC;
        display: inline-block;
        z-index: 2;
    }
    a.mini_logo{
        width: 154px;
        height: 50px;
        background: url('http://dev.vanusnun.com/application/assets/image/lightbox_logo.png') no-repeat center;
        display: block;
        margin: -9px auto 0px;
        position: relative;
        z-index: 1;
    }
    img.picture_image{
        max-width: 200px;
        max-height: 200px;
        margin: 15px auto;
        display: block;
    }
    .recipe_instruction{
        width: 480px;
        margin: 15px auto 0px; 
        font-size: 13px;
        color: #000;
        text-shadow: 1px 1px 1px #ccc;
        line-height: 20px;
    }
    p.title{
        font-weight: bold;
        margin: 15px 0px;
    }
</style>
<div class="recipe_detail_bg">
    <div class="recipe_ribbon">
        <span><?php echo $recipe_name; ?></span>
    </div>
    <a href="<?php echo base_url(); ?>" class="mini_logo"></a>
    <img class="picture_image" src="<?php echo $recipe_image; ?>" />
    <div class="recipe_instruction">
        <p class="title">Ingredient</p>
        <div class="panel_text">
            <?php echo $ingredient; ?>
        </div>
        <p class="title">How To Cook</p>
        <div class="panel_text">
            <?php echo $instruction; ?>
        </div>        
    </div>
</div>