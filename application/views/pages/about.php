<div class="vanusnun_bg_about_page"></div>
<div class="vanusnun_bg_about_page_detail">    
    <div class="about_title"><?php __('About Us'); ?></div>
    <div class="sign_logo"></div>
    <div class="about_detail">
        <ul>
            <?php if ($lang == 'en_US') : ?>
                <li>Chiang Mai Vanussanun Co.,Ltd. Was Originated From A Domestic Business By Mr. Chok-Lee , The Father And Mrs. Lib-Eye Sae-Tang, The Mother. We Primarily Produced Pickled Vegetables And Fruits. Later It Has Been Proceeded By Mr. Chadchan Ekchaipattanakul And Legitimated For A Company With The Capital Investment Of 2,000,000 Baht.</li>
                <li>The Headquarters Is Located At 388 Chiang Mai - Lumpang Road, Tumbon Fah - Ham, Amphur Muang, Chiang Mai. Since Mr. Chadchan Ekchaipattanakul Proceeded The Business, He Has Expanded It To The Sales Of The Local Products From Both Within Chiang Mai And The Adjacent Provinces. These Products Have Become Widely Known. On The Part Of Pickled Vegetable And Fruit Manufacture It Has Been Developed Into Moderness And High Qualities For The Purpose Of Consumers Safety. The Products Under Vanussanun's Brand Name, Therefore, Are Needed By Their Market Places Because Their Sanitary Factors And Over - 20 - Year - Business Are Recommended.</li>
            <?php else: ?>
                <li>บริษัท เชียงใหม่ วนัสนันท์ จำกัด ได้ก่อกำเนิดขึ้นมาจากกิจการภายในครอบครัว โดย คุณพ่อชกลี และ คุณแม่ลิบอ้าย แซ่ตั้ง ในเบื้องต้นผลิตและจำหน่ายสินค้าประเภทผักและผลไม้ ดอง ต่อมาผู้สืบทอดกิจการคือ คุณ ชัดชาญ เอกชัยพัฒนกุล ได้ดำเนินการจดทะเบียนจัดตั้ง บริษัท ด้วยทุนจดทะเบียน 2 ล้านบาท เมื่อวันที่ 7 ธันวาคม 2532</li>
                <li>โดยมีสำนักงาน ตั้งอยู่เลขที่ 398 ถ.เชียงใหม่-ลำปาง ต.ฟ้าฮ่าม อ.เมือง จ.เชียงใหม่ หลังจากที่เข้ามาดำเนินกิจการ คุณชัดชาญ เอกชัยพัฒนกุล ได้ขยายกิจการสู่การจำหน่ายสินค้าของฝากที่เป็นผลิตภัณฑ์ พื้นบ้าน ทั้งของจังหวัด เชียงใหม่ และจังหวัดใกล้เคียงจนมีชื่อเสียงเป็นที่รู้จักโดยทั่วไป ด้านการผลิตและจำหน่ายสินค้าผัก และ ผลไม้ดอง ได้มีการพัฒนาผลิตภัณฑ์ให้มีความทันสมัย และมีคุณภาพโดยคำนึงถึงความปลอดภัยของผู้บริโภคเป็นสำคัญ สินค้าภาย ใต้ชื่อ วนัสนันท์ จึงเป็นที่ต้องการของตลาด เพราะ มีความมั่นใจในความสะอาดถูกอนามัย ในการบริโภค และเป็นที่เชื่อถือมากกว่า 20 ปี</li>
            <?php endif; ?>
        </ul>
    </div>
</div>
<div class="vanusnun_shadow"></div>