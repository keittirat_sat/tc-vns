<?php

/**
 * Response
 * 
 * @package User
 * @subpackage Views
 * @since 1.0
 * @author Nomkhonwaan ComputerScience 
 */

echo json_encode(array('status' => $status, 'message' => $message));

/* End of file response.php */
/* Location: ./application/modules/User/views/response.php */
