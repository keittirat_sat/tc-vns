<?php
/**
 * Sidebar
 * 
 * @package Dashboard
 * @subpackage Views
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
?>

<div id="hotlinks">
    <ul>
        <?php foreach ($menu as $index => $value): ?>
            <li>
                <a href="<?php echo $value['URL']; ?>">
                    <?php echo $value['text']; ?>
                </a>
            </li>

            <?php if ($index + 1 < count($menu)): ?>
                <li>|</li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>
</div><!-- #sidebar -->

<?php
/* End of file sidebar.php */
/* Location: ./application/modules/Dashboard/views/sidebar.php */
