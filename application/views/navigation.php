<?php
/**
 * Navigation
 * 
 * @package Dashboard
 * @subpackage Views
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
?>

<div id="navigation">
    <ul>
        <?php foreach ($menu as $value): ?>
            <li>
                <a href="<?php echo $value['URL']; ?>">
                    <?php echo $value['text']; ?>
                </a>
                <?php $cond_class = $value['cond']['class']; ?>
                <?php $cond_method = (isset($value['cond']['method']) ? $value['cond']['method'] : 'index'); ?>

                <?php if (!strcmp($cond_class, $class)): ?>
                    <?php if (isset($value['cond']['method'])): ?>
                        <?php if (!strcmp($cond_method, $method)): ?>
                            <div class="arrow-up"></div>
                        <?php endif; ?>
                    <?php else: ?>                
                        <div class="arrow-up"></div>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if (isset($value['sub'])): ?>
                    <div class="sub-menu round-corner no-top" style="opacity: 0; margin-top: 20px;">
                        <span class="arrow-up"></span>
                        <?php foreach ($value['sub'] as $index => $sub): ?>
                            <a href="<?php echo $sub['URL']; ?>" class="list">
                                <?php echo $sub['text']; ?>
                            </a>

                            <?php if ($index < count($value['sub']) - 1): ?>
                                <div class="divider"></div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div><!-- .sub-menu -->
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </ul>
</div><!-- #navigation -->

<script type="text/javascript">
    $(function()
    {
        $('#dsb_nav li').hover(
        function()
        {
            var sub = $(this).find('.sub-menu');
            
            $(sub).stop().animate(
            {
                marginTop: '0',
                opacity: 1
            }, 'fast');
            
            $(sub).addClass('shown');
        }, 
        function()
        {
            $('.sub-menu').stop().animate(
            {
                marginTop: '20px',
                opacity: 0
            }, 'fast');
        }); 
    });
</script>

<?php
/* End of file navigation.php */
/* Location: ./application/modules/Dashboard/views/navigation.php */
