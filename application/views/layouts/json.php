<?php
/**
 * Json
 * 
 * @package Layouts
 * @subpackage Layouts
 * @since 2.0
 * @author Nomkhonwaan ComputerScience
 */

header('Content-Type: text/plain');
echo $contents;

/* End of file json.php */
/* Location: ./application/modules/User/views/layouts/json.php */