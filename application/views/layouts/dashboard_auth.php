<?php
/**
 * Default Layout
 * 
 * @package Dashboard
 * @subpackage Layouts
 * @author Nomkhonwaan ComputerScience 
 */
?>

<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="utf-8" />

        <link rel="shortcut icon" href="<?php echo image_asset_url('favicon.ico', 'Dashboard'); ?>" />

        <title><?php echo (isset($title) ? $title : NULL); ?></title>

        <!-- javaScript -->
        <?php echo js_asset('jquery-1.7.2.min.js'); ?>
        <?php echo js_asset('jquery.metadata.js'); ?>
        <?php echo js_asset('cufon-yui.js'); ?>
        <?php echo js_asset('supermarket_400.font.js', 'Dashboard'); ?>
        <?php echo js_asset('dsb.script.js', 'Dashboard'); ?>

        <!-- StyleSheet -->
        <?php echo css_asset('reset.css', '960'); ?>
        <?php echo css_asset('960.css', '960'); ?>
        <?php echo css_asset('bootstrap.min.css', 'Bootstrap'); ?>
        <?php echo css_asset('dsb.style.css', 'Dashboard'); ?>

        <script type="text/javascript">
            Cufon.replace('.cufon');
        </script>
    </head>

    <body>
        <div id="dsb_body"><?php echo $contents; ?></div><!-- #dsb_body -->
    </body>
</html>

<?php
/* End of file default.php */
/* Location: ./application/views/layouts/default.php */