<?php
/**
 * Default
 * 
 * @package Layouts
 * @subpackage Layouts
 * @since 2.0
 * @author Nomkhonwaan ComputerScience 
 */
?>

<!DOCTYPE HTML>
<html lang="en-US" xmlns:fb="http://ogp.me/ns/fb#">
    <head>
        <meta charset="utf-8" />

        <link rel="shortcut icon" href="<?php echo image_asset_url("vanasnunfav.png") ?>" />

        <title>Chiang Mai Vanussanun Co.,Ltd. : <?php echo (isset($title) ? $title : NULL); ?></title>
        <?php echo css_asset('reset.css'); ?>
        <?php echo css_asset('960.css'); ?>
        <?php echo css_asset('colorbox.css'); ?>
        
        <?php echo css_asset('style-' . DEFAULT_LOCALE . '.css'); ?>

        <?php echo js_asset('jquery-1.7.2.min.js'); ?>
        <?php echo js_asset('jquery.metadata.js'); ?>
        <?php echo js_asset('jquery.colorbox.js'); ?>
        <?php echo js_asset('cufon-yui.js'); ?>
        <?php echo js_asset('TP_Tankhun_400-TP_Tankhun_700-TP_Tankhun_italic_400-TP_Tankhun_italic_700.font.js'); ?>
        <?php if (!empty($fb)): ?>
            <?php echo $fb; ?>
        <?php else: ?>
            <meta property="og:title" content="Chiang Mai Vanussanun Co.,Ltd." />
            <meta property="og:type" content="website" />
            <meta property="og:url" content="<?php echo current_url(); ?>" />
            <meta property="og:image" content="<?php echo image_asset_url('header_mainlogo.png'); ?>" />
            <meta property="og:site_name" content="Chiang Mai Vanussanun Co.,Ltd." />
            <meta property="og:description" content="The Sales Of The Local Products From Both Within Chiang Mai And The Adjacent Provinces. These Products Have Become Widely Known. On The Part Of Pickled Vegetable And Fruit Manufacture It Has Been Developed Into Moderness And High Qualities For The Purpose Of Consumers Safety." />
            <meta property="fb:admins" content="1304175534" />
        <?php endif; ?>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-32176545-1']);
            _gaq.push(['_setDomainName', 'vanusnun.com']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
            
            $(function()
            { 
                var flag = true;
                //                $('.locale_selector').live({
                //                    mouseenter: function(){
                //                        $(this).find('.locale_body').stop().show().css({'display':'block'}).animate({'paddingTop':'8px'},200);
                //                    },
                //                    mouseleave: function(){
                //                        $(this).find('.locale_body').stop().hide().animate({'paddingTop':'2px'},200);
                //                        $(this).find('.locale_body.selected').show();
                //                    }
                //                });
                
                $('.locale_selector').hover(function()
                {
                    $(this).find('ul').stop().animate({ height: 'toggle' }, 'fast');
                    $('#default_locale #locale-separater').fadeIn('fast');
                }, 
                function()
                {
                    $(this).find('ul').stop().animate({ height: 'toggle' }, 'fast');
                    $('#default_locale #locale-separater').fadeOut('fast');
                });
                
                $('.locale_body').not('#default_locale').click(function()
                {
                    var sign = $(this).metadata().lang_id;
                
                    // $('#default_locale').text($(this).text());
                
                    $.post('/pages/set_locale', 
                    {
                        locale: sign
                    },
                    function()
                    {
                        window.location.reload();
                    });
                });
                
                //                $('.locale_body').click(function(){
                //                    $('.locale_body').removeClass('selected');
                //                    $(this).css({'display':'block'}).addClass('selected');
                //
                //                    $('.locale_body').stop().hide().animate({'paddingTop':'2px'},200);
                //                    $('.locale_body.selected').show(); 

                //                    $.post('/pages/set_locale',{locale: $(this).metadata().lang_id},function(res){
                //                        if($.trim(res) == '1'){
                //                            window.location.reload();
                //                        }
                //                    });
                //                });
                
//                $('.cart_sub a.logo').click(function(){                    
//                    $('.cart_sub table').animate({'height':'toggle'},300);
//                    $('.sub_total, .checkout').animate({'height':'toggle','bottom':'toggle'},300);
//                });
                
                $('.click_follow, .mail_icn').live({
                    click: function(){
                        $('.subscribe_btn input[type=text]').stop().animate({'width':"200px"},300,function(){
                            $('.subscribe_btn input[type=button]').stop().animate({'width':"52px"},200);
                            $('.flag_subscribe').animate({'height':'69px','bottom':'-69px'},200);
                        }).focus();
                        flag = false;
                        $('.click_follow').css({'z-index':'0'});
                        $('.subscribe_btn').css({'z-index':'2'}).animate({'opacity': '1'},100);
                    }
                });                
                
                $('.subscribe_btn input[type=button]').click(function(){
                    email = $('.subscribe_btn input[type=text]').val();
                    $.post('/pages/add_subscribe',{email: email},function(html){
                        if(html.status == 'success'){
                            $('.flag_subscribe').animate({'height':'0px','bottom':'0px'},200,function(){
                                $('.subscribe_btn input[type=text]').stop().animate({'width':"0px"},300,function(){
                                    $('.subscribe_btn input[type=button]').stop().animate({'width':"0px"},200);
                                    $('.subscribe_btn').animate({'opacity': '0'},100).css({'z-index':'0'});                        
                                    $('.click_follow').css({'z-index':'2'});                 
                                });
                            });
                        }else{
                            alert(html.status);
                        }
                    },'json');
                });
                
                $('.subscribe_btn input[type=text]').focus(function(){
                    flag = false;
                });
                
                $('.subscribe_btn input[type=text]').blur(function(){
                    flag = true;
                    setTimeout(function(){
                        if(flag){   
                            $('.flag_subscribe').animate({'height':'0px','bottom':'0px'},200,function() {                               
                                $('.subscribe_btn input[type=text]').stop().animate({'width':"0px"},300,function(){
                                    $('.subscribe_btn input[type=button]').stop().animate({'width':"0px"},200);
                                    $('.subscribe_btn').animate({'opacity': '0'},100).css({'z-index':'0'});                        
                                    $('.click_follow').css({'z-index':'2','bottom':'0px'});
                                });
                            });
                        }
                    },3000);
                });
                
                $('.cart_display tr.item_in_cart').live({
                    click: function(){
                        $.colorbox({'href':'/pages/edit_cart?product_id='+$(this).metadata().product_id});
                    }
                });
                
                Cufon.replace('.btn_menu, .product_category .category, .product_filter table span, .about_title');
            });           
        </script>
    </head>

    <body>
        <div class="main_container">
            <a class="vanusnun_main_logo" href="<?php echo site_url(); ?>"></a>
            <div class="cart_sub">
                <a class="logo"></a>
                <div class="cart_display">
                    <?php echo $cart_display; ?>
                </div>
            </div>
            <div class="top_menu">
                <div class="clearfix menu_btn_group">
                    <a class="btn_home" href="<?php echo site_url('pages/index'); ?>"></a>
                    <a class="btn_menu" href="<?php echo site_url('pages/about'); ?>"><?php __('About Us'); ?></a>
                    <a class="btn_menu" href="<?php echo site_url('pages/product'); ?>"><?php __('Product'); ?></a>
                    <a class="btn_menu" href="<?php echo site_url('pages/export'); ?>"><?php __('Lanna Recipe'); ?></a>
                    <a class="btn_menu" href="<?php echo site_url('pages/recipe'); ?>"><?php __('Recipe'); ?></a>
                    <a class="btn_menu" href="<?php echo site_url('pages/video'); ?>"><?php __('Video'); ?></a>
                    <a class="btn_menu last" href="<?php echo site_url('pages/contact'); ?>"><?php __('Contact Us'); ?></a>
                </div>
            </div>

            <div class="main_content clearfix">
                <div class="vanusnun_language_facebook clearfix">
                    <a href="http://www.facebook.com/pages/%E0%B9%80%E0%B8%8A%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B9%83%E0%B8%AB%E0%B8%A1%E0%B9%88%E0%B8%A7%E0%B8%99%E0%B8%B1%E0%B8%AA%E0%B8%99%E0%B8%B1%E0%B8%99%E0%B8%97%E0%B9%8C/108391455897916" class="fb_btn" target="_blank"></a>
                    <div class="follow_vanusnun">
                        <a class="mail_icn" href="#follow_us" ></a>
                        <div class="click_follow">&laquo; click!</div>
                        <div class="subscribe_btn clearfix">
                            <input type="text" value="">
                            <input type="button" value="">
                            <div class="flag_subscribe">
                                <ul>
                                    <li>E-News Letter</li>
                                    <li>Keep following us</li>
                                    <li>Our news and Product</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="locale_selector">
                        <div class="locale_tail"></div>
                        <div id="default_locale" class="locale_body" style="color: #fff; cursor: default;">
                            <?php $default_locale = explode('_', $locale); ?>
                            <?php echo strtoupper($default_locale[0]); ?>
                            <div id="locale-separater" style="border-bottom: 1px dashed #fff; display: none; margin: 3px auto 0 auto; width: 64%;"></div>
                        </div>
                        <?php if (!empty($all_locale)): ?>
                            <ul style="display: none;">
                                <?php foreach ($all_locale as $lang): ?>
                                    <li class="locale_body { lang_id: '<?php echo $lang['sign']; ?>' } <?php echo (!strcmp($locale, $lang['sign']) ? 'selected' : NULL); ?>">
                                        <?php $lang_name = explode('_', $lang['sign']); ?>
                                        <?php echo strtoupper($lang_name[0]); ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                        <div class="locale_head"></div>
                    </div>
                </div>
                <?php echo $contents; ?>
            </div>

        </div>
        <?php if ($this->agent->is_mobile()): ?>
            <?php $class = 'ipad'; ?>
        <?php else: ?>
            <?php $class = ''; ?>
        <?php endif; ?>
        <div class="main_footer <?php echo $class; ?>">
            <div class="detail_footer clearfix">
                <div class="vanusnun_standard"></div>
                <div class="vanusnun_footer_text">
                    <br/>
                    398 Chiang Mai - Lampang Rd. T.Fah-Hamm Muang<br/>
                    Chiang Mai , Thailand 50000<br/>
                    Tel. 66 +53 240829, 243010 Fax. 66 +53 249518<br/>
                    Email: Info@Vanusanun.com
                </div>
<!--                <div class="gdi_decoration">All Right Reserved &copy;<?php echo date('Y'); ?>. Powered by <a href="http://www.gdidevelop.com" target="_blank">GDi R&amp;D</a></div>-->
            </div>            
        </div>

    </body>
</html>

<?php
/* End of file default.php */
/* Location: ./application/views/layouts/default.php */