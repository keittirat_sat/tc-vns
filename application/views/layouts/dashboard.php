<?php
/**
 * Default Layout
 * 
 * @package Dashboard
 * @subpackage Layouts
 * @author Nomkhonwaan ComputerScience 
 */
?>

<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="utf-8" />

        <link rel="shortcut icon" href="<?php echo image_asset_url('favicon.ico', 'Dashboard'); ?>" />

        <title><?php echo (isset($title) ? $title : NULL); ?></title>

        <!-- javaScript -->
        <?php echo js_asset('jquery-1.7.2.min.js'); ?>
        <?php echo js_asset('jquery.metadata.js'); ?>
        <?php echo js_asset('jquery.blockUI.js'); ?>
        <?php echo js_asset('jquery.colorbox-min.js', 'ColorBox'); ?>
        <?php echo js_asset('cufon-yui.js'); ?>
        <?php echo js_asset('supermarket_400.font.js', 'Dashboard'); ?>
        <?php echo js_asset('dsb.script.js', 'Dashboard'); ?>

        <!-- StyleSheet -->
        <?php echo css_asset('reset.css', '960'); ?>
        <?php echo css_asset('960.css', '960'); ?>
        <?php echo css_asset('bootstrap.min.css', 'Bootstrap'); ?>
        <?php echo css_asset('colorbox.css', 'ColorBox'); ?>
        <?php echo css_asset('dsb.style.css', 'Dashboard'); ?>

        <script type="text/javascript">
            Cufon.replace('.cufon');
        </script>
    </head>

    <body>
        <div id="dsb_header" class="clearfix">
            <div class="container_12">
                <div id="dsb_nav" class="grid_12">
                    <?php echo $navigation; ?>
                </div><!-- #dsb_nav -->

                <!--                <div class="gdi_logo"></div>-->
            </div><!-- .container_12 -->
        </div><!-- #dsb_header -->

        <div id="dsb_body">
            <?php echo $contents; ?>    
        </div><!-- #dsb_body -->

        <div id="dsb_footer">
            <div class="container_12">
                <div class="grid_6">
                    Copyright © 2012 <a href="http://gdidevelop.com">GDI Development</a>, All rights reserved.
                </div>
                
                <div class="grid_6 align-right">
                    <?php echo $hotlinks; ?>
                </div>
            </div>
        </div><!-- #dsb_footer -->
    </body>
</html>

<?php
/* End of file default.php */
/* Location: ./application/views/layouts/default.php */