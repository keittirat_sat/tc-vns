<!DOCTYPE html>
<html lang="en-US" xmlns:fb="http://ogp.me/ns/fb#">
    <head>
        <meta charset="UTF-8">
        <link rel="shortcut icon" href="<?php echo image_asset_url("vanasnunfav.png") ?>" />
        <title>Chiang Mai Vanussanun Co.,Ltd.<?php echo (isset($title) ? " : " . $title : NULL); ?></title>

        <!--CSS Section-->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">        
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
        <?php echo css_asset("nivo-slider.css"); ?>
        <?php echo css_asset("themes/default/default.css"); ?>
        <?php echo css_asset("aec_2014.css"); ?>
        <?php echo isset($css) ? $css : "" ?>

        <!--JS Section-->
        <?php echo js_asset('jquery-1.7.2.min.js'); ?>
        <?php echo js_asset("jquery.bgpos.js"); ?>
        <?php echo js_asset('jquery.form.js'); ?>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>        
        <script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-ui-map/3.0-rc1/min/jquery.ui.map.full.min.js"></script>   
        <?php echo js_asset("jquery.nivo.slider.pack.js"); ?>

        <!--FB tag-->
        <meta property="og:title" content="Chiang Mai Vanussanun Co.,Ltd." />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="<?php echo current_url(); ?>" />
        <meta property="og:image" content="<?php echo image_asset_url('header_mainlogo.png'); ?>" />
        <meta property="og:site_name" content="Chiang Mai Vanussanun Co.,Ltd." />
        <meta property="og:description" content="The Sales Of The Local Products From Both Within Chiang Mai And The Adjacent Provinces. These Products Have Become Widely Known. On The Part Of Pickled Vegetable And Fruit Manufacture It Has Been Developed Into Moderness And High Qualities For The Purpose Of Consumers Safety." />
        <meta property="fb:admins" content="1304175534" />

        <!--Overide CSS-->
        <style>
            /*Responsive disabled*/
            body{
                min-width: 970px;
            }

            .container {
                max-width: none !important;
                width: 970px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 txt_center">
                    <?php echo image_asset('Header_logo.png'); ?>
                </div>
            </div>
            <?php echo $contents; ?>
        </div>
    </body>
</html>