<!DOCTYPE html>
<html lang="en-US" xmlns:fb="http://ogp.me/ns/fb#">
    <head>
        <meta charset="UTF-8">
        <link rel="shortcut icon" href="<?php echo image_asset_url("vanasnunfav.png") ?>" />
        <title>Chiang Mai Vanussanun Co.,Ltd.<?php echo (isset($title) ? " : " . $title : NULL); ?></title>

        <!--CSS Section-->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">        
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
        <?php echo css_asset("nivo-slider.css"); ?>
        <?php echo css_asset("themes/default/default.css"); ?>
        <?php echo css_asset("aec_2014.css"); ?>
        <?php echo isset($css) ? $css : "" ?>

        <!--JS Section-->
        <?php echo js_asset('jquery-1.7.2.min.js'); ?>
        <?php echo js_asset("jquery.bgpos.js"); ?>
        <?php echo js_asset('jquery.form.js'); ?>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>        
        <script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-ui-map/3.0-rc1/min/jquery.ui.map.full.min.js"></script>   
        <?php echo js_asset("jquery.nivo.slider.pack.js"); ?>
        <?php echo js_asset("cufon-yui.js"); ?>
        <?php echo js_asset("Quark_300-Quark_700.font.js"); ?>

        <!--FB tag-->
        <meta property="og:title" content="Chiang Mai Vanussanun Co.,Ltd." />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="<?php echo current_url(); ?>" />
        <meta property="og:image" content="<?php echo image_asset_url('header_mainlogo.png'); ?>" />
        <meta property="og:site_name" content="Chiang Mai Vanussanun Co.,Ltd." />
        <meta property="og:description" content="The Sales Of The Local Products From Both Within Chiang Mai And The Adjacent Provinces. These Products Have Become Widely Known. On The Part Of Pickled Vegetable And Fruit Manufacture It Has Been Developed Into Moderness And High Qualities For The Purpose Of Consumers Safety." />
        <meta property="fb:admins" content="1304175534" />

        <!--Overide CSS-->
        <style type="text/css">
            /*Responsive disabled*/
            body{
                min-width: 970px;
            }

            .container {
                max-width: none !important;
                width: 970px;
            }
        </style>

        <script type="text/javascript">
            Cufon.replace('.cufon');
        </script>
    </head>
    <body>
        <!--.menu-->
        <section class='menu'>
            <div class="container">
                <div class='row' style='height: 155px;'>
                    <div class='col-xs-12'>
                        <?php echo image_asset('Header_logo.png'); ?>
                        <ul class='main_menu_btn vns_menu clearfix <?php echo $menu; ?>'>
                            <li <?php echo $menu == "menu_1" ? "class='active'" : ""; ?>><a data-append='18' href='<?php echo site_url('vns'); ?>' class='home_menu_btn'></a></li>     
                            <li <?php echo $menu == "menu_2" ? "class='active'" : ""; ?>><a data-append='104' href="<?php echo site_url('vns/aboutus') ?>">About Us</a></li>     
                            <li <?php echo $menu == "menu_3" ? "class='active'" : ""; ?>><a data-append='208' href="<?php echo site_url('vns/product') ?>">Product</a></li>     
                            <li <?php echo $menu == "menu_4" ? "class='active'" : ""; ?>><a data-append='300' href="<?php echo site_url('vns/video') ?>">Video</a></li>     
                            <li <?php echo $menu == "menu_5" ? "class='active'" : ""; ?>><a data-append='402' href="<?php echo site_url('vns/contactus') ?>">Contact Us</a></li>
                        </ul>
                        <div class="cart_toggle" data-trigger="manual" data-placement="left" data-container="body" data-content="<?php echo $locale == 'en_US' ? "Your item added to cart already" : "ได้เพิ่มสินค้าลงตระกร้าเรียบร้อยแล้ว"; ?>">
                            <div class="txt_center" style="padding:60px 0px 0px;">
                                <b style='color:#666;'>Cart&nbsp;</b><span class="badge badge-success" id="total_item">0</span>
                            </div>
                        </div>
                        <div class="locale_setting">
                            <div class="txt_center current_lang" style="padding-top: 31px;">
                                <span class="badge badge-success"><?php echo $locale == "en_US" ? "ENG" : "TH" ?></span>
                            </div>
                            <ul class="lang_list">
                                <li><a href="#eng" data-lang="en_US">ENG</a></li>
                                <li><a href="#th" data-lang="th_TH">TH</a></li>
                            </ul>
                            <div class="lid_lang"></div>
                        </div>
                        <div class="cart_display">
                            <h3 class="txt_center quark txt_shadow green"><b>Shopping List</b></h3>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="45">No.</th>
                                        <th>Name</th>
                                        <th width="75">Amount</th>
                                        <th width="90" colspan="2">Price</th>
                                    </tr>
                                </thead>
                                <tbody id="cart_display"></tbody>
                                <tfoot id="foot_cart">
                                    <tr>
                                        <th colspan="3" class="green txt_right">total price</th>
                                        <th class="txt_center" id="total_cart_price" style="width:60px;"></th>
                                        <th style="width:30px;"></th>
                                    </tr>
                                </tfoot>
                            </table>
                            <h4 class="txt_center red quark" id="not_found_item_cart"><?php echo $locale == "en_US" ? "No item in cart" : "ไม่พบสินค้าในตระกร้า"; ?></h4>
                            <p class="txt_right" style="padding: 0px 10px;">
                                <a href="<?php echo site_url("vns/checkout"); ?>" id="checkout_continue" class="btn btn-success"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;Checkout now!</a>
                                <button class="btn btn-danger" id="cart_close"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;Cancel</button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section><!--/.menu-->        

        <?php echo $contents; ?>

        <section class='footer'>
            <div class='curv_footer'></div>
            <div class='content_footer'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-xs-4'>
                            <h1 class='green quark'>
                                <b>FACE</b>BOOK
                            </h1>
                            <div class="fb-like-box" data-href="https://www.facebook.com/pages/%E0%B9%80%E0%B8%8A%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B9%83%E0%B8%AB%E0%B8%A1%E0%B9%88%E0%B8%A7%E0%B8%99%E0%B8%B1%E0%B8%AA%E0%B8%99%E0%B8%B1%E0%B8%99%E0%B8%97%E0%B9%8C/108391455897916" data-width="293" data-colorscheme="dark" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
                        </div>
                        <div class='col-xs-4'>
                            <h1 class='green quark'>
                                <b>CON</b>TACT
                            </h1>
                            <ul class='footer_address white'>
                                <li>
                                    <?php echo image_asset("footer_contact_pin.png") ?>
                                    <?php if ($locale == "en_US"): ?>
                                        398 Chiang Mai - Lampang Rd. T.Fah-Hamm Muang Chiang Mai , Thailand 50000
                                    <?php else: ?>
                                        เลขที่ 398 ถ.เชียงใหม่-ลำปาง ต.ฟ้าฮ่าม อ.เมือง จ.เชียงใหม่ 50000
                                    <?php endif; ?>
                                </li>
                                <li>
                                    <?php echo image_asset("footer_contact_address.png") ?>
                                    Tel. 053-240829, 243010<br/>
                                    Fax. 053-249518
                                </li>
                                <li>
                                    <?php echo image_asset("footer_contact_mail.png") ?>
                                    Email: <?php echo safe_mailto("sales.vanusnun@gmail.com", null, array('class' => 'link_inherite')); ?>
                                </li>
                            </ul>
                        </div>
                        <div class='col-xs-4 white'>
                            <h1 class='green quark'>
                                <b>NEWS</b>LETTER
                            </h1>
                            <p>E-News Letter Keep following us Our news and Product</p>
                            <form role="form" action="<?php echo site_url('vns/add_subscribe'); ?>" class="subscribe_form">
                                <div class="input-group">
                                    <input type="email" placeholder="Email" name="email" required="required" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-success scb_btn" type="submit" data-loading-text="Adding...">SUBMIT</button>
                                    </span>
                                </div><!-- /input-group -->
                            </form>
                            <p><?php echo image_asset("footer_icon_GMP.png", '', array('class' => 'img-responsive margin_top_10')); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div id="fb-root"></div>
        <!--Google Analytics-->        
        <script type="text/javascript">
            function get_cart() {
                $.get('<?php echo site_url('vns/get_cart'); ?>', function(res) {
                    console.log(">>", res.length);
                    var table = "";
                    var total_amount = 0;
                    var amount = 0;
                    if (res.length > 0) {
                        $.each(res, function(id, val) {
                            table += "<tr>";
                            table += "<td align='center'>" + (id + 1) + "</td>";
                            table += "<td>" + val.name + "</td>";
                            table += "<td align='center' class='each_amount'>" + val.total + "</td>";
                            table += "<td align='right' class='each_price'>" + val.price * val.total + "฿</td>";
                            table += "<td align='center' class='red'><a data-id='" + val.id + "' href='#del' class='remove_stock link_inherite'><i class='glyphicon glyphicon-remove'></i></a></td>";
                            table += "</tr>";
                            total_amount += val.price * val.total;
                            amount += parseInt(val.total);
                        });
                        $('#cart_display').html(table);
                        $('#foot_cart').show();
                        $('#not_found_item_cart').hide();
                        $('#total_cart_price').text(total_amount + "฿");
                        $('#checkout_continue').removeAttr('disabled');
                    } else {
                        $('#foot_cart').hide();
                        $('#not_found_item_cart').show();
                        $('#cart_display').html("");
                        $('#checkout_continue').attr('disabled', 'disabled');
                    }
                    $('#total_item').text(amount);
                }, 'json');
            }


            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-32176545-1']);
            _gaq.push(['_setDomainName', 'vanusnun.com']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=194531797284411";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            //Main Template control
            $(function() {
                get_cart();

                $('ul.vns_menu li a').mouseenter(function() {
                    var temp = $(this).attr('data-append');
                    $('ul.vns_menu').stop().animate({'backgroundPosition': '(' + temp + 'px 0)'}, {duration: 800});
                });

                $('ul.vns_menu').mouseleave(function() {
                    var temp = $('ul.vns_menu li.active a').attr('data-append');
                    $('ul.vns_menu').stop().animate({'backgroundPosition': '(' + temp + 'px 0)'}, {duration: 800});
                });

                $('.subscribe_form').ajaxForm({
                    beforeSend: function() {
                        $('.scb_btn').attr('disabled', 'disabled').text("Adding...");
                    },
                    complete: function(xhr) {
                        console.log(xhr.responseText);
                        var json = $.parseJSON(xhr.responseText);
                        if (json.status === "success") {
                            $('.scb_btn').removeAttr('disabled').text("Added!!!");
                            $('.subscribe_form').trigger('reset');
                        } else {
                            alert(json.status);
                        }
                    }
                });

                $('.mask_product').fadeOut(0);

                $('.product_thumb').mouseenter(function() {
                    if ($(this).find('.swap_btn').attr('data-enable') == 1) {
                        $(this).find('form').show();
                        $(this).find('.swap_btn').hide();
                    } else {
                        $(this).find('form').hide();
                        $(this).find('.swap_btn').show();
                    }
                    $(this).find('.mask_product').fadeIn(150);
                }).mouseleave(function() {
                    if ($(this).find('.swap_btn').attr('data-enable') == 0) {
                        $(this).find('.mask_product').fadeOut(150);
                    }
                });

                $('.swap_btn').click(function() {
                    $(this).attr('data-enable', 1);
                    $(this).siblings('form').show();
                    $(this).hide();
                });

                $('.cancel_cart').click(function() {
                    var target = $(this).attr('data-target');
                    $('#' + target).find('.swap_btn').attr('data-enable', 0);
                    $('#' + target).find('.mask_product').fadeOut(150);
                });

                $.each($('.add_order'), function(idx, ele) {
                    $(ele).ajaxForm({
                        beforeSend: function() {

                        },
                        complete: function(xhr) {
                            console.log(xhr.responseText);
                            var json = $.parseJSON(xhr.responseText);
                            if (json.status === "success") {
                                $('#product_' + json.id).find('.mask_product').fadeOut(150);
                                get_cart();
                                $('.cart_toggle').popover('show');
                            }
                        }
                    });
                });

                $('.lang_list').animate({height: 'toggle'}, 0);
                $('.locale_setting').mouseenter(function() {
                    $('.lang_list').animate({height: 'toggle'}, 300);
                }).mouseleave(function() {
                    $('.lang_list').animate({height: 'toggle'}, 300);
                });

                $('.lang_list a').click(function() {
                    var lang_id = $(this).attr('data-lang');
                    $.post('<?php echo site_url('vns/set_locale'); ?>', {locale: lang_id}, function(res) {
                        if (res.status === "success") {
                            location.reload();
                        } else {
                            alert("Connection Error");
                        }
                    }, 'json');
                });

                $('.cart_display').animate({'height': 'toggle', 'opacity': 0}, 0);
                $('.cart_toggle').toggle(function() {
                    $('.cart_display').animate({'height': 'toggle', 'opacity': 1}, 300);
                    $(this).popover('hide');
                }, function() {
                    $('.cart_display').animate({'height': 'toggle', 'opacity': 0}, 300);
                    $(this).popover('hide');
                });

                $('#cart_close').click(function() {
                    $('.cart_toggle').trigger('click');
                });

                $('tbody tr').live({
                    mouseenter: function() {
                        $(this).find('a').stop().animate({'opacity': 1}, 150);
                    },
                    mouseleave: function() {
                        $(this).find('a').stop().animate({'opacity': 0}, 150);
                    }
                });

                $('a.remove_stock').live({
                    click: function() {
                        if (confirm('<?php echo $locale === "en_US" ? "Do you want to delete this item?" : "ยืนยันการลบสินค้าออกจาตระกร้าสินค้า" ?>')) {
                            var product_id = $(this).attr('data-id');
                            $.get('<?php echo site_url('vns/del_cart'); ?>', {product_id: product_id}, function(res) {
                                if (res.status === "success") {
                                    get_cart();
                                } else {
                                    alert("connection Error");
                                }
                            }, 'json');
                        }
                    }
                });
                
                $(".popover_vns").popover({trigger:"hover"})
            });
        </script>
    </body>
</html>
