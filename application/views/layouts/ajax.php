<?php
/**
 * Ajax
 * 
 * @package Layouts
 * @subpackage Layouts
 * @since 1.0
 * @author Nomkhonwaan ComputerScience
 */

echo $contents;

/* End of file ajax.php */
/* Location: ./application/views/layouts/ajax.php */