<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

// start session
if(!session_id()) session_start();


/**
 * Set user session data
 * 
 * @param string $key
 * @param mixed $value 
 * 
 */
function set_session($key, $value)
{
    $_SESSION[$key] = $value;
}

/**
 * Get user session data
 * 
 * @param string $key
 * @return mixed
 */
function get_session($key)
{
    return (isset($_SESSION[$key]) ? $_SESSION[$key] : NULL);
}

/**
 * Remove session by $key name
 * 
 * @param string $key 
 */
function remove_session($key)
{
    if (isset($_SESSION[$key]))
    {
        unset($_SESSION[$key]);
    }
}

/* End of file session_helper.php */
/* Location: ./application/helpers/session_helper.php */