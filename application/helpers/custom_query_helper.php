<?php

function get_lates_product($total) {
    $ci = get_CI();
    $product = $ci->dashboard->get_product(NULL, NULL, NULL, $total * 2, 'desc');
    foreach ($product as &$each_product) {
        $category = $ci->dashboard->get_category($each_product['cat_id']);
        $each_product['cate_info'] = $category;
    }
    return $product;
}

function set_product_rows($product, $col = 3) {
    $data = array();
    $i = -1;
    foreach ($product as $index => $each_product) {
        if (!($index % $col)) {
            $i++;
        }
        
        if (!array_key_exists($i, $data)) {
            $data[$i] = array();
        }

        array_push($data[$i], $each_product);
    }
    
    return $data;
}
