<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * -----------------------------------------------------------------------------
 * Global function
 * ----------------------------------------------------------------------------- 
 */

/**
 * Return CI instance
 * 
 * @return object 
 */
function get_CI()
{
    $CI = &get_instance();

    return $CI;
}

/**
 * Get domain name
 * 
 * @return string
 */
function get_rsite_url()
{
    $site_url = site_url();

    // replace http|https and any special char exclude slashes
    $rsite_url = preg_replace('/(http:\/\/|https:\/\/|\/)/', '', $site_url);

    return $rsite_url;
}

/**
 * -----------------------------------------------------------------------------
 * Site configure function 
 * ----------------------------------------------------------------------------- 
 */

/**
 * -----------------------------------------------------------------------------
 * Text and content function 
 * ----------------------------------------------------------------------------- 
 */

/**
 * Locale string
 * 
 * @param string $string
 * @param boolean $print
 * @return string 
 */
function __($string, $print = TRUE)
{
   // default locale is en_US
    $default_locale = get_cookie('default_locale');

    if (!strcmp($default_locale, 'en_US'))
    {
        if ($print === TRUE) echo $string;
        return $string;
    }
    else
    {
        $CI = get_CI();
        $i18n = $CI->load->config('i18n');

        if (isset($i18n['th_TH'][$string]))
        {
            if ($print === TRUE) echo $i18n['th_TH'][$string];
            return $i18n['th_TH'][$string];
        }
        else
        {
            if ($print === TRUE) echo $string;
            return $string;
        }
    }
}

/**
 * generate slug from post title
 * 
 * @param string $string
 * @return string 
 */
function slug($string)
{
    return preg_replace('/[^A-Za-z0-9-ก-ฮ่้๊๋ิีึืุูเะ๐ไใ]+/', '-', $string);
}

/**
 * Generate page number
 *
 * @param int $page
 * @param int $totalitems
 * @param int $limit
 * @param int $adjacents
 * @param int $targetpage
 * @param int $pagestring
 * @return string 
 */
function get_pagination($page = 1, $totalitems, $limit = 15, $adjacents = 1, $pagestring = '#')
{
    //other vars
    $prev = $page - 1;         //previous page is page - 1
    $next = $page + 1;         //next page is page + 1
    $lastpage = ceil($totalitems / $limit);    //lastpage is = total items / items per page, rounded up.
    $lpm1 = $lastpage - 1;        //last page minus 1

    /*
      Now we apply our rules and draw the pagination object.
      We're actually saving the code to a variable in case we want to draw it more than once.
     */
    $pagination = "";

    //pages	
    if ($lastpage < 7 + ($adjacents * 2)) //not enough pages to bother breaking it up
    {
        for ($counter = 1; $counter <= $lastpage; $counter++)
        {
            if ($counter == $page) $pagination .= "<li class=\"active\"><a href=\"{$pagestring}/{$counter}\" class=\"button\">{$counter}</a></li>";
            else $pagination .= "<li><a href=\"{$pagestring}/{$counter}\" class=\"button\">{$counter}</a></li>";
        }
    }

    elseif ($lastpage >= 7 + ($adjacents * 2)) //enough pages to hide some
    {
        //close to beginning; only hide later pages
        if ($page < 1 + ($adjacents * 3))
        {
            for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
            {
                if ($counter == $page) $pagination .= "<li class=\"active\"><a href=\"{$pagestring}/{$counter}\" class=\"button\">{$counter}</a></li>";
                else $pagination .= "<li><a href=\"{$pagestring}/{$counter}\" class=\"button\">{$counter}</a></li>";
            }
            $pagination .= "<li class=\"disabled\"><a href=\"#\" class=\"button\">...</a></li>";
            $pagination .= "<li><a href=\"{$pagestring}/{$lpm1}\" class=\"button\">{$lpm1}</a></li>";
            $pagination .= "<li><a href=\"{$pagestring}/{$lastpage}\" class=\"button\">{$lastpage}</a></li>";
        }

        //in middle; hide some front and some back
        elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
        {
            $pagination .= "<li class=\"active\"><a href=\"{$pagestring}/1\" class=\"button\">1</a></li>";
            $pagination .= "<li class=\"active\"><a href=\"{$pagestring}/2\" class=\"button\">2</a></li>";
            $pagination .= "<li class=\"disabled\"><a href=\"#\">...</a></li>";
            for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
            {
                if ($counter == $page) $pagination .= "<li class=\"active\"><a href=\"{$pagestring}/{$counter}\" class=\"button\">{$counter}</a></li>";
                else $pagination .= "<li><a href=\"{$pagestring}/{$counter}\" class=\"button\">{$counter}</a></li>";
            }
            $pagination .= "<li class=\"disabled\"><a href=\"#\">...</a></li>";
            $pagination .= "<li><a href=\"{$pagestring}/{$lpm1}\" class=\"button\">{$lpm1}</a></li>";
            $pagination .= "<li><a href=\"{$pagestring}/{$lastpage}\" class=\"button\">{$lastpage}</a></li>";
        }

        //close to end; only hide early pages
        else
        {
            $pagination .= "<li class=\"active\"><a href=\"{$pagestring}/1\" class=\"button\">1</a></li>";
            $pagination .= "<li class=\"active\"><a href=\"{$pagestring}/2\" class=\"button\">2</a></li>";
            $pagination .= "<li class=\"disabled\"><a href=\"#\">...</a></li>";
            for ($counter = $lastpage - (1 + ($adjacents * 3)); $counter <= $lastpage; $counter++)
            {
                if ($counter == $page) $pagination .= "<li class=\"active\"><a href=\"{$pagestring}/{$counter}\" class=\"button\">{$counter}</a></li>";
                else $pagination .= "<li><a href=\"{$pagestring}/{$counter}\" class=\"button\">{$counter}</a></li>";
            }
        }
    }

    return $pagination;
}

function post_status($status)
{
    switch ($status)
    {
        case DRAFT:
            return 'Draft';
            break;
        case PUBLISH:
            return 'Publish';
            break;
        default:
            return 'Undefined';
            break;
    }
}

function shipping_type($shipping)
{
    switch ($shipping)
    {
        case SHIP_BY_AIR:
            return 'Air';
            break;
        case SHIP_BY_BUS:
            return 'Bus';
            break;
        case SHIP_BY_TRAIN:
            return 'Train';
            break;
        case SHIP_BY_EMS:
            return 'EMS';
            break;
        default:
            return 'Undefined';
            break;
    }
}

function product_status($status)
{
    switch ($status)
    {
        case ORDER_ORDERING:
            return 'Unconfirm';
            break;
        case ORDER_CONFIRMED:
            return 'Confirmed';
            break;
        case ORDER_PACKED:
            return 'Packed';
            break;
        case ORDER_SENDING:
            return 'Sending';
            break;
        case ORDER_SUCCESS:
            return 'Success';
            break;
        case ORDER_FAILURE:
            return 'Failure';
            break;
        default:
            return 'Undefind';
            break;
    }
}

/**
 * -----------------------------------------------------------------------------
 * Page and menu function
 * -----------------------------------------------------------------------------
 */

/**
 * Check class name from URL
 * 
 * @param string $class_name
 * @param string $out_put
 * @return mixed
 */
function is_class($class_name, $output = NULL)
{
    $CI = get_CI();

    $fetch_class = $CI->router->fetch_class();

    if (strcasecmp($fetch_class, $class_name) == 0)
    {
        return (isset($output) ? $output : TRUE);
    }

    return FALSE;
}

/** Check method name from URL
 * 
 * @param string $method_name
 * @param string $out_put
 * @return mixed
 */
function is_method($method_name, $output = NULL)
{
    $CI = get_CI();

    $fetch_method = $CI->router->fetch_method();

    if (strcasecmp($fetch_method, $method_name) == 0)
    {
        return (isset($output) ? $output : TRUE);
    }

    return FALSE;
}

/**
 * -----------------------------------------------------------------------------
 * User and security function 
 * ----------------------------------------------------------------------------- 
 */
/**
 * -----------------------------------------------------------------------------
 * Debug 
 * ----------------------------------------------------------------------------- 
 */

/**
 * Get class prefix
 * 
 * @param string $class_name 
 * @return string
 */
function get_class_prefix($class_name)
{
    preg_match('/([a-zA-Z]+_)/', $class_name, $matches);

    return (!empty($matches) ? $matches[0] : NULL);
}

/* End of file dsb_helper.php */
/* Location: ./application/modules/dashboard/helpers/dsb_helper.php */